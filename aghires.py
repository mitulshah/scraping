from bs4 import BeautifulSoup
import requests
import re
import MySQLdb
import urllib2
import logging

# for logging
LOG_FILENAME = 'aghires.log'
logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M',
                    filename='scraping/logs/{}'.format(LOG_FILENAME) )

# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '104.236.54.155:8080'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)


def fetch_data_using_scraping(page='?searchId=1443099754&action=search&page=1'):

    base_url = "https://aghires.com/browse-by-company/%s" % (page)
    # result = requests.get(base_url)
    # data = result.text
    request = urllib2.Request(base_url, headers=hdr)
    try:
        result = urllib2.urlopen(request)
        status = True
    except urllib2.HTTPError, err:
        logging.error({'error': err, 'url': base_url})
    if status and result.getcode() == 200:
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')

        for element in soup.find('tbody').find_all('tr'):
            all_td = element.find_all('td')
            td_html = all_td[1].find('a', href=True)
            location = all_td[2].text
            print "##### start scraping for {} #####".format(td_html.text)
            request_int = urllib2.Request(td_html['href'], headers=hdr)
            try:
                result_int = urllib2.urlopen(request_int) 
            except urllib2.HTTPError, err:
                logging.error({'error': err, 'url': base_url})
            if status and result_int.getcode() == 200:
                data_int = result_int.read()
                soup_internal = BeautifulSoup(data_int, 'html.parser')

            # r_link = requests.get()
            # if r_link.status_code == 200:
            #     r = r_link.text

            #     soup_internal = BeautifulSoup(r, 'html.parser')

                company_info = soup_internal.find(
                    'div', {'class': 'compProfileInfo'})

                findphone = company_info.find(
                    'strong', text=re.compile('Phone'))
                extract_phone = findphone.nextSibling.split()

                # fetch phone number
                try:
                    phone = extract_phone[1]
                except:
                    phone = ''
                # fetch website if exists
                try:
                    website = company_info.find('a').get('href')
                except:
                    website = ''

                # fetch website logo, if exists
                try:
                    websiteLogo = company_info.find('img').get('src')
                except:
                    websiteLogo = ''

                address = company_info.text.split('Phone')[0]

                company_detail = soup_internal.find(
                    'div', {'class': 'listingInfo'}).text

                sublst = [td_html.text, location, websiteLogo,
                          address, phone, website, company_detail]

                # print "Congratulation..!! {} is scrapped
                # :)".format(td_html.text)

                # mainlst.append(sublst)
                # print mainlst
                storeindb(sublst)

    # call the function again if next page is exist.
    nextPage = soup.find('span', {'class': 'nextBtn'}).find('a').get('href')
    if nextPage:
        fetch_data_using_scraping(nextPage)


def storeindb(item):
    print "storing into database ...!!!!!"
    # Open database connection
    db = MySQLdb.connect("localhost", "root", "root", "scrape")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    try:
        # for item in mainlst:
        sql = "INSERT into aghires (name, location, logo, address, phone, website, company_detail) values(%s, %s, %s, %s, %s, %s, %s)"

        cursor.execute(sql, (item[0].encode('utf-8'), item[1].encode('utf-8'), item[2].encode('utf-8'), item[
                       3].encode('utf-8'), item[4].encode('utf-8'), item[5].encode('utf-8'), item[6].encode('utf-8')))
        db.commit()
    except Exception as e:
        logging.error({'error': e, 'message': 'Data insertation issue', 'data': item})
    # disconnec.encode('utf-8')t from server
    db.close()

fetch_data_using_scraping()
# print mainlst
# with(open('lst.py', 'w')) as f:
#     f.write(str(mainlst))
