from config import SQLAlchemy
from datetime import datetime
from uuid import uuid4
db = SQLAlchemy()


class SummarizeEvent(db.Model):

    """
    Summarize events of atomic.events table
    """
    __tablename__ = 'atomic.summarize_events'

    summarize_eventid = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.Integer)
    collector_date = db.Column(db.Date)
    mkt_source = db.Column(db.String(255))
    mkt_medium = db.Column(db.String(255))
    mkt_campaign = db.Column(db.String(255))
    is_user_new = db.Column(db.String(20))
    visits = db.Column(db.Integer)
    amount = db.Column(db.Float)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __init__(self, app_id, collector_date, mkt_source, mkt_medium,
                 mkt_campaign, is_user_new, visits, amount,
                 created_at, updated_at):

        self.app_id = app_id
        self.collector_date = collector_date
        self.mkt_source = mkt_source
        self.mkt_medium = mkt_medium
        self.mkt_campaign = mkt_campaign
        self.is_user_new = is_user_new
        self.visits = visits
        self.amount = amount
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return '{} {}'.format(self.mkt_source, self.mkt_medium, self.visits)


class User(db.Model):

    """
    Model referes to users table
    """
    __tablename__ = 'users'

    userid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    token = db.Column(db.String(255))
    token_expiry = db.Column(db.DateTime)
    isdeleted = db.Column(db.Integer)

    def __init__(self, username, token, token_expiry,
                 isdeleted):
        self.username = username
        self.token = token
        self.token_expiry = token_expiry
        self.isdeleted = 0

    def __repr__(self):
        pass
