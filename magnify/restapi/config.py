import os 

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.stormpath import StormpathManager
from stormpath.client import Client
from sqlalchemy import create_engine

from models import *


app = Flask(__name__, static_url_path="")
app.debug = True

dbhost = 'snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com:5439'
dbuser = 'niravb'
dbpass = 'XmTW9rsc'
dbname = 'webanalytics'
DB_URI = 'redshift://' + dbuser + ':' + dbpass + '@' + dbhost + '/' + dbname
# engine = create_engine('postgresql+psycopg2://power_user:n31181b@localhost/snowplow')

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI
app.config['SECRET_KEY'] = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

db = SQLAlchemy(app)
db.pool_recycle = 3600

db.init_app(app)



# app.config['STORMPATH_API_KEY_FILE'] = os.getcwd() + '/apiKey.properties'
# app.config['STORMPATH_APPLICATION'] = 'api-sas'
# app.config['STORMPATH_ENABLE_REGISTRATION'] = False
# app.config['STORMPATH_ENABLE_LOGIN'] = True
# app.config['STORMPATH_ENABLE_LOGOUT'] = False
# app.config['STORMPATH_REQUIRE_GIVEN_NAME'] = False
# app.config['STORMPATH_REQUIRE_SURNAME'] = False

# stormpath_manager = StormpathManager()
# stormpath_manager.init_app(app)

apikeypath = os.getcwd() + '/apiKey.properties'
client = Client(api_key_file=apikeypath)
