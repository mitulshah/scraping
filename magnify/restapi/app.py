from datetime import datetime, timedelta
from pytz import timezone
from uuid import uuid4
import ast
from collections import defaultdict

from flask.ext.restplus import Api, Resource, fields
from flask.ext.cors import CORS
from stormpath.error import Error as StormpathError
from flask_restful import reqparse
from werkzeug.contrib.fixers import ProxyFix
from sqlalchemy.sql import func
# import local module
from models import *
from config import db, app, Flask, client

cst = timezone('US/Central')
curtime = cst.localize(datetime.now()).strftime('%Y-%m-%d')
yesterday_start = cst.localize(datetime.now() - timedelta(1)
                               ).strftime('%Y-%m-%d 00:00:00')
yesterday_end = cst.localize(datetime.now() - timedelta(1)
                             ).strftime('%Y-%m-%d 23:59:59')

app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='Snowplow Aggregation API',
          description='Data Summarization',
          )
CORS(app)


def abort_if_doesnt_exist(ele):
    api.abort(404, "{} doesn't exist".format(ele))


visits = api.namespace(
    'visits', description='Compute Marketing attributes')
dayvsallcomparison = api.namespace(
    'yesterdayVSAllDayReport', description='Performance Report, \
    Yesterday vs All Time')

datewisereport = api.namespace(
    'dateWiseReport', description='Date Wise Summarize Report')

# parser_ = api.parser()
parser = reqparse.RequestParser()
parser.add_argument(
    'app_id', type=str, required=True, help='Define APP ID',
    location='form')
parser.add_argument(
    'start_date', type=str, required=True, help='Set Start Date',
    location='form')
parser.add_argument(
    'end_date', type=str, required=True, help='Set End Date',
    location='form')
parser.add_argument(
    'mkt_source', type=str, required=True, help='Set Mkt Source',
    location='form')
parser.add_argument(
    'mkt_medium', type=str, required=True, help='Set Mkt Medium',
    location='form')
parser.add_argument(
    'mkt_campaign', type=str, required=True, help='Set Mkt Camapign',
    location='form')


@visits.route('/<string:app_id>/<string:startdate1>/<string:enddate>')
@api.doc(responses={404: 'No record founds'},
         params={'startdate': 'Start Date Format: yyyy-mm-dd',
                 'enddate': 'End Date Format: yyyy-mm-dd',
                 'app_id': 'Client identification Code'})
class Conversion(Resource):

    """
    Return calculation of visits and conversion
    """

    def get(self, app_id, startdate, enddate):
        """
        visit records of given periods
        """
        # resulta = db.session.query(SummarizeEvent)
        query = """SELECT
                   sum(visits), a.mkt_source, a.mkt_medium, a.mkt_campaign
                   FROM atomic.summarize_events a
                   WHERE 
                   a.collector_date between '{0}' and '{2}'
                   AND a.app_id = '{3}'
                   AND EXISTS (
                        SELECT 1 from atomic.summarize_events b
                          where coalesce(a.mkt_source, 'Empty') = coalesce(b.mkt_source, 'Empty')
                          AND coalesce(a.mkt_medium, 'Empty')  = coalesce(b.mkt_medium, 'Empty')
                          AND coalesce(a.mkt_campaign, 'Empty') = coalesce(b.mkt_campaign, 'Empty')
                          AND b.collector_date between '{0}' and '{1}'
                          AND b.app_id = '{3}'
                   )
                   GROUP BY a.mkt_source, a.mkt_medium, a.mkt_campaign
                   ORDER BY mkt_source""".format(startdate, enddate, curtime, app_id)
        raw = db.session.execute(query)

        result = []
        for row in raw:
            result.append(
                {'visits': row[0], 'source': row[1], 'medium': row[2],
                 'campaign': row[3]})
        return {'result': result, "status": True}


@dayvsallcomparison.route('/<string:app_id>')
@api.doc(responses={404: 'No record founds'},
         params={'app_id': 'Client identification Code'})
class YesterdayVsAllCampaignReport(Resource):

    """
    Return performance of each channel with their landing page
    """

    def get(self, app_id):
        """

        """
        # query = """SELECT * FROM atomic.daily_reports \
        # WHERE  app_id='{0}' AND created_at between '{1}' AND '{2}' \
        # order by ptime, mkt_source,
        # mkt_medium, mkt_campaign """.format(
        #     app_id, yesterday_start, yesterday_end)
        query = """SELECT * FROM atomic.daily_reports \
        WHERE  app_id='{0}' \
        order by ptime, mkt_source,
        mkt_medium, mkt_campaign """.format(
            app_id, yesterday_start, yesterday_end)

        rows = db.session.execute(query)
        result = []
        d = defaultdict()
        for row in rows:
            campaignname = row[5]
            if campaignname.endswith("/"):
                campaignname = campaignname[:-1]
            uid = row[3] + row[4] + campaignname
            try:
                if len(d[uid]) > 0:
                    pass
            except KeyError:
                d[uid] = defaultdict(list)
            d[uid]['MKTSOURCE'] = row[3]
            d[uid]['MKTMEDIUM'] = row[4]
            d[uid]['MKTCAMPAIGN'] = campaignname
            otherdata = {'PAGEURLHOST': row[1], 'PAGEURLPATH': row[2],
                         'MKTSOURCE': row[3], 'MKTMEDIUM': row[4],
                         'MKTCAMPAIGN': campaignname, 'attr_method': row[6],
                         'visits': row[7],
                         'visitors': row[8], 'converters': row[9]}
            if row[10] == u'all':
                d[uid]['resultsalltime'].append(otherdata)

        result.append(d)

        return {'result': result}


@datewisereport.route('/')
class DateWiseReport(Resource):

    """
    Return performance of each channel with their landing page
    """
    # @api.doc(parser=parser)
    def post(self):
        """

        """
        argkey = parser.parse_args()
        start_date = argkey['start_date']
        end_date = argkey['end_date']
        mkt_campaign = argkey['mkt_campaign']
        mkt_source = argkey['mkt_source']
        mkt_medium = argkey['mkt_medium']
        app_id = argkey['app_id']

        start_date = start_date + ' 00:00:00'
        end_date = end_date + ' 23:59:59'

        query = """SELECT 
        app_id, page_urlhost, page_urlpath,
        mkt_source, mkt_medium, mkt_campaign, clicktype,
        sum(visits) visits,
        sum(visitors) visitors,
        sum(converters) converters, ptime
        FROM atomic.daily_reports
        WHERE  app_id='{0}'
        AND created_at between '{1}' AND '{2}'
        AND mkt_source='{3}'
        AND mkt_medium='{4}'
        AND (mkt_campaign='{5}' OR mkt_campaign='{5}/')
        AND ptime != 'all'
        AND clicktype='lc'
        GROUP BY app_id, page_urlhost, page_urlpath,
        mkt_source,
        mkt_medium, mkt_campaign, ptime, clicktype
        order by ptime, mkt_source,
        mkt_medium, mkt_campaign""".format(
            app_id, start_date, end_date, mkt_source,
            mkt_medium, mkt_campaign)
        # print query
        rows = db.session.execute(query)
        result = []
        d = defaultdict()
        for row in rows:
            # print row
            campaignname = row[5]
            if campaignname.endswith("/"):
                campaignname = campaignname[:-1]
            uid = row[3] + row[4] + campaignname
            try:
                if len(d[uid]) > 0:
                    pass
            except KeyError:
                d[uid] = defaultdict(list)
            # d[uid]['MKTSOURCE'] = row[3]
            # d[uid]['MKTMEDIUM'] = row[4]
            # d[uid]['MKTCAMPAIGN'] = campaignname
            otherdata = {'PAGEURLHOST': row[1], 'PAGEURLPATH': row[2],
                         'MKTSOURCE': row[3], 'MKTMEDIUM': row[4],
                         'MKTCAMPAIGN': campaignname,
                         'visits': row[7],
                         'visitors': row[8], 'converters': row[9]}
            # Add only Last click data
            if row[6] == 'lc':
                d[uid][row[2]] = otherdata
            # else:
            #     d[uid]['resultsdaywise'].append(otherdata)
        result.append(d)

        return {'result': result}


if __name__ == '__main__':
    app.debug = True
    app.run(port=5502)
