import cookielib
import os
import urllib
import urllib2
import re
import string
from bs4 import BeautifulSoup
import time
import requests
from credential import username, password
import json
import operator
import csv
from socket import timeout
import psycopg2
import psycopg2.extras

cookie_filename = "parser.cookies.txt"

cur = ''


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


@redshift_dbconn
class AlexaParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        proxy = urllib2.ProxyHandler({'http': '209.242.141.60:8080'})
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=1),
            # urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            proxy
        )
        self.opener.addheaders = [
            ('User-agent',
             ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'))
        ]

        # Login
        # self.loginPage()
        siteurllist = ['http://www.magnifymoney.com/']
        for website in siteurllist:
            self.loadData(website)
        self.cj.save()  

    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                print "=*= With No DATA, {} is going to open =*=".format(url)
                response = self.opener.open(url, timeout=10)
            return ''.join(response.readlines())
        except (urllib2.HTTPError, urllib2.URLError) as error:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            print error
            # return self.loadPage(url, data)
            return

    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        html = self.loadPage("https://www.alexa.com/secure/login?resource=")
        soup = BeautifulSoup(html, 'html.parser') 

        login_data = urllib.urlencode({
            'email': self.login,
            'password': self.password,
            'resource': '/pro/dashboard',
            'resource': '',
            'ip_address': '14.140.153.0'
        })

        html = self.loadPage(
            "https://www.alexa.com/secure/login/ajaxex", login_data)

        if html:
            print "You're logged In now, Please comment out the function loginpage for next request"
        return

    def loadData(self, siteurl):

        # change the below lines and add the link of specific user
        # reciept id is "clickedEntityId" (get the value from below url)
        page = "http://www.alexa.com/siteinfo/{}".format(siteurl)
        try:
            html = self.loadPage(page)

            soup = BeautifulSoup(html, 'html.parser')
            try:
                bounce_rate = soup.find('span', {'data-cat': 'bounce_percent'}
                                        ).find('strong').text
            except Exception:
                bounce_rate = ''
            try:
                daily_pageview = soup.find('span', {'data-cat': 'pageviews_per_visitor'}
                                           ).find('strong').text
            except Exception:
                daily_pageview = 0.0

            try:
                global_rank = soup.find(
                    'span', {'data-cat': 'globalRank'}).find('strong', {'class': 'metrics-data'}).text
            except Exception:
                global_rank = 0

            us_visitors = us_rank = ''
            for tr in soup.find('table', {'id': "demographics_div_country_table"}).find('tbody').find_all('tr'):
                if tr.find('a')['href'] == '/topsites/countries/US':
                    us_visitors = tr.find_all('td')[1].text
                    us_rank = tr.find_all('td')[2].text

            gender = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-gender'})
            try:
                male = gender.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                male = ''
            try:
                female = gender.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                female = ''

            demo_age = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-age'})
            try:
                age18_24 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age18_24 = ''

            try:
                age25_34 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age25_34 = ''

            try:
                age35_44 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[3].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age35_44 = ''

            try:
                age45_54 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[4].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age45_54 = ''

            try:
                age55_64 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[5].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age55_64 = ''

            try:
                age65 = demo_age.find_all(
                    'div', {'class': 'pybar-row'})[6].find('span', {'class': 'pybar-help'}).text
            except Exception:
                age65 = ''

            demo_child = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-children'})
            try:
                child_yes = demo_child.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                child_yes = ''

            try:
                child_no = demo_child.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                child_no = ''

            education = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-education'})
            try:
                nocollege = education.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                nocollege = ''
            try:
                somecollege = education.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                somecollege = ''
            try:
                graduatecollege = education.find_all(
                    'div', {'class': 'pybar-row'})[3].find('span', {'class': 'pybar-help'}).text
            except Exception:
                graduatecollege = ''
            try:
                college = education.find_all(
                    'div', {'class': 'pybar-row'})[4].find('span', {'class': 'pybar-help'}).text
            except Exception:
                college = ''

            demo_income = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-income'})

            try:
                income0_30k = demo_income.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                income0_30k = ''
            try:
                income30_60k = demo_income.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                income30_60k = ''
            try:
                income60_100k = demo_income.find_all(
                    'div', {'class': 'pybar-row'})[3].find('span', {'class': 'pybar-help'}).text
            except Exception:
                income60_100k = ''
            try:
                income100k = demo_income.find_all(
                    'div', {'class': 'pybar-row'})[4].find('span', {'class': 'pybar-help'}).text
            except Exception:
                income100k = ''

            localtion = soup.find(
                'div', {"id": "demographics-content"}).find('span', {'class': 'demo-col3'})
            try:
                home = localtion.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                home = ''
            try:
                school = localtion.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                school = ''
            try:
                work = localtion.find_all(
                    'div', {'class': 'pybar-row'})[3].find('span', {'class': 'pybar-help'}).text
            except Exception:
                work = ''
            
            ethnicity = soup.find(
                'div', {"id": "demographics-content"}).find('div', {'class': 'demo-ethnicity'})
            try:
                african = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[1].find('span', {'class': 'pybar-help'}).text
            except Exception:
                african = ''
            try:
                african_american = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[2].find('span', {'class': 'pybar-help'}).text
            except Exception:
                african_american = ''
            try:
                asian = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[3].find('span', {'class': 'pybar-help'}).text
            except Exception:
                asian = ''

            try:
                caucasian = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[4].find('span', {'class': 'pybar-help'}).text
            except Exception:
                caucasian = ''

            try:
                hispanic = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[5].find('span', {'class': 'pybar-help'}).text
            except Exception:
                hispanic = ''

            try:
                middle_eastern = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[6].find('span', {'class': 'pybar-help'}).text
            except Exception:
                middle_eastern = ''

            try:
                other = ethnicity.find_all(
                    'div', {'class': 'pybar-row'})[7].find('span', {'class': 'pybar-help'}).text
            except Exception:
                other = ''

            try:
                speed = soup.find(
                    'section', {"id": "loadspeed-panel-content"}).find('span').text
            except Exception:
                speed = ''

            try:
                s1 = soup.find(
                    'section', {"id": "loadspeed-panel-content"}).find('p').text
                s2 = s1.split('(')[1]
                speed_in_seconds = s2.split(')')[0]
            except Exception:
                speed_in_seconds = ''

            # print "Speed is {}".format(speed)

            fdata = [(siteurl, bounce_rate.strip(), daily_pageview.strip(),
                      us_visitors.strip(), us_rank.strip(),
                      global_rank.strip(),
                      male.strip(), female.strip(), age18_24.strip(), age25_34.strip(),
                      age35_44.strip(), age45_54.strip(), age55_64.strip(),
                      age65.strip(),
                      child_yes.strip(),
                      child_no.strip(),
                      nocollege.strip(), somecollege.strip(),
                      graduatecollege.strip(), college.strip(),
                      income0_30k.strip(), income30_60k.strip(),
                      income60_100k.strip(), income100k.strip(),
                      home.strip(), school.strip(), work.strip(),
                      african.strip(), african_american.strip(), asian.strip(),
                      caucasian.strip(), hispanic.strip(), middle_eastern.strip(),
                      other.strip(), speed.strip(), speed_in_seconds.strip())]
            # print fdata
            try:
                cur.executemany(
                    "INSERT into atomic.magnify_model_alexa_demography (domain_url, bounce_rate, daily_pageview, us_visitors, us_rank, global_rank ,male, female, age18_24, age25_34, age35_44, age45_54, age55_64, age65, child_yes, child_no, nocollege, somecollege, graduatecollege, college, income0_30k, income30_60k, income60_100k, income100k, home, school, work, african, african_american, asian, caucasian, hispanic, middle_eastern, other, speed, speed_in_seconds) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", fdata)
            except Exception as e:
                print e
        except urllib2.HTTPError, err:
            print err
        except Exception as e:
            print e

parser = AlexaParser(username, password)
