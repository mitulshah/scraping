import psycopg2
import psycopg2.extras
from collections import OrderedDict
import re
cur = ''


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper

alldata = []


@redshift_dbconn
def SelectQuery():
    try:
        cur.execute(
            "select domain_url, data from atomic.magnify_model_alexaapi2 where id > 777")
        response = cur.fetchall()
        for res in response:
            domain_url = res[0]
            print domain_url
            data = eval(res[1])

            try:
                path = data['aws:UrlInfoResponse']['aws:Response'][
                    'aws:UrlInfoResult']['aws:Alexa']
            except Exception as e:
                path = {}
            try:
                categoryData = path['aws:Related'][
                    'aws:Categories']['aws:CategoryData']
                categoryTitle = categoryData['aws:Title']
                categoryPath = categoryData['aws:AbsolutePath']
            except TypeError:
                categoryData = path['aws:Related'][
                    'aws:Categories']['aws:CategoryData']
                categoryTitle = categoryData[0]['aws:Title']
                categoryPath = categoryData[0]['aws:AbsolutePath']
            except Exception as e:
                categoryTitle = ''
                categoryPath = ''
            try:
                threeMonthRank = path['aws:TrafficData']['aws:Rank']
            except Exception as e:
                threeMonthRank = 0

            try:
                usage = path['aws:TrafficData']['aws:UsageStatistics']
            except Exception as e:
                usage = {}

            try:
                threeMonthVisitors = usage['aws:UsageStatistic'][0][
                    'aws:PageViews']['aws:PerMillion']['aws:Value']
                threeMonthVisitors = re.sub("[^0-9.]", "", threeMonthVisitors)
            except Exception as e:
                threeMonthVisitors = 0

            try:
                oneMonthVisitors = usage['aws:UsageStatistic'][1][
                    'aws:PageViews']['aws:PerMillion']['aws:Value']
                oneMonthVisitors = re.sub("[^0-9.]", "", oneMonthVisitors)
            except Exception:
                oneMonthVisitors = 0

            try:
                sevenDaysVisitors = usage['aws:UsageStatistic'][2][
                    'aws:PageViews']['aws:PerMillion']['aws:Value']
                sevenDaysVisitors = re.sub("[^0-9.]", "", sevenDaysVisitors)
            except Exception:
                sevenDaysVisitors = 0

            try:
                oneDaysVisitors = usage['aws:UsageStatistic'][3][
                    'aws:PageViews']['aws:PerMillion']['aws:Value']
                oneDaysVisitors = re.sub("[^0-9.]", "", oneDaysVisitors)
            except Exception:
                oneDaysVisitors = 0

            # print "Domain {}, Rank {}, Category Title {},\
            # 3-Month Visitors {} \
            # 1-Month Visitors {} \
            # 7-Days Visitors {} \
            # 1-Days visitors {} \
            # ".format(domain_url, threeMonthRank,
            #          categoryTitle,
            #          threeMonthVisitors,
            #          oneMonthVisitors,
            #          sevenDaysVisitors,
            #          oneDaysVisitors)
            # alldata.append((domain_url, threeMonthRank,
            #                 categoryTitle,
            #                 categoryPath,
            #                 threeMonthVisitors,
            #                 oneMonthVisitors,
            #                 sevenDaysVisitors,
            #                 oneDaysVisitors))
            alldata = (domain_url, threeMonthRank,
                       categoryTitle,
                       categoryPath,
                       threeMonthVisitors,
                       oneMonthVisitors,
                       sevenDaysVisitors,
                       oneDaysVisitors)
            # print path
            cur.execute(
                "INSERT into atomic.magnify_model_alexa (domain_url, global_rank, category_title, category_path, threemonths_visitors_mn, onemonth_visitors_mn, sevendays_visitors_mn, onedays_visitors_mn) values(%s, %s, %s, %s, %s, %s, %s, %s)", alldata)
    except Exception as e:
        print e

SelectQuery()


@redshift_dbconn
def insertationProcess():
    SelectQuery()
    try:
        cur.executemany(
            "INSERT into atomic.magnify_model_alexa (domain_url, global_rank, category_title, category_path, threemonths_visitors_mn, onemonth_visitors_mn, sevendays_visitors_mn, onedays_visitors_mn) values(%s, %s, %s, %s, %s, %s, %s, %s)", alldata)
    except Exception as e:
        print e

# insertationProcess()
