import datetime
import urllib
import hmac
import hashlib
import base64
import xmltodict
import json
import urllib2
import psycopg2
import psycopg2.extras

# Query Options  # refer to AWIS API reference for full details.
Action = "UrlInfo"
Url = "http://wate.com/"
ResponseGroup = "TrafficData,Categories"

# Config Options
# PATH_TO_SECRET = 'c:\\users\\john\\aws-secret.txt' #Path to ANSI encoded
# text file with secret.
AWSAccessKeyId = "AKIAIAORGQLMVG24WYVA"  # Enter your AWS access id.

# Don't edit below.
# f = open('c:\\users\\john\\aws-secret.txt')
# secret = (f.read())
secret = 'B7/5duW8RxeY+ib48YjyR1wzCfiDgR3J0G21TKeA'
SignatureVersion = "2"
SignatureMethod = "HmacSHA256"
ServiceHost = "awis.amazonaws.com"
PATH = "/"
alldata = []


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


def create_timestamp():
    now = datetime.datetime.now()
    timestamp = now.isoformat()
    return timestamp


def create_uri(params):
    params = [(key, params[key])
              for key in sorted(params.keys())]
    return urllib.urlencode(params)


def create_signature(params):
    Uri = create_uri(params)
    msg = "\n".join(["GET", ServiceHost, PATH, Uri])
    hmac_signature = hmac.new(secret, msg, hashlib.sha256)
    signature = base64.b64encode(hmac_signature.digest())
    return urllib.quote(signature)


def main(urlinfo):
    params = {
        'Action': Action,
        'Url': urlinfo,
        'ResponseGroup': ResponseGroup,
        'SignatureVersion': SignatureVersion,
        'SignatureMethod': SignatureMethod,
        'Timestamp': create_timestamp(),
        'AWSAccessKeyId': AWSAccessKeyId,
    }

    uri = create_uri(params)
    signature = create_signature(params)

    url = "http://%s/?%s&Signature=%s" % (ServiceHost, uri, signature)
    # print url
    res = urllib2.urlopen(url)

    response = ''
    for x in res:
        response += x

    # print response
    json_data = xmltodict.parse(str(response))
    alldata.append((urlinfo, str(dict(json_data))))

    with open('Awisdata.txt', 'w') as f:
        f.write(str(alldata))

urllist = ['http://www.noticiariodirecto.com/',
           'http://www.wsmv.com/',
           'http://us.quizly.io/',
           'http://www.wikifeet.com/',
           'http://www.rechingon.com/',
           'http://www.wildlifeinsider.com/',
           'http://www.postcrescent.com/',
           'http://www.iomtoday.co.im/',
           'http://www.pensamientos.com/',
           'http://www.dailydisclosure.com/',
           'http://www.azcentral.com/',
           'http://www.foreverceleb.com/',
           'http://www.wisebread.com/',
           'http://www.dailydisclosure.com/',
           'http://www.littlebudha.com/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://www.liekr.com/',
           'http://www.earthtripper.com/',
           'http://www.dailydisclosure.com/',
           'http://www.particlenews.com/',
           'http://www.dailydisclosure.com/',
           'http://www.nbcconnecticut.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.yourdailydish.com/',
           'http://www.openstars.info/',
           'http://www.bestplay.pk/',
           'http://www.dailydisclosure.com/',
           'http://www.littlebudha.com/',
           'http://www.dailydisclosure.com/',
           'http://www.lifestylepassion.com/',
           'http://www.ap.org/',
           'http://www.dailydisclosure.com/',
           'http://watchersonthewall.com/',
           'http://www.trend-chaser.com/',
           'https://www.thelevelup.com/',
           'http://www.10tv.com/',
           'https://www.oyster.com/',
           'http://www.theledger.com/',
           'http://www.thefunkyruca.com/',
           'http://www.westernmassnews.com/',
           'http://www.dailydisclosure.com/',
           'http://www.military.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.sublimly.com/',
           'http://rawapk.com/',
           'http://www.lifestylepassion.com/',
           'http://www.nbcconnecticut.com/',
           'http://www.metacafe.com/',
           'http://www.biography.com/news/john-cena-video-larry-king-interview',
           'http://www.ap.org/',
           'http://thelisticles.net/',
           'http://www.intenseexperiences.com/',
           'http://www.pensamientos.com/',
           'http://www.earthtripper.com/',
           'http://www.yourdailydish.com/',
           'http://goolfm.net/',
           'http://www.dailydisclosure.com/',
           'http://appletoolbox.com/',
           'http://www.dailydisclosure.com/',
           'http://www.nbcsports.com/',
           'http://www.sportingnews.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.nbcconnecticut.com/',
           'http://www.visaliatimesdelta.com/',
           'http://reallycorny.com/',
           'http://www.dailydisclosure.com/',
           'http://www.sublimly.com/',
           'http://www.ignisnatura.cl/',
           'http://www.lifestylepassion.com/',
           'http://www.americanitsolutions.com/',
           'http://www.dailydisclosure.com/',
           'http://www.crazynews.net/',
           'http://www.nbcwashington.com/',
           'http://ifemenino.com/',
           'http://www.sublimly.com/',
           'http://www.pensamientos.com/',
           'http://www.worldatlas.com/',
           'http://kingpiggy.com/',
           'http://www.nbcwashington.com/',
           'http://wildlifeinsider.com/',
           'http://www.ap.org/',
           'http://www.crazynews.net/',
           'http://www.photographyblog.com/',
           'http://www.intenseexperiences.com/',
           'http://www.dailydisclosure.com/',
           'http://earth911.com/',
           'http://www.everydaydiabeticrecipes.com/',
           'http://www.dayhot.com/',
           'http://www.earthtripper.com/',
           'http://www.trend-chaser.com/',
           'http://www.lifestylepassion.com/',
           'http://www.dailydisclosure.com/',
           'http://www.military.com/',
           'http://www.shieldsgazette.com/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://www.realgm.com/',
           'http://www.nbcconnecticut.com/',
           'http://www.handycafe.com/',
           'http://www.dailydisclosure.com/',
           'http://www.richestlifestyle.com/',
           'http://www.dailydisclosure.com/',
           'http://www.liekr.com/',
           'http://www.earthtripper.com/',
           'http://www.faithit.com/',
           'http://www.afternoonspecial.com/',
           'http://www.whenlovewasreal.com/',
           'http://www.pressroomvip.com/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://www.dailydisclosure.com/',
           'http://naidunia.jagran.com/',
           'http://www.cuidadosdetusalud.com/',
           'http://www.sportingz.com/',
           'http://www.dailydisclosure.com/',
           'http://www.handycafe.com/',
           'http://thetopnewsblog.com/',
           'http://lotto.pch.com/',
           'http://jewishbusinessnews.com/',
           'http://www.graphic.com.gh/',
           'http://www.nbcwashington.com/',
           'http://dc.wikia.com/',
           'http://www.rediff.com/',
           'http://mensdailydigest.com/',
           'http://www.azcentral.com/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://www.belfasttelegraph.co.uk/',
           'http://www.viviendoensalud.com/',
           'http://www.sublimly.com/',
           'http://www.earthtripper.com/',
           'http://www.pensamientos.com/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://www.dailydisclosure.com/',
           'http://www.dailydisclosure.com/',
           'http://myfirstclasslife.com/',
           'http://www.leftlanenews.com/',
           'http://thewowstyle.tumblr.com/',
           'http://www.dailydisclosure.com/',
           'http://www.majorten.com/',
           'http://www.lifestylepassion.com/',
           'http://usefultipsforhome.com/',
           'http://www.dailydisclosure.com/',
           'http://www.faithit.com/',
           'http://www.dailydisclosure.com/',
           'http://tamillol.com/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://saludtecnologicapij.blogspot.in/',
           'http://earth911.com/',
           'http://jagrukbharat.com/',
           'http://americanactionnews.com/',
           'http://www.dailydisclosure.com/',
           'http://www.thelostogle.com/',
           'http://www.intenseexperiences.com/',
           'http://wildlifeinsider.com/',
           'http://www.lifestylepassion.com/',
           'http://www.springfieldnewssun.com/',
           'http://www.nbcwashington.com/',
           'http://www.cnbc.com/',
           'http://buzja.com/',
           'http://courier-tribune.com/',
           'http://dailycurrant.com/',
           'http://mensdailydigest.com/',
           'http://www.townnews365.com/',
           'http://cyber-breeze.com/',
           'http://www.obsev.com/',
           'http://ftw.usatoday.com/',
           'http://noticiariodirecto.com/',
           'https://soundcloud.com',
           'http://www.thestar.co.uk/',
           'http://www.ap.org/']

for urldata in urllist:
    print urldata
    main(urldata)


@redshift_dbconn
def insertationProcess():
    try:
        cur.executemany(
            "INSERT into atomic.magnify_model_alexaapi2 (domain_url, data) values(%s, %s)", alldata)
    except Exception as e:
        print e

insertationProcess()
