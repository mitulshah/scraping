import psycopg2
import psycopg2.extras
import time
import csv

alldata = []


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


def getData():
    """
    fetch data
    """
    with open('mapping_data.csv', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='@', quotechar='|')
        for row in spamreader:
            alldata.append(tuple(row))


@redshift_dbconn
def insertationProcess():
    getData()
    for x in alldata:
        try:
            cur.execute(
                "INSERT into atomic.magnify_mapping_url (referring_url, url, taboola_name, global_site_ranking, us_site_ranking, male, female, no_college, some_college, college, graduate_school, age_18_24, age_25_34, age_35_44, age_45_54, age_55_64, age_65, income_0_30K, income_30K_60K, income_60K_100K, income_100K, race_african, race_african_american, race_asian, race_caucasian, race_hispanic, race_middle_eastern, race_other, speedscore_mobile, usability_score, speedscore_desktop, domain_authority, page_authority) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", x)
        except Exception as e:
            print e, x

insertationProcess()
