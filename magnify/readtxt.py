from urlparse import urlparse

with open("urllist.txt", 'r') as f:
    listofURLWithNull = f.read().splitlines()

print len(set(listofURLWithNull))

listofURLRemoveNull = filter(None, listofURLWithNull)

# print listofURLRemoveNull

listOfURL = set(listofURLWithNull)
print len(listOfURL)

fullURL1 = ['http://{0}'.format(x)
            for x in listOfURL if not x.startswith('http')]

fullURL2 = [x
            for x in listOfURL if x.startswith('http')]

fullURL = fullURL1 + fullURL2

print len(fullURL)

cleanUrlList = []
for furl in fullURL:
    parsed_uri = urlparse(furl)
    cleanUrl = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    if cleanUrl != 'http:///':
        cleanUrlList.append(cleanUrl)

# print sorted(cleanUrlList)
# print len(cleanUrlList)
d = ['http://www.prometheusgm.com/billboard',
 'http://www.cracked.com/',
 'www.wrc.com',
 'www.wwl.com',
 'www.kgw.com',
 'http://comicvine.gamespot.com/',
 'http://www.usgamer.net/',
 'http://www.opposingviews.com/contact',
 'http://www.bravotv.com',
 'http://www.kgbanswers.com/',
 'http://www.kvue.com/',
 'http://www.firstcoastnews.com/',
 'http://www.nbcphiladelphia.com/',
 'http://www.mmafighting.com/',
 'http://extreme.com/',
 'http://extreme.com/',
 'http://www.bumpclick.com/',
 'www.ufc.com',
 'http://www.meredith.com',
 'http://www.kcentv.com',
 'http://wgno.com/',
 'http://www.salon.com',
 'http://www.krem.com/',
 'http://www.nwcn.com/',
 'http://www.utopiandirect.com/',
 'http://getmommyfit.com.dnstree.com/',
 'http://www.13newsnow.com/',
 'http://www.wzzm13.com/',
 'http://wsbt.com/',
 'http://www.nametests.com/',
 'http://therednews.com/',
 'http://stayglam.com/',
 'http://www.juicerhead.com',
 'http://www.whatstrend.in/',
 'http://www.jobmonkeyjobs.com/',
 'http://lovethistrend.com/',
 'http://wbay.com/',
 'http://www.pronews.gr',
 'http://puppylovernews.com/',
 'http://wiat.com/',
 'http://wishtv.com/',
 'http://www.theremedist.com/',
 'http://wjbf.com/',
 'http://wlfi.com/',
 'http://en.gamevui.vn/',
 'http://littlemeows.com/',
 'http://www.influencive.com/',
 'http://www.libertatea.ro/',
 'http://www.wpbf.com/',
 'http://www.realitatea.net/',
 'http://www.dennis.co.uk',
 'http://www.forexlive.com/',
 'http://www.holytaco.com/',
 'http://www.ufc.com',
 'http://www.kokiku.tv/',
 'http://www.awaztoday.pk/',
 'http://extreme.com/',
 'http://extreme.com/',
 'http://extreme.com/',
 'http://extreme.com/',
 'http://www.kiro7.com/',
 'http://extreme.com/',
 'http://www.crushable.com/',
 'http://www.ktvb.com/',
 'http://www.stategazette.com/',
 'http://www.off-road.com/',
 'http://www.fultonsun.com/',
 'http://www.abc6.com/',
 'http://www.arkansas.com/',
 'http://lawikiguia.com/',
 'http://www.texarkanagazette.com/',
 'http://wereblog.com/',
 'http://whopostthis.com/',
 'http://www.whas11.com/',
 'http://www.social-peek.com/',
 'http://www.ky3.com/',
 'http://www.wlbz2.com/',
 'http://www.underwearexpert.com/',
 'http://www.unilad.co.uk/',
 'http://torontoist.com',
 'http://www.themangazine.com/',
 'https://www.zacks.com/',
 'http://ffxiv.consolegameswiki.com/',
 'http://supercurioso.com/',
 'http://www.sovrn.com/',
 'http://topyoutubers.net/',
 'http://in.investing.com/',
 'www.acutezmedia.com',
 'http://investmentwatchblog.com/',
 'http://www.favecrafts.com/',
 'http://lifetimesmi.com/',
 'http://mlb.mlb.com/',
 'http://www.qysh.me/',
 'http://www.houmatoday.com',
 'http://www.tv14.net/',
 'http://www.4029tv.com/',
 'http://fanbread.com/',
 'www.khou.com',
 'https://www.thenest.com/',
 'http://www.wwltv.com/',
 'http://citydescribed.com/',
 'http://extreme.com/',
 'www.ufc.com',
 'http://bibinews.com/']
# print "Total Clean Url {}".format(len(set(cleanUrlList)))
# d2 = set(cleanUrlList)
# d1 = set(d)
# d3 = list(set(d1)-set(d2))
# print d3

# import collections
# repeatedValue = [(item, count) for item, count in collections.Counter(
#     cleanUrlList).items() if count > 1]
# print "repeated url {}".format(len(repeatedValue))
# for x in (repeatedValue):
#     print x


l = ['http://www.mydesert.com/', 'http://www.myfoxchicago.com/', 'http://www.myfoxdfw.com/', 'http://www.myfoxmemphis.com/', 'http://www.myfoxny.com/', 'http://www.myfoxphilly.com/', 'http://www.myfoxphoenix.com/', 'http://www.myjewishlearning.com/', 'http://www.mylol.com/', 'http://www.myrtlebeachonline.com/', 'http://www.mystatebeautiful.com/', 'http://www.mytractorforum.com/', 'http://www.namasthetelangaana.com/', 'http://www.nametests.com/', 'http://www.nametests.com/', 'http://www.naplesnews.com/', 'http://www.natchezdemocrat.com/', 'http://www.nationalfootballpost.com/', 'http://www.nationalmemo.com/', 'http://www.nationalmemo.com/', 'http://www.nationalreview.com/', 'http://www.nationnews.com/', 'http://www.nbc.com/', 'http://www.nbc.com/', 'http://www.nbcbayarea.com/', 'http://www.nbcchicago.com/', 'http://www.nbcconnecticut.com/', 'http://www.nbclosangeles.com/', 'http://www.nbcmiami.com/', 'http://www.nbcnews.com/', 'http://www.nbcnews.com/', 'http://www.nbcnewyork.com/', 'http://www.nbcnewyork.com/', 'http://www.nbcot.com/', 'http://www.nbcot.org/', 'http://www.nbcot.org/', 'http://www.nbcphiladelphia.com/', 'http://www.nbcphiladelphia.com/', 'http://www.nbcsandiego.com/', 'http://www.nbcsports.com/', 'http://www.nbcsports.com/', 'http://www.nbcsports.com/', 'http://www.nbcstations.com/', 'http://www.nbcwashington.com/', 'http://www.nbcwashington.com/', 'http://www.ncaa.com/', 'http://www.ndtv.com/', 'http://www.ndtv.com/', 'http://www.ndtv.com/', 'http://www.necn.com/', 'http://www.neekly.com/', 'http://www.nerdwallet.com/', 'http://www.networkworld.com/', 'http://www.newjerseyhunter.com/', 'http://www.neworleans.com/', 'http://www.news-journalonline.com/', 'http://www.news-leader.com/', 'http://www.news-press.com/', 'http://www.news24nepal.tv/', 'http://www.news9.com/', 'http://www.news9.com/', 'http://www.news965.com/', 'http://www.newsandpromotions.com/', 'http://www.newsdzezimbabwe.co.uk/', 'http://www.newsfig.com/', 'http://www.newshijack.net/', 'http://www.newslo.com/', 'http://www.newsmax.com/', 'http://www.newsobserver.com/', 'http://www.newson6.com/', 'http://www.newsoxy.com/', 'http://www.newyorkupstate.com/', 'http://www.nextadvisor.com/', 'http://www.nfl.com/', 'http://www.ngalive.com/', 'http://www.nh1.com/', 'http://www.nintendolife.com/', 'http://www.nitansolutions.com/', 'http://www.nj.com/', 'http://www.nola.com/', 'http://www.northeasttimes.com/', 'http://www.notator.org/', 'http://www.notator.org/', 'http://www.noticias365.com/', 'http://www.nottinghampost.com/', 'http://www.nutrition.org.uk/', 'http://www.nwherald.com/', 'http://www.nydailynews.com/', 'http://www.nydailynews.com/', 'http://www.nymag.com/', 'http://www.nytimes.com/', 'http://www.observer-reporter.com/', 'http://www.ocala.com/', 'http://www.oddee.com/', 'http://www.oddee.com/', 'http://www.odditycentral.com/', 'http://www.ohfabuloso.com/', 'http://www.olimba.com/', 'http://www.oliveboard.in/', 'http://www.omfggossip.com/', 'http://www.omgfacts.com/', 'http://www.omgfreak.com/', 'http://www.omgviews.com/', 'http://www.one-europe.info/', 'http://www.onecountry.com/', 'http://www.onegreenplanet.org/', 'http://www.onenewsnow.com/', 'http://www.onlinestoresurveys.com/', 'http://www.onlinevideoconverter.com/', 'http://www.onlyinyourstate.com/', 'http://www.opposingviews.com/', 'http://www.oregon.com/', 'http://www.origjinale.al/', 'http://www.orlandoweekly.com/', 'http://www.out.com/', 'http://www.outdoorhub.com/', 'http://www.outsideonline.com/', 'http://www.packersnews.com/', 'http://www.palmbeachpost.com/', 'http://www.pandacat.com/', 'http://www.pappaspost.com/', 'http://www.parentingworld.net/', 'http://www.parismatch.com/', 'http://www.pasionaguila.com/', 'http://www.pasionaguila.com/', 'http://www.patheos.com/', 'http://www.patrika.com/', 'http://www.pawmygosh.com/', 'http://www.pcworld.com/', 'http://www.peekworthy.com/', 'http://www.pennlive.com/', 'http://www.performgroup.com/', 'http://www.performgroup.com/', 'http://www.pghcitypaper.com/', 'http://www.phillymag.com/', 'http://www.phillyvoice.com/', 'http://www.phonearena.com/', 'http://www.picayuneitem.com/', 'http://www.picksandparlays.net/', 'http://www.pinkvilla.com/', 'http://www.pix11.com/', 'http://www.playbuzz.com/', 'http://www.plus3network.com/', 'http://www.pnj.com/', 'http://www.polarisatvforums.com/', 'http://www.politico.com/', 'http://www.politico.com/', 'http://www.politicschatter.com/', 'http://www.polltracker.talkingpointsmemo.com/', 'http://www.pophitz.com/', 'http://www.popnhop.com/', 'http://www.popscreen.com/', 'http://www.popsugar.com/', 'http://www.popular.faceguia.com/', 'http://www.poughkeepsiejournal.com/', 'http://www.power953.com/', 'http://www.powerlineblog.com/', 'http://www.powershow.com/', 'http://www.powerstrokediesel.com/', 'http://www.press-citizen.com/', 'http://www.pressdemocrat.com/', 'http://www.pressherald.com/', 'http://www.pressroomvip.com/', 'http://www.primeromining.com/', 'http://www.pro-football-reference.com/', 'http://www.proceso.com.mx/', 'http://www.pronews.gr/', 'http://www.proudcons.com/', 'http://www.providr.com/', 'http://www.publicopiniononline.com/', 'http://www.purpleclover.com/', 'http://www.pushsquare.com/', 'http://www.q13fox.com/', 'http://www.qpolitical.com/', 'http://www.quirkyfeeds.com/', 'http://www.quirlycues.com/', 'http://www.quizbone.com/', 'http://www.quiznatic.com/', 'http://www.quizzzat.com/', 'http://www.rachaelray.com/', 'http://www.raidomia.com/', 'http://www.ram1500diesel.com/', 'http://www.randomsaladgames.com/', 'http://www.rantsports.com/', 'http://www.rap-up.com/', 'http://www.ratemyjob.com/', 'http://www.ratemyteachers.com/', 'http://www.rawstory.com/', 'http://www.rd.com/', 'http://www.rd.com/', 'http://www.realclearpolitics.com/', 'http://www.realclearpolitics.com/', 'http://www.realitatea.net/', 'http://www.realitywives.net/', 'http://www.realitywives.net/', 'http://www.realworldsurvivor.com/', 'http://www.rebootillinois.com/', 'http://www.recapo.com/', 'http://www.recetasparaadelgazar.com/', 'http://www.reckontalk.com/', 'http://www.recommendations.com/', 'http://www.rediff.com/', 'http://www.redpepper.co.ug/', 'http://www.redstate.com/', 'http://www.relativelyinteresting.com/', 'http://www.remedioscaserosweb.com/', 'http://www.rense.com/', 'http://www.reproworthy.com/', 'http://www.resharelist.com/', 'http://www.reshareworthy.com/', 'http://www.reuters.com/', 'http://www.reuters.com/', 'http://www.reviewjournal.com/', 'http://www.richestnews.com/', 'http://www.riodejaneiro.com/', 'http://www.rotoworld.com/', 'http://www.rsvlts.com/', 'http://www.rugerforum.com/', 'http://www.ruidosonews.com/', 'http://www.russellgrant.com/', 'http://www.s10forum.com/', 'http://www.sacbee.com/', 'http://www.sacurrent.com/', 'http://www.safesearch.school/', 'http://www.safetylevel.com/', 'http://www.salary.com/', 'http://www.salon.com/', 'http://www.salonmedia.com/', 'http://www.samuel-warde.com/', 'http://www.sandiegouniontribune.com/', 'http://www.sanfoundry.com/', 'http://www.sanluisobispo.com/', 'http://www.saturdaydownsouth.com/', 'http://www.saukvalley.com/', 'http://www.savagesports.co.uk/', 'http://www.savagesports.tv/', 'http://www.savagesports.tv/', 'http://www.savagesports.tv/', 'http://www.sawfirst.com/', 'http://www.saymedia.com/', 'http://www.scarymommy.com/', 'http://www.schurz.com/', 'http://www.sciencealert.com/', 'http://www.scoop.it/', 'http://www.scoopwhoop.com/', 'http://www.scotsman.com/', 'http://www.searchhomeremedy.com/', 'http://www.secrant.com/', 'http://www.secretchina.com/', 'http://www.semesterz.com/', 'http://www.semissourian.com/', 'http://www.sequelsva.com/', 'http://www.seriouseats.com/', 'http://www.seth-rogen.com/', 'http://www.sfexaminer.com/', 'http://www.sfgate.com/', 'http://www.sharethread.com/', 'http://www.sharetofu.com/', 'http://www.shedthoselbs.com/', 'http://www.shekulli.com.al/', 'http://www.sheldonsfans.com/', 'http://www.sherdog.com/', 'http://www.shreveporttimes.com/', 'http://www.shtfplan.com/', 'http://www.shtfplan.com/', 'http://www.shtyle.fm/', 'http://www.sify.com/', 'http://www.sikkimexotica.com/', 'http://www.silentyarn.com/', 'http://www.siliconindia.com/', 'http://www.singaporeanbiz.com/', 'http://www.singaporefriendly.com/', 'http://www.singaporemusicguide.com/', 'http://www.sizzlingfeed.com/', 'http://www.sliptalk.com/', 'http://www.slipups.com/', 'http://www.smash.com/', 'http://www.smithsonianmag.com/', 'http://www.snapchat.codes/', 'http://www.sniperforums.com/', 'http://www.snowboarding.com/', 'http://www.soafanatic.com/', 'http://www.social-peek.com/', 'http://www.socialdunks.com/', 'http://www.socialholicnetwork.com/', 'http://www.socialsweethearts.de/', 'http://www.socialsweethearts.de/', 'http://www.socialsweethearts.de/', 'http://www.socialsweethearts.de/', 'http://www.socialtheater.com/', 'http://www.sofeminine.co.uk/', 'http://www.sooziq.com/', 'http://www.soundcoolmusic.com/', 'http://www.soundpublishing.com/', 'http://www.southflorida.com/', 'http://www.sovrn.com/', 'http://www.spaste.com/', 'http://www.spiritdaily.com/', 'http://www.sportdog.gr/', 'http://www.sportingnews.com/', 'http://www.sportingz.com/', 'http://www.sports-reference.com/', 'http://www.sportsblog.com/', 'http://www.sportsbreak.com/', 'http://www.sportsgrid.com/', 'http://www.sportskeeda.com/', 'http://www.sportsplays.com/', 'http://www.sqlservercentral.com/', 'http://www.stabroeknews.com/', 'http://www.standard.co.uk/', 'http://www.standardmedia.co.ke/', 'http://www.star-telegram.com/', 'http://www.starmock.com/', 'http://www.starnewsonline.com/', 'http://www.startribune.com/', 'http://www.stategazette.com/', 'http://www.statenisland.com/', 'http://www.statesman.com/', 'http://www.statesmanjournal.com/', 'http://www.statisticbrain.com/', 'http://www.statusbomb.com/', 'http://www.steelersdepot.com/', 'http://www.stereogum.com/', 'http://www.stiripesurse.ro/', 'http://www.stlmugshots.com/', 'http://www.stltoday.com/', 'http://www.stophillarypac.org/', 'http://www.story.com/', 'http://www.streetinsider.com/', 'http://www.stromtrooper.com/', 'http://www.stylebistro.com/', 'http://www.stylemotivation.com/', 'http://www.subaruforester.org/', 'http://www.suffolknewsherald.com/', 'http://www.suggest.com/', 'http://www.sunherald.com/', 'http://www.sunnyskyz.com/', 'http://www.superstarmagazine.com/', 'http://www.superstarmagazine.com/', 'http://www.sureawesomeness.com/', 'http://www.swedespeed.com/', 'http://www.sweepstakesfordays.com/', 'http://www.t-shirtforums.com/', 'http://www.t3.com/', 'http://www.tactical-life.com/', 'http://www.tallahassee.com/', 'http://www.tallmadgeexpress.com/', 'http://www.tamedia.ch/', 'http://www.tamilstar.com/', 'http://www.tamiltvshows.net/', 'http://www.taosnews.com/', 'http://www.tapology.com/', 'http://www.taringa.net/', 'http://www.tasteofhome.com/', 'http://www.tasteofhome.com/', 'http://www.tbo.com/', 'http://www.teaparty.com/', 'http://www.techconsumer.com/', 'http://www.technologypapa.com/', 'http://www.techradar.com/', 'http://www.techrepublic.com/', 'http://www.techrepublic.com/', 'http://www.techsatish.com/', 'http://www.techsupportforum.com/', 'http://www.tegna.com/', 'http://www.tegna.com/', 'http://www.tegna.com/', 'http://www.tegnafoundation.org/', 'http://www.telegraph.co.uk/', 'http://www.tellychakkar.com/', 'http://www.telugu.awaitnews.com/', 'http://www.teluguwall.com/', 'http://www.tennessean.com/', 'http://www.texarkanagazette.com/', 'http://www.thaivisa.com/', 'http://www.the-dispatch.com/', 'http://www.theadvocate.com/', 'http://www.theamericanconservative.com/', 'http://www.theamericanconservative.com/', 'http://www.theamericanmirror.com/', 'http://www.theartikulounonews.com/', 'http://www.theatlantic.com/', 'http://www.theatlantic.com/', 'http://www.thebaseballcube.com/', 'http://www.thebassbarn.com/', 'http://www.theblacksheartimes.com/', 'http://www.theblaze.com/', 'http://www.thebraiser.com/', 'http://www.thechive.com/', 'http://www.thecrux.com/', 'http://www.thedailybeast.com/', 'http://www.thedailyiq.com/', 'http://www.thedailypedia.com/', 'http://www.thedieselstop.com/', 'http://www.theeverygirl.com/', 'http://www.thefactsite.com/', 'http://www.thefiscaltimes.com/', 'http://www.thegloss.com/', 'http://www.thegrandreport.com/', 'http://www.thegrumpyfish.com/', 'http://www.theguardian.com/', 'http://www.theherald-news.com/', 'http://www.theherald.com.au/', 'http://www.thehollywoodgossip.com/', 'http://www.thehollywoodgossip.com/', 'http://www.theladbible.com/', 'http://www.theleafchronicle.com/', 'http://www.thelocal.com/', 'http://www.thelocal.com/', 'http://www.thelocal.de/', 'http://www.thelocal.dk/', 'http://www.thelocal.fr/', 'http://www.thelocal.it/', 'http://www.thelocal.se/', 'http://www.themalaymailonline.com/', 'http://www.themangazine.com/', 'http://www.themarysue.com/', 'http://www.themeparktourist.com/', 'http://www.themindinspired.org/', 'http://www.thenewcivilrightsmovement.com/', 'http://www.theneworleansadvocate.com/', 'http://www.theneworleansadvocate.com/', 'http://www.thenewstribune.com/', 'http://www.theolympian.com/', 'http://www.theonion.com/', 'http://www.theoutdoorstrader.com/', 'http://www.thepoke.co.uk/', 'http://www.therebel.media/', 'http://www.theremedist.com/', 'http://www.theresourcegirls.com/', 'http://www.therichest.com/', 'http://www.therockwfk.com/', 'http://www.theshrug.com/', 'http://www.thesmokinggun.com/', 'http://www.thesportreview.com/', 'http://www.thestar.co.uk/', 'http://www.thestarpress.com/', 'http://www.thestranger.com/', 'http://www.thetimesherald.com/', 'http://www.thevisitor.co.uk/', 'http://www.thewalkingdead.com/', 'http://www.thewashingtondailynews.com/', 'http://www.thewrap.com/', 'http://www.thinkhealthier.com/', 'http://www.thisblewmymind.com/', 'http://www.threedollarclick.com/', 'http://www.thv11.com/', 'http://www.tickld.com/', 'http://www.tigerdroppings.com/', 'http://www.tigernet.com/', 'http://www.timesgazette.com/', 'http://www.timesofindia.indiatimes.com/', 'http://www.timesofisrael.com/', 'http://www.timesrecordnews.com/', 'http://www.timetobreak.com/', 'http://www.timewarnercable.com/', 'http://www.tipsybartender.com/', 'http://www.tmnews.com/', 'http://www.tmonews.com/', 'http://www.tmz.com/', 'http://www.tmz.com/', 'http://www.todayifoundout.com/', 'http://www.todaysbuzz.com/', 'http://www.todaysbuzz.com/', 'http://www.tomorrowoman.com/', 'http://www.topdrawersoccer.com/', 'http://www.topeak.com/', 'http://www.topnews.in/', 'http://www.tor-fashion.com/', 'http://www.torqueboss.com/', 'http://www.torqueboss.com/', 'http://www.townandcountrymag.com/', 'http://www.trapshooters.com/', 'http://www.treasurenet.com/', 'http://www.trend-chaser.com/', 'http://www.trendelier.com/', 'http://www.tribunemedia.com/', 'http://www.tribunemedia.com/', 'http://www.tribunemedia.com/', 'http://www.triumphrat.net/', 'http://www.trivalleycentral.com/', 'http://www.triviahive.com/', 'http://www.troymessenger.com/', 'http://www.trx250r.net/', 'http://www.tupaki.com/', 'http://www.tuscaloosanews.com/', 'http://www.tv.com/', 'http://www.tv.indonewyork.com/', 'http://www.tvfanatic.com/', 'http://www.tvguide.com/', 'http://www.tvguide.com/', 'http://www.twcc.com/', 'http://www.twitlonger.com/', 'http://www.tyronetimes.co.uk/', 'http://www.udayavani.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ufc.com/', 'http://www.ummat.com.pk/', 'http://www.underwearexpert.com/', 'http://www.unilad.co.uk/', 'http://www.unmotivating.com/', 'http://www.urbanjoker.com/', 'http://www.urbanjoker.com/', 'http://www.urdogs.com/', 'http://www.urtech.ca/', 'http://www.usatoday.com/', 'http://www.usatoday.com/', 'http://www.usgamer.net/', 'http://www.usmessageboard.com/', 'http://www.usnews.com/', 'http://www.usnews.com/', 'http://www.usports.sg/', 'http://www.uspresidentialelectionnews.com/', 'http://www.utahsright.com/', 'http://www.v4vartha.com/', 'http://www.vagazette.com/', 'http://www.valavideo.com/', 'http://www.vancitybuzz.com/', 'http://www.vanguardngr.com/', 'http://www.vcstar.com/', 'http://www.veritenews.com/', 'http://www.verticalscope.com/', 'http://www.verticalscope.com/', 'http://www.verticalscope.com/', 'http://www.verticalscope.com/', 'http://www.verticalscope.com/', 'http://www.vervemobile.com/', 'http://www.vervemobile.com/', 'http://www.vervemobile.com/', 'http://www.veteranstoday.com/', 'http://www.vhnd.com/', 'http://www.vicksburgpost.com/', 'http://www.vidbull.me/', 'http://www.vidzvid.com/', 'http://www.vintag.es/', 'http://www.viralmoon.net/', 'http://www.viralnova.com/', 'http://www.viralnova.com/', 'http://www.viralpie.net/', 'http://www.viralpiranha.com/', 'http://www.viralpiranha.com/', 'http://www.viralpiranha.com/', 'http://www.viralpursuit.com/', 'http://www.viralwalrus.com/', 'http://www.viralwomen.com/', 'http://www.virginiamn.com/', 'http://www.virtualjerusalem.com/', 'http://www.vizio.com/', 'http://www.vocativ.com/', 'http://www.vozviral.net/', 'http://www.vulture.com/', 'http://www.vulture.com/', 'http://www.wackyy.net/', 'http://www.waitup.com/', 'http://www.wakeboardingmag.com/', 'http://www.walesonline.co.uk/', 'http://www.walleyecentral.com/', 'http://www.warpedspeed.com/', 'http://www.warriorzen.com/', 'http://www.washingtontimes.com/', 'http://www.watchtheyard.com/', 'http://www.wattalyf.com/', 'http://www.wbal.com/', 'http://www.wbaltv.com/', 'http://www.wbir.com/', 'http://www.wboc.com/', 'http://www.wcnc.com/', 'http://www.wcsh6.com/', 'http://www.wcvb.com/', 'http://www.wdsu.com/', 'http://www.webdunia.com/', 'http://www.webecoist.momtastic.com/', 'http://www.webtreeonline.com/', 'http://www.weeklyviral.com/', 'http://www.wehuntedthemammoth.com/', 'http://www.wellbuzz.com/', 'http://www.weqyoua.net/', 'http://www.wesh.com/', 'http://www.wesh.com/', 'http://www.westchestermagazine.com/', 'http://www.westernmassnews.com/', 'http://www.wetpaint.com/', 'http://www.wfaa.com/', 'http://www.wfmj.com/', 'http://www.wfsb.com/', 'http://www.wftv.com/', 'http://www.wftv.com/', 'http://www.wgal.com/', 'http://www.wgal.com/', 'http://www.whattoexpect.com/', 'http://www.whec.com/', 'http://www.wherevent.com/', 'http://www.whio.com/', 'http://www.whio.com/', 'http://www.whiskeyriff.com/', 'http://www.whnt.com/', 'http://www.wholehogsports.com/', 'http://www.whotv.com/', 'http://www.wideopencountry.com/', 'http://www.wideopenspaces.com/', 'http://www.wideopenspaces.com/', 'http://www.widget.com/', 'http://www.wikia.com/', 'http://www.wikihow.com/', 'http://www.wikipicky.com/', 'http://www.winloot.com/', 'http://www.wisn.com/', 'http://www.wisn.com/', 'http://www.wizzed.com/', 'http://www.wktv.com/', 'http://www.wkyc.com/', 'http://www.wlbz2.com/', 'http://www.wlky.com/', 'http://www.wltx.com/', 'http://www.wlwt.com/', 'http://www.wlwt.com/', 'http://www.wmata.com/', 'http://www.wmtw.com/', 'http://www.wmur.com/', 'http://www.wnd.com/', 'http://www.wnem.com/', 'http://www.wnep.com/', 'http://www.wokv.com/', 'http://www.womens-forum.com/', 'http://www.wonderwall.com/', 'http://www.wonderwall.com/', 'http://www.worldlifestyle.com/', 'http://www.worldlifestyle.com/', 'http://www.worldviewsph.com/', 'http://www.wownowiknow.com/', 'http://www.wowticles.com/', 'http://www.wpbeginner.com/', 'http://www.wpbf.com/', 'http://www.wptz.com/', 'http://www.wpworking.com/', 'http://www.wpxi.com/', 'http://www.wpxi.com/', 'http://www.wqad.com/', 'http://www.wrcbtv.com/', 'http://www.wreg.com/', 'http://www.wrestlingforum.com/', 'http://www.wsbtv.com/', 'http://www.wsbtv.com/', 'http://www.wsj.com/', 'http://www.wsmv.com/', 'http://www.wsoctv.com/', 'http://www.wsoctv.com/', 'http://www.wtae.com/', 'http://www.wtffunfact.com/', 'http://www.wthr.com/', 'http://www.wtkr.com/', 'http://www.wusa9.com/', 'http://www.wwl.com/', 'http://www.wwltv.com/', 'http://www.wxii12.com/', 'http://www.wzzm13.com/', 'http://www.xfinity.com/', 'http://www.yahoo.com/', 'http://www.ydr.com/', 'http://www.yesimright.com/', 'http://www.yespets.com/', 'http://www.youpak.com.pk/', 'http://www.yourdailydip.com/', 'http://www.yourdailydip.com/', 'http://www.yourdailydish.com/', 'http://www.yourdailydish.com/', 'http://www.yourfunapp.com/', 'http://www.yuni.com/', 'http://www.zelda.com/', 'http://www.zengardner.com/', 'http://www.zerocensorship.com/', 'http://www.zestvip.com/', 'http://www.ziffdavis.com/', 'http://www.zimbabwesituation.com/', 'http://www2.kusports.com/', 'http://www2.ljworld.com/', 'http://www5.narutoget.io/', 'http://wzakcleveland.hellobeautiful.com/', 'http://xobenzo.com/', 'http://yamstar.com/', 'http://youngest_mother.tripod.com/', 'http://yourblackworld.net/', 'http://yourdailyjournal.com/', 'http://yourspace.reporternews.com/', 'http://youseeingthis.com/', 'http://zap2it.com/', 'https://arabic.rt.com/', 'https://arabic.rt.com/', 'https://avacadell.com/', 'https://blackthen.com/', 'https://cdanews.com/', 'https://constitutionalrightspac.com/', 'https://developer.apple.com/', 'https://disqus.com/', 'https://disqus.com/', 'https://disqus.com/', 'https://disqus.com/', 'https://disqus.com/', 'https://doyouremember.com/', 'https://epom.com/', 'https://extremefreestyle.wordpress.com/', 'https://fitnessrepublic.com/', 'https://help.disqus.com/', 'https://help.disqus.com/', 'https://hooch.co/', 'https://investmentwatchblog.com/', 'https://kinja.com/', 'https://mail.cloudzimail.com/', 'https://malwaretips.com/', 'https://mobileposse.com/', 'https://multimedia.journalism.berkeley.edu/', 'https://nepalkokhabar.wordpress.com/', 'https://new.digi.com./', 'https://nobullying.com/', 'https://orders.grinandbearit.com.au/', 'https://pages.questexweb.com/', 'https://play.google.com/', 'https://secure.trump2016.com/', 'https://skepchick.org/', 'https://ssl1.gmti.com/', 'https://stations.fcc.gov/', 'https://subscene.com/', 'https://support.office.com/', 'https://sweepstakesalerts.com/', 'https://theconservativetreehouse.com/', 'https://themetix.com/', 'https://trendingstylist.com/', 'https://twinfinite.net/', 'https://uplay.ubi.com/', 'https://wordpress.com/', 'https://www.accesshollywood.com/', 'https://www.bikerornot.com/', 'https://www.bungie.net/', 'https://www.camfrog.com/', 'https://www.demandmedia.com/', 'https://www.demandmedia.com/', 'https://www.eaa.unsw.edu.au/', 'https://www.eaa.unsw.edu.au/', 'https://www.englishandmedia.co.uk/', 'https://www.ezoic.com/', 'https://www.facebook.com/', 'https://www.formula1.com/', 'https://www.gomlab.com/', 'https://www.hypestat.com\\/', 'https://www.maplesoft.com/', 'https://www.mentor.com/', 'https://www.microsoft.com/', 'https://www.nerdwallet.com/', 'https://www.newzealandnow.govt.nz/', 'https://www.nigeriafilms.com/', 'https://www.nigerianbulletin.com/', 'https://www.offgridworld.com/', 'https://www.pinterest.com/', 'https://www.playboy.com/', 'https://www.ranksays.com/', 'https://www.roadsnacks.ne/', 'https://www.rt.com/', 'https://www.savingadvice.com/', 'https://www.stgeorgeutah.com/', 'https://www.theguardian.com/', 'https://www.theinternetworks.co.uk/', 'https://www.thenest.com/', 'https://www.threedollarclick.com/', 'https://www.threedollarclick.com/', 'https://www.uswitch.com/', 'https://www.vegas.com/', 'https://www.vg247.com/', 'https://www.visitengland.com/', 'https://www.weekendcollective.com/', 'https://www.youtube.com/', 'https://www.zacks.com/', 'https://www.ziffdavis.com/', 'https://xtremesport4u.wordpress.com/', 'https://yurn.it/']
print len(set(l))

print set(l)