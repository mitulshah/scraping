from mozscape import Mozscape
import psycopg2
import psycopg2.extras
import time
client = Mozscape(
    'mozscape-fda72603a2',
    '22539923fbb4d5f9cec0ad7338b155a1')


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper

alldata = []


def main(url):
    try:
        authorities = client.urlMetrics([url],
                                        Mozscape.UMCols.domainAuthority | Mozscape.UMCols.pageAuthority)
    except Exception as e:
        print "This url has issue".format(url)
    try:
        pda = authorities[0]['pda']
    except Exception as e:
        pda = 0

    try:
        upa = authorities[0]['upa']
    except Exception as e:
        upa = 0

    alldata.append((url, pda, upa))
    with open('mozscapedata.txt', 'w') as f:
        f.write(str(alldata))


@redshift_dbconn
def insertationProcess():
    urllist = getUrls()

    for url in urllist:
        main(url[0])
        time.sleep(10)

    try:
        cur.executemany(
            "INSERT into atomic.magnify_model_mozscape (domain_url, domain_authority, page_authority) values(%s, %s, %s)", alldata)
    except Exception as e:
        print e


def getUrls():
    """
    fetch all urls which is stoted into aws redshift database
    """
    cur.execute("""SELECT distinct siteurl from magnify_siteurls where siteurl NOT IN (select domain_url from atomic.magnify_model_mozscape ) AND isdeleted=0""")
    rows = cur.fetchall()
    return rows

insertationProcess()
