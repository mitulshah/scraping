import psycopg2
import psycopg2.extras
# import time
# import csv

alldata = []


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


def getData():
    """
    fetch data
    """
    query = """select 
    mg.refr_urlpath, mg.siteurl, mg.taboola_name, al.global_rank,  al.us_rank, al.male, al.female, al.nocollege, al.somecollege, al.college, al.graduatecollege,  al.age18_24, al.age25_34, al.age35_44, al.age45_54, al.age55_64, al.age65, 
    al.income0_30K, al.income30_60K, al.income60_100K, al.income100K, al.african, al.african_american, al.asian, al.caucasian, al.hispanic, al.middle_eastern, al.other,
    ggl.speedscore_mobile, ggl.usability_score, ggl.speedscore_desktop, ms.domain_authority, ms.page_authority
    from magnify_siteurls mg 
    inner join atomic.magnify_model_alexa_demography al ON mg.siteurl = al.domain_url 
    inner join atomic.magnify_model_googleapi ggl ON mg.siteurl = ggl.domain_url 
    inner join atomic.magnify_model_mozscape ms ON mg.siteurl = ms.domain_url 
    where mg.isdeleted=0 """
    cur.execute(query)
    rows = cur.fetchall()
    # print(rows)
    for x in rows:
        alldata.append(tuple(x))


@redshift_dbconn
def insertationProcess():
    getData()
    try:
        cur.executemany(
            "INSERT into atomic.magnify_mapping_url (referring_url, url, taboola_name, global_site_ranking, us_site_ranking, male, female, no_college, some_college, college, graduate_school, age_18_24, age_25_34, age_35_44, age_45_54, age_55_64, age_65, income_0_30K, income_30K_60K, income_60K_100K, income_100K, race_african, race_african_american, race_asian, race_caucasian, race_hispanic, race_middle_eastern, race_other, speedscore_mobile, usability_score, speedscore_desktop, domain_authority, page_authority) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", alldata)
    except Exception as e:
        print e


insertationProcess()
