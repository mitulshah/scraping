import urllib2
import urllib
import ast
import psycopg2
import psycopg2.extras
import time


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        conn.commit()
        # close db connection

        conn.close()

    return func_wrapper

alldata = []


def main(url1):
    url = urllib.quote_plus(url1)
    if not url.startswith('http'):
        url = 'http://' + url
    try:
        googleurlmbl = "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url={}&filter_third_party_resources=false&strategy=mobile&key=AIzaSyCZfiD8wFAwABlK-Pi-8uxzqoH458T0jG4".format(
            url)

        resmbl = urllib2.urlopen(googleurlmbl)
        responsembl = ast.literal_eval(resmbl.read())

        googleurldesktop = "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url={}&filter_third_party_resources=false&strategy=desktop&key=AIzaSyCZfiD8wFAwABlK-Pi-8uxzqoH458T0jG4".format(
            url)
        resdesktop = urllib2.urlopen(googleurldesktop)
        responsedesktop = ast.literal_eval(resdesktop.read())
    except Exception as e:
        print "This one has issue {}".format(url1)

    try:
        speedscore_mobile = responsembl['ruleGroups']['SPEED']['score']
    except Exception as e:
        speedscore_mobile = 0

    try:
        usability_score = responsembl['ruleGroups']['USABILITY']['score']
    except Exception as e:
        usability_score = 0

    try:
        speedscore_desktop = responsedesktop['ruleGroups']['SPEED']['score']
    except Exception as e:
        speedscore_desktop = 0

    alldata.append((url1,
                    speedscore_mobile,
                    usability_score,
                    speedscore_desktop))

    with open('googleapidata.txt', 'w') as f:
        f.write(str(alldata))


def getUrls():
    """
    fetch all urls which is stoted into aws redshift database
    """
    cur.execute("""SELECT distinct siteurl from magnify_siteurls where siteurl NOT IN (select domain_url from atomic.magnify_model_googleapi ) AND isdeleted=0""")
    rows = cur.fetchall()
    return rows


@redshift_dbconn
def insertationProcess():
    urllist = getUrls()
    for x in urllist:
        main(x[0])
    print alldata
    try:
        cur.executemany(
            "INSERT into atomic.magnify_model_googleapi (domain_url, speedscore_mobile, usability_score, speedscore_desktop) values(%s, %s, %s, %s)", alldata)
    except Exception as e:
        print e


insertationProcess()
