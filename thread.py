import threading


def worker(num):
    """thread worker function"""
    print 'Worker: %s' % num
    return

threads = []
for i in range(500):
    t = threading.Thread(target=worker, args=(i,))
    threads.append(t)
    t.start()

for i in range(500):
    print i
