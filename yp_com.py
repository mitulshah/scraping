# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import urllib2
import re
import MySQLdb
import ast
from itertools import cycle
import logging
from math import ceil
import time


states = ['AL-1', 'AK-1', 'AZ-1', 'AR-51', 'CA-1', 'CO-1', 'CT-1', 'DE-13','FL-1', 'GA-1', 'HI-1', 'ID-1', 'IL-1', 'IN-1',
          'IA-1', 'KS-1', 'KY-1', 'LA-1', 'ME-1', 'MD-1', 'MA-1', 'MI-1', 'MN-1', 'MS-1',
          'MO-1', 'MT-1', 'NE-1', 'NV-1', 'NH-1', 'NJ-1', 'NM-1', 'NY-1', 'NC-1', 'ND-1',
          'OH-1', 'OK-1', 'OR-1', 'PA-1', 'RI-1', 'SC-1', 'SD-1', 'TN-1', 'TX-1', 'UT-1',
          'VT-1', 'VA-1', 'WA-1', 'WV-1', 'WI-1', 'WY-1']

# for logging
LOG_FILENAME = 'yellowpages.log'
logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s %(name)s %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M',
                    filename='logs/{}'.format(LOG_FILENAME))


# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '70.248.28.13:8080'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)


def yellowpages_scraping(state, currentpage, total_pages=1,
                         totalresult=0, retry=0, page_not_found=0):
    if currentpage > 101:
        return True

    mainlst = []
    base_url = "http://www.yellowpages.com/search?search_terms=agriculture&geo_location_terms=%s&s=name&page=%s" % (
        state, str(currentpage))
    print(base_url)
    # Aquire data from URL
    request = urllib2.Request(base_url, headers=hdr)
    status = False
    if page_not_found < 7:
        page_not_found += 1
        try:
            result = urllib2.urlopen(request)
            status = True
        except urllib2.HTTPError, err:

            print err.code
            print "Trying again"
            yellowpages_scraping(
                state, currentpage, total_pages, totalresult, 0, page_not_found)

        except Exception as e:
            print "error is ", e
            yellowpages_scraping(
                state, currentpage, total_pages, totalresult, 0, page_not_found)
    else:
        print "Sorry!! {} is not responding.. Request for page {}..!!!".format(state, currentpage + 1)
        yellowpages_scraping(
            state, currentpage+1, total_pages, totalresult, 0, 0)
    if status and result.getcode() == 200:
        print state + " :: started on " + time.ctime() + " page :: " + str(currentpage)
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')
        # soup.prettify(formatter=lambda soup: soup.replace(u'\xa0', ' '))
        try:
            if total_pages == 1:
                # fetch the total result when any new state is comming
                pagetext = soup.find('div', {'class': 'pagination'}).find('p').text
                ofIndex = pagetext.index('of ') + len('of ')
                resultIndex = pagetext.index('results')
                totalresult = pagetext[ofIndex:resultIndex]
                total_pages = int(ceil(int(totalresult)/30))

            noresult = soup.find_all('div', {'class': 'no-results-main'})
            if len(noresult) > 0:
                return False

            blocks = soup.find_all('div', {'class': 'info'})
            if not len(blocks):
                if retry < 3:
                    print "Alert -- Trying {} time to get result from {} of {}".format(
                        retry, currentpage, state)
                    retry += 1
                    yellowpages_scraping(
                        state, currentpage, total_pages, totalresult, retry)
                else:
                    print "Warning !!! Page {} is skipped for {}".format(currentpage, state)
                    logging.debug(
                        {'error': 'Page is skipped due to lack of result',
                         'state': state, 'page': currentpage})
                    yellowpages_scraping(
                        state, currentpage+1, total_pages, totalresult, 0)

            for element in blocks:
                # fetch the number of record
                numberString = element.find('h3', {'class': 'n'}).text
                number = numberString.split('.')[0]

                # fetch the link for inner page
                anchortag = element.find('a', href=True)
                innerpagelink = 'http://www.yellowpages.com' + anchortag['href']
                try:
                    mainlst.append(
                        fetch_innerpage_content(innerpagelink, state, currentpage))
                except Exception as e:
                    mainlst.append(
                        fetch_innerpage_content(innerpagelink, state, currentpage))

                # print "{} -- {} out of {} records are fetched.".format(
                #     state, number, totalresult)

            # start storing the data for every single pages
            db = MySQLdb.connect(
                "localhost", "root", "XmTW9rsc", "scrape",
                use_unicode=True, charset='utf8')
            # prepare a cursor object using cursor() method
            cursor = db.cursor()
            for item in mainlst:
                # print(item)
                if item:
                    try:
                        cursor.execute("""INSERT into yp_com (business_name,
                            state, latitude, longitude, street_address,
                            city_state, phone, website, email,
                            years_in_business, daytime, description,
                            servicesOrProducts,
                            associations, aka, brands, payment, categories, url
                            ) values (%s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, 
                            %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
                            str(item[0]), str(item[1]), item[2], item[3],
                            str(item[4]), str(item[5]), str(item[6]),
                            str(item[7]), str(item[8]), str(item[9]),
                            str(item[10]), str(item[11]), str(item[12]),
                            str(item[13]), str(item[14]), str(item[15]),
                            str(item[16]), str(item[17]), str(item[18])))
                        db.commit()
                    except Exception as e:
                        logging.error({'error': e, 'url': item[18],
                                       'response': item, 'pageNo': currentpage})
            # disconnec.encode('utf-8')t from server
            db.close()
            print "{}, {} pages are stored in db..!!!".format(state, currentpage)
        except Exception as e:
            logging.error({'error': e, 'url': base_url,
                           'pageNo': currentpage})
        # call the function again if next page is exist.
        if total_pages > currentpage:
            yellowpages_scraping(state, int(currentpage)+1, total_pages,
                                 totalresult, 0)


def fetch_innerpage_content(innerpagelink, state, currentpage):

    innerRequest = urllib2.Request(innerpagelink, headers=hdr)
    try:
        innerResult = urllib2.urlopen(innerRequest)

        if innerResult.getcode() == 200:
            innerData = innerResult.read()
            innerSoup = BeautifulSoup(innerData, 'html.parser')
            innerSoup.prettify(formatter=lambda s: s.replace(u'\xa0', ' '))

            # print "{} -- {}".format(number, state)
            # number += 1
            first_paragraph = innerSoup.find(
                'div', {'class', 'business-card'})

            try:
                business_name = first_paragraph.find('h1').text
            except:
                business_name = ''

            try:
                latlong = first_paragraph['data-pushpin']
                latitude = ast.literal_eval(latlong)['lat']
                longitude = ast.literal_eval(latlong)['lon']
            except:
                latitude = ''
                longitude = ''

            try:
                street_address = first_paragraph.find(
                    'p', {'class', 'street-address'}).text
            except:
                street_address = ''

            try:
                city_state = first_paragraph.find(
                    'p', {'class', 'city-state'}).prettify(formatter="html")
                city_state = city_state.text
            except:
                city_state = ''

            try:
                phone = first_paragraph.find(
                    'p', {'class', 'phone'}).text
            except:
                phone = ''

            try:
                websiteTag = first_paragraph.find(
                    'a', {'class': 'custom-link'}, href=True)
                website = websiteTag['href']
            except:
                website = ''

            try:
                emailTag = first_paragraph.find(
                    'a', {'class': 'email-business'}, href=True)
                email = emailTag['href'].split('mailto:')[1]
            except:
                email = ''

            try:
                years_in_business = ''
                for yrs_in_business in innerSoup.find_all('section', {'class': 'years-in-business'}):
                    years_in_business = yrs_in_business.find(
                        'p', {'class': 'count'}).text
            except:
                years_in_business = ''

            second_paragraph = innerSoup.find_all('dl')[0]
            third_paragraph = innerSoup.find_all('dl')[1]
            # print second_paragraph.parent.name
            if second_paragraph.parent.name == 'section':
                daytime = []
                try:
                    for time in second_paragraph.find_all('time'):
                        daytime.append(time['datetime'])
                except:
                    daytime = []

                try:
                    description_d = second_paragraph.find(
                        'dd', {'class': 'description'}).prettify(formatter="html")
                    description = description_d.text
                except:
                    description = ''

                try:
                    service = third_paragraph.find(
                        'dt', text=re.compile('Services/Products:'))
                    servicesOrProducts = service.nextSibling.string
                except:
                    servicesOrProducts = ''

                try:
                    associations = third_paragraph.find(
                        'dd', {'class': 'associations'}).text
                except:
                    associations = ''

                try:
                    aka = third_paragraph.find(
                        'dd', {'class': 'aka'}).text
                except:
                    aka = ''

                try:
                    brands = third_paragraph.find(
                        'dd', {'class': 'brands'}).text
                except:
                    brands = ''

                try:
                    payment = third_paragraph.find(
                        'dd', {'class': 'payment'}).text
                except:
                    payment = ''

                try:
                    categoriesTag = third_paragraph.find(
                        'dd', {'class': 'categories'}).find_all('a')
                except:
                    categoriesTag = []
                    # print(second_paragraph, third_paragraph,  innerpagelink)
                    # print("********")

                categories = []
                for category in categoriesTag:
                    categories.append(category.text)

                return [business_name, state, latitude, longitude,
                        street_address, city_state, phone, website, email,
                        years_in_business, daytime, description,
                        servicesOrProducts, associations, aka, brands,
                        payment, categories, innerpagelink]

    except urllib2.HTTPError, err:
        logging.error(
            {'error': err, 'pagelink': innerpagelink, 'state': state, 'pageNo': currentpage})
        print "Trying again"

    except Exception as e:
        logging.error(
            {'error': e, 'pagelink': innerpagelink, 'state': state, 'pageNo': currentpage})

for stateObj in states:
    stateval, pageval = tuple(stateObj.split('-'))
    # print stateval, pageval
    yellowpages_scraping(stateval, int(pageval))
    print "==#== {} has been completed. ==*==".format(stateval)
