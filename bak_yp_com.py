from bs4 import BeautifulSoup
import requests
import urllib2
import re
import MySQLdb
import ast
from itertools import cycle
import logging

completed_state = ['AL', 'AK', 'AZ', 'AR']
states = ['CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
          'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']

LOG_FILENAME = 'yallowpages.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)


def fetch_data_using_scraping(state, page, proxyUrl, number):

    mainlst = []
    base_url = "http://www.yellowpages.com/search?search_terms=agriculture&geo_location_terms=%s&s=name&page=%s" % (
        state, str(page))
    hdr = {"User-Agent":
           "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
    proxy = urllib2.ProxyHandler({'http': proxyUrl})

    # Create an URL opener utilizing proxy
    opener = urllib2.build_opener(proxy)
    urllib2.install_opener(opener)

    # Aquire data from URL
    request = urllib2.Request(base_url, headers=hdr)
    result = urllib2.urlopen(request)
    if result.getcode() == 200:
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')

    for element in soup.find_all('div', {'class': 'info'}):
        anchorTag = element.find('a', href=True)
        actual_link = 'http://www.yellowpages.com' + anchorTag['href']
        innerRequest = urllib2.Request(actual_link, headers=hdr)
        innerResult = urllib2.urlopen(innerRequest)
        if innerResult.getcode() == 200:
            innerData = innerResult.read()
            innerSoup = BeautifulSoup(innerData, 'html.parser')
            print "{} -- {}".format(number, state)
            number += 1
            first_paragraph = innerSoup.find(
                'div', {'class', 'business-card'})

            try:
                business_name = first_paragraph.find('h1').text
            except:
                business_name = ''

            try:
                latlong = first_paragraph['data-pushpin']
                latitude = ast.literal_eval(latlong)['lat']
                longitude = ast.literal_eval(latlong)['lon']
            except:
                latitude = ''
                longitude = ''

            try:
                street_address = first_paragraph.find(
                    'p', {'class', 'street-address'}).text
            except:
                street_address = ''

            try:
                city_state = first_paragraph.find(
                    'p', {'class', 'city-state'}).text
            except:
                city_state = ''

            try:
                phone = first_paragraph.find('p', {'class', 'phone'}).text
            except:
                phone = ''

            try:
                websiteTag = first_paragraph.find(
                    'a', {'class': 'custom-link'}, href=True)
                website = websiteTag['href']
            except:
                website = ''

            try:
                emailTag = first_paragraph.find(
                    'a', {'class': 'email-business'}, href=True)
                email = emailTag['href'].split('mailto:')[1]
            except:
                email = ''

            try:
                years_in_business = ''
                for yrs_in_business in innerSoup.find_all('section', {'class': 'years-in-business'}):
                    years_in_business = yrs_in_business.find(
                        'p', {'class': 'count'}).text
            except:
                years_in_business = ''

            for second_paragraph in innerSoup.find_all('dl'):
                # print second_paragraph.parent.name
                if second_paragraph.parent.name == 'section':
                    datetime = []
                    try:
                        for time in second_paragraph.find_all('time'):
                            datetime.append(time['datetime'])
                    except:
                        datetime = []

                    try:
                        description = second_paragraph.find(
                            'dd', {'class': 'description'}).text
                    except:
                        description = ''

                    try:
                        service = second_paragraph.find(
                            'dt', text=re.compile('Services/Products:'))
                        servicesOrProducts = service.nextSibling.string
                    except:
                        servicesOrProducts = ''

                    try:
                        associations = second_paragraph.find(
                            'dd', {'class': 'associations'}).text
                    except:
                        associations = ''

                    try:
                        aka = second_paragraph.find(
                            'dd', {'class': 'aka'}).text
                    except:
                        aka = ''

                    try:
                        brands = second_paragraph.find(
                            'dd', {'class': 'brands'}).text
                    except:
                        brands = ''

                    try:
                        payment = second_paragraph.find(
                            'dd', {'class': 'payment'}).text
                    except:
                        payment = ''

                    try:
                        categoriesTag = second_paragraph.find(
                            'dd', {'class': 'categories'}).find_all('a')
                    except:
                        categoriesTag = []
                    categories = []
                    for category in categoriesTag:
                        categories.append(category.text)
            mainlst.append([business_name, state, latitude, longitude, street_address, city_state, phone, website, email,
                            years_in_business, datetime, description, servicesOrProducts, associations, aka, brands, payment, categories, actual_link])

        # with(open('yp_lst.py', 'w')) as f:
        #     f.write(str(mainlst))

    # set flag flase initially, when data is stored, change to True
    data_storage = False
    # start storing the data for every single pages
    db = MySQLdb.connect("localhost", "root", "root", "scrape", charset='utf8')
    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    for item in mainlst:

        try:
            cursor.execute("""INSERT into yp_com (business_name, state, latitude, longitude, street_address, city_state, phone, website, email, years_in_business, datetime, description, servicesOrProducts, associations, aka, brands, payment, categories, url) values (%s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
                str(item[0]), str(item[1]), item[2], item[3], str(item[4]), str(item[5]), str(item[6]), str(item[7]), str(item[8]), str(item[9]),  str(item[10]), str(item[11]), str(item[12]), str(item[13]), str(item[14]), str(item[15]), str(item[16]), str(item[17]), str(item[18])))
            db.commit()
            data_storage = True
        except Exception as e:
            logging.debug(str({'error': e.message, 'url': item[18]}))
    # disconnec.encode('utf-8')t from server
    db.close()
    print "hey..!! data is stored in db..!!!"

    # call the function again if next page is exist.
    nextPage = soup.find('div', {'class': 'pagination'})
    try:
        nxtpage = nextPage.find('a', {'class': 'next'}).get('data-page')
        fetch_data_using_scraping(
            state, nxtpage, '12.218.150.209:8080', number)
    except Exception as e:
        print "Alert ........... process repeatesgi......."

        fetch_data_using_scraping(state, page, '12.218.150.209:8080', number)
        # log for each fail record
        # logging.debug(
        #     {'error': e.message, 'state': state, 'page': page})

print "Stop.... scraping is going on"
for state in states:
    fetch_data_using_scraping(state, 93, '12.218.150.209:8080', 1)
    print "==#== {} has been completed. ==*==".format(state)
    break
