import json
import time
import os
from googleapiclient import discovery
from uuid import uuid4
import string
import random
from gcloud import storage, exceptions
import boto3
from oauth2client.client import GoogleCredentials

# custom modules
from config import *
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

# create a file handler
try:
    handler = logging.FileHandler(LOG_DIR+'/'+LOG_FILENAME)
except IOError:
    os.makedirs(LOG_DIR)
    handler = logging.FileHandler(LOG_DIR+'/'+LOG_FILENAME)
handler.setLevel(logging.ERROR)

# create a logging format
formatter = logging.Formatter('%(asctime)s %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)


class AwsToBigQuery(object):

    """
    Process to move snowplow events
    from AWS to BIQ QUERY
    """

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.client = storage.Client()
        self.gcs_bucket_obj = self.client.get_bucket(GCS_BUCKET_NAME)
        self.s3_bucket_obj = self.s3.Bucket(S3_BUCKET)

    def load_table(self, bigquery, project_id,
                   dataset_id, table_name, source_schema,
                   source_path, num_retries=5):
        """
        Starts a job to load a bigquery table from CSV
        Args:
            bigquery: an initialized and authorized bigquery client
            google-api-client object
            source_schema: a valid bigquery schema,
            see https://cloud.google.com/bigquery/docs/reference/v2/tables
            source_path: the fully qualified Google Cloud Storage location of
            the data to load into your table
        Returns: a bigquery load job, see
        https://cloud.google.com/bigquery/docs/reference/v2/jobs#configuration.load
        """

        # Generate a unique job ID so retries
        # don't accidentally duplicate query
        job_data = {
            'jobReference': {
                'projectId': project_id,
                'jobId': str(uuid4())
            },
            'configuration': {
                'load': {
                    'sourceUris': [source_path],
                    'schema': {
                        'fields': source_schema
                    },
                    'destinationTable': {
                        'projectId': project_id,
                        'datasetId': dataset_id,
                        'tableId': table_name
                    },
                    'sourceFormat': 'CSV',
                    'fieldDelimiter': '\t',
                    'allowJaggedRows': True,
                    'ignoreUnknownValues': True
                }
            }
        }

        return bigquery.jobs().insert(
            projectId=project_id,
            body=job_data).execute(num_retries=num_retries)
    # [END load_table]

    # [START poll_job]
    def poll_job(self, bigquery, job):
        """Waits for a job to complete."""

        print('Waiting for job to finish...')

        request = bigquery.jobs().get(
            projectId=job['jobReference']['projectId'],
            jobId=job['jobReference']['jobId'])

        while True:
            result = request.execute(num_retries=2)

            if result['status']['state'] == 'DONE':
                if 'errorResult' in result['status']:
                    raise RuntimeError(result['status']['errorResult'])
                print('Job complete.')
                return

            time.sleep(1)
    # [END poll_job]

    # [START run]
    def main(self, project_id, dataset_id, table_name,
             schema_file, data_path,
             poll_interval, num_retries, filename):
        # [START build_service]
        # Grab the application's default credentials from the environment.
        credentials = GoogleCredentials.get_application_default()

        # Construct the service object for interacting with the BigQuery API.
        bigquery = discovery.build('bigquery', 'v2',
                                   credentials=credentials)
        # [END build_service]

        with open(schema_file, 'r') as f:
            schema = json.load(f)

        job = self.load_table(bigquery, project_id,
                              dataset_id, table_name,
                              schema, data_path, num_retries)

        self.poll_job(bigquery, job)

        self.delete_file_from_gcs(filename)
        # also create a filename which is available in s3
        s3_filename = filename.replace('run-', 'run=')[:-16]
        # Remove unnecessary files from bucket and tmp folder
        self.move_files_to_archievs(filename, s3_filename)
        self.delete_file_from_s3_good(s3_filename)

    def getUploadedCSV(self):
        """
                function is used to fetching files from
                Google Cloud and sending to Main function
                to store data in BIGQUERY
        """
        for blob in self.gcs_bucket_obj.list_blobs():
            blobname = blob.path.split('/')[-1]
            blobAddress = "gs://{0}/{1}".format(GCS_BUCKET_NAME, blobname)
            try:
                self.main('usb-snowplow-prototype',
                          'atomic', 'events',
                          'table.json',  blobAddress, 1, 2, blobname)
            except Exception as e:
                self.generate_log('getUploadedCSV', str(e), blobname)

                # main(args.project_id, args.dataset_id, args.table_name,
                #     args.schema_file, args.data_path, args.poll_interval,
                #     args.num_retries)

    def uploadCSVtoGCS(self):
        """
        function upload csv to google cloud from local machine
        code is finding files from tmp_dir folder and moving to gcs
        """
        listOfFiles = os.listdir(temp_dir)
        for fileObj in listOfFiles:
            filepath = os.path.join(temp_dir, fileObj)
            if fileObj.find('.') == -1:
                blob = self.gcs_bucket_obj.blob(fileObj)
                try:
                    blob.upload_from_filename(filename=filepath)
                    # os.remove(filepath) will remove in end of process
                except Exception as e:
                    self.generate_log('uploadCSVtoGCS', str(e), fileObj)
        self.getUploadedCSV()

    def id_generator(self, size=6,
                     chars=string.ascii_uppercase + string.digits):
        """
        To generate random string
        """
        return ''.join(random.choice(chars) for _ in range(size))

    def download_from_s3(self):
        """
        Download files from AWS s3 bucket in tmp folder
        """
        for obj in self.s3_bucket_obj.objects.filter(
                Prefix=S3_BUCKET_GOOD_DIR):
            objaddress = obj.key
            if objaddress.find('part') > 1:
                s3_file_parent_name = objaddress.split(
                    '/')[-2].replace('=', '-')
                print s3_file_parent_name
                filename = s3_file_parent_name + "-" + self.id_generator(15)
                try:
                    self.s3.meta.client.download_file(
                        S3_BUCKET, objaddress, temp_dir + '/' + filename)
                except IOError:
                    if not os.path.exists(temp_dir):
                        os.makedirs(temp_dir)
                    self.s3.meta.client.download_file(
                        S3_BUCKET, objaddress, temp_dir + '/' + filename)

        self.uploadCSVtoGCS()

    def move_files_to_archievs(self, filename, s3_filename):
        """
        Once data is moved to BIGQUERY,
        move file from /GOOD/ bucket to 
        /ARCHIVE/ in S3 AWS.
        So first copy the file and delete from origin
        """
        filepath = temp_dir+'/'+filename
        try:
            self.s3.meta.client.upload_file(
                filepath, S3_BUCKET,
                S3_BUCKET_ARCHIVE_DIR + s3_filename)
            os.remove(filepath)
        except Exception as e:
            self.generate_log('move_files_to_archievs', str(e), filename)

    def delete_file_from_s3_good(self, s3_filename):
        """
        Once all process done,
        Remove file log from s3 good bucket
        """
        f = S3_BUCKET_GOOD_DIR + '/' + s3_filename
        self.s3_bucket_obj.delete_objects(Delete={'Objects': [{'Key': f}]}) # can delete multiple files

    def delete_file_from_gcs(self, filename):
        """
        Delete file from Google Cloud
        """
        try:
            self.gcs_bucket_obj.delete_blob(filename)
        except exceptions.NotFound as e:
            self.generate_log('delete_file_from_gcs', str(e), filename)

    def generate_log(self, func_name, error, filename):
        """
        Generate log file with combination of function name and error
        """
        logger.error('!'+"="*10 + 'Getting issue while processing the file '+str(filename)+' ' + "="*10+'!')
        logger.error("Function name is :" func_name)
        logger.error("Error : " + error)

Obj = AwsToBigQuery()
Obj.download_from_s3()
# Obj.move_files_to_archievs('run-2015-12-03-07-22-28-WGKMUZ683CQ50OZ1')
