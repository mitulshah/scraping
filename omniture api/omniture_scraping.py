import cookielib
import os
import urllib
import urllib2
import re
import string
from bs4 import BeautifulSoup
import time
import requests
from credential import username, password
import json
import operator
import csv
from socket import timeout
from furl import furl
import combi
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

cookie_filename = "omniture.cookies.txt"
FILENAME = 'USB_newrepeat_Month_1stMarch2017.csv'
resulttype = 'keymetric'


class OmnitureParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        proxy = urllib2.ProxyHandler({'http': '178.62.52.60:8118'})
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            proxy
        )
        self.opener.addheaders = [
            ('User-agent',
             ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'))
        ]

        # Login
        x = self.loginPage()
        if x:
            self.loadData()

        self.cj.save()

    def generateSession(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            f_url = response.url
            f = furl(f_url)
            self.sessionID = f.args['ssSession']
            self.JPJ = f.args['jpj']
            return True
        except Exception as e:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            # return self.generateSession(url, data)
            raise


    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            # return ''.join(response.readlines())
            d = response.read()
            return d
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            return self.loadPage(url, data)


    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        # html = self.loadPage("https://sc2.omniture.com/login/")
        # soup = BeautifulSoup(html, 'html.parser')

        login_data = urllib.urlencode({
            'company': 'usbank',
            'username': self.login,
            'password': self.password,
            'enterprise_login': True,
            'selected_locale': 'en_US',
            'host': 'sc2.omniture.com',
            'save_login_info': 'on',
            'redirect': '',
            'loginref': '',
            'sso_attempt': 0
        })
        try:
            html = self.generateSession(
                "https://sc2.omniture.com/p/suite/1.3/?a=Main.Auth", login_data)
        except Exception as e:
            print(e)

        if html:
            return True

    def loadData(self):

        combination = combi.index(resulttype)
        urlnmbr = 1
        for url1 in combination:
            ob_segid = url1[1]
            url = ''.join(url1)
            # url = "https://sc2.omniture.com/sc15/reports/index.html?a=Report.Standard&r=Report.GetConversions&rp=ec_pri%7C12%3Bec_reset%7C1%3Bperiod_from%7C09%2F01%2F16%3Bperiod_to%7C09%2F30%2F16%3Bperiod%7C11608%3Brange_period%7C1%3Brow%7C0%3Bdetail_depth%7C200"
            # print(url)
            url += '&ssSession={0}&jpj={1}#/'.format(self.sessionID, self.JPJ)
            html = self.loadPage(url)
            soup = BeautifulSoup(html, 'html.parser')
            all_scripts = soup.find_all('script', {'charset': 'utf-8'})
            newAfterRepeat = True
            for scrpt in all_scripts:
                if 'report_model.setResponse({"configure":"<table data-class=' in scrpt.text:
                    the_text = scrpt.text
                    try:
                        tablestartpoint = the_text.split('"main_data":"')[1]
                        d = tablestartpoint.split('\\n",')[0].replace("\\", "")
                        soup1 = BeautifulSoup(d, 'html.parser')
                        if resulttype=='newrepeat':
                            try:
                                headertds = soup1.find('tr', {'class': 'data_table_header'}).find_all('td', {'class': 'data_table_header_font'})
                                for i, htds in enumerate(headertds):
                                    if i < 4:
                                        if 'new' in htds.text.lower():
                                            newindex = i
                                        if 'repeat' in htds.text.lower():
                                            repeatindex = i
                                if newindex < repeatindex:
                                    print('*'*10, url)
                                    newAfterRepeat = False

                            except Exception as e:
                                pass
                        trs = soup1.find_all('tr', {'class': 'hover_highlight'})
                        trlist = []
                        with open(FILENAME, 'a') as csvfile:
                            spamwriter = csv.writer(csvfile, delimiter=',',
                                                    quoting=csv.QUOTE_MINIMAL)
                            for eachtr in trs:
                                tds = eachtr.find_all('td', {'class': 'data_table_text'})
                                p = []
                                for eachtd in tds:
                                    p.append(eachtd.text)
                                if not newAfterRepeat:
                                    p1, p2 = p.index(p[1]), p.index(p[2])
                                    p3, p4 = p.index(p[3]), p.index(p[4])
                                    p[p1], p[p3] = p[p3], p[p1]
                                    p[p2], p[p4] = p[p4], p[p2]

                                p.append(ob_segid)
                                p.append(url)
                                spamwriter.writerow(p)
                        break
                    except Exception as e:
                        print(e)

            print("{} urls are completed.".format(urlnmbr))
            urlnmbr += 1

OmnitureParser(username, password)
