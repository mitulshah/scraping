from bs4 import BeautifulSoup
import requests
import urllib2

# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '70.248.28.13:8080'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

url = "https://www.android.com/pay/where-to-use/"


def get_all_colleges(base_url):
    request = urllib2.Request(base_url, headers=hdr)
    try:
        result = urllib2.urlopen(request)
    except Exception as e:
        print(e)
    data = result.read()
    soup = BeautifulSoup(data, 'html.parser')
    divimages = soup.find_all(
        'div', {'class': 'pay-logo-list__item--m-2-columns'})
    for imageDiv in divimages:
        img = imageDiv.find('img')
        print("https://www.android.com/" + img.get('src', ''))

get_all_colleges(url)
