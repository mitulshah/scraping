import os
import os.path as op
import re
import json
from datetime import datetime, timedelta
from collections import defaultdict

import flask_admin as admin
from flask import Flask, url_for, redirect, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from wtforms import form, fields, validators
from werkzeug.security import generate_password_hash, check_password_hash
import flask_login as login
from sqlalchemy import func
from wtforms import validators

# import flask_admin as admin
from flask.ext.admin import Admin, BaseView, expose
from flask_admin.contrib import sqla
from flask_admin.contrib.sqla import filters
from flask_admin import helpers
from flask.ext.cors import CORS

# Create application
app = Flask(__name__)
CORS(app)
app.debug = True
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)

# Define models

# to run models and create or drop db


class MarketingTest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250))
    mktcampaign = db.Column(db.String(250))
    mktmedium = db.Column(db.String(250))
    mktsource = db.Column(db.String(250))
    summary = db.Column(db.String(500))

    # Required for administrative interface. For python 3 please use __str__
    # instead.
    def __unicode__(self):
        return self.title

# Define models


class AdserverDataModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mktsource = db.Column(db.String(100))
    mktmedium = db.Column(db.String(100))
    mktcampaign = db.Column(db.String(100))
    pageurlpath = db.Column(db.String(500))
    adtype = db.Column(db.String(100))
    impressions = db.Column(db.BigInteger)
    ctr = db.Column(db.Float(precision=2))
    clicks = db.Column(db.Integer)
    cpc = db.Column(db.Float(precision=2))
    cpm = db.Column(db.Float(precision=2))
    conversion_rate = db.Column(db.Float)
    actions = db.Column(db.Integer)
    cpa = db.Column(db.Float(precision=2))
    spent = db.Column(db.Float(precision=2))
    date = db.Column(db.Date)
    # Required for administrative interface. For python 3 please use __str__
    # instead.

    def __unicode__(self):
        return self.impressions, self.clicks, self.actions


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    note = db.Column(db.String(500))

    mt_id = db.Column(db.Integer(), db.ForeignKey(MarketingTest.id))
    mt = db.relationship(MarketingTest, backref='Note')

    def __unicode__(self):
        return '%s' % (self.note)


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    question = db.Column(db.String(500))

    mt_id = db.Column(db.Integer(), db.ForeignKey(MarketingTest.id))
    mt = db.relationship(MarketingTest, backref='Question')

    def __unicode__(self):
        return '%s' % (self.question)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    login = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120))
    password = db.Column(db.String(64))

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


# Define login and registration forms (for flask-login)


class LoginForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()
        if user is None:
            raise validators.ValidationError('Invalid user')

        # we're comparing the plaintext pw with the the hash from the db
        if user.password != self.password.data:
            # to compare plain text passwords use
            # if user.password != self.password.data:
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(User).filter_by(login=self.login.data).first()


class RegistrationForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    email = fields.TextField()
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        if db.session.query(User).filter_by(login=self.login.data).count() > 0:
            raise validators.ValidationError('Duplicate username')


# Initialize flask-login
def init_login():
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)


# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super(MyAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for('.index'))
        link = '<p>Don\'t have an account? <a href="' + \
            url_for('.register_view') + '">Click here to register.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()

    @expose('/registeradmin/', methods=('GET', 'POST'))
    def register_view(self):
        form = RegistrationForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = User()

            form.populate_obj(user)
            # we hash the users password to avoid saving it as plaintext in the db,
            # remove to use plain text:
            user.password = form.password.data

            db.session.add(user)
            db.session.commit()

            login.login_user(user)
            return redirect(url_for('.index'))
        link = '<p>Already have an account? <a href="' + \
            url_for('.login_view') + '">Click here to log in.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))


# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated


class Marketing(sqla.ModelView):
    inline_models = (Note, Question)

    def is_accessible(self):
        return login.current_user.is_authenticated


class AdserverDataView(sqla.ModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated


class Dashboard(BaseView):

    @expose('/')
    def index(self):
        # List = MarketingTest.query.join(Note, Question).add_columns(
        #     Note.note, Question.question).group_by(MarketingTest.id)

        Query = db.session.query(MarketingTest,
                                 func.group_concat(Note.note.distinct(),
                                                   ' -#-'),
                                 func.group_concat(
                                     Question.question.distinct(), ' -#-')
                                 ).outerjoin(
            Note, Question).group_by(MarketingTest.id).all()

        response = []
        for item in Query:
            dictObj = {}
            dictObj['title'] = item[0].title
            dictObj['objective'] = {}
            dictObj['objective']['summary'] = item[0].summary

            questList = []
            try:
                questions = re.split(r' -#-+,*', item[-1])[:-1]
                # queset = set(questions)
                for q in questions:
                    questList.append({'question': q})
            except Exception:
                pass
            dictObj['objective']['keyquestions'] = questList

            NoteList = []
            try:
                notes = re.split(r' -#-+,*', item[-2])[:-1]
                # noteset = set(notes)
                for n in notes:
                    NoteList.append({'item': n})
            except Exception:
                pass
            dictObj['notes'] = NoteList
            dictObj['MKTCAMPAIGN'] = item[0].mktcampaign
            dictObj['MKTMEDIUM'] = item[0].mktmedium
            dictObj['MKTSOURCE'] = item[0].mktsource
            dictObj['CUSTOMKEY'] = item[0].mktsource + item[0].mktmedium + item[0].mktcampaign
            response.append(dictObj)
        resp = {'result': response}
        return json.dumps(resp)
        # return self.render(
        #     'admin/jsonresponse.html',
        #     arg1=resp
        # )


class Adserverdata(BaseView):

    def query_func(self, timefilter='all'):
        query = """db.session.query(AdserverDataModel).with_entities(
            AdserverDataModel.mktsource,
            AdserverDataModel.mktmedium,
            AdserverDataModel.mktcampaign,
            AdserverDataModel.pageurlpath,
            AdserverDataModel.adtype,
            func.sum(AdserverDataModel.impressions).label("impressions"),
            func.sum(AdserverDataModel.ctr).label("ctr"),
            func.sum(AdserverDataModel.clicks).label("clicks"),
            func.sum(AdserverDataModel.cpc).label("cpc"),
            func.sum(AdserverDataModel.cpm).label("cpm"),
            func.sum(AdserverDataModel.conversion_rate).label(
                "conversion_rate"),
            func.sum(AdserverDataModel.actions).label("actions"),
            func.sum(AdserverDataModel.cpa).label("cpa"),
            func.sum(AdserverDataModel.spent).label("spent")
        )"""
        if timefilter != 'all':
            query += """.filter(AdserverDataModel.date=='{}')""".format(
                timefilter)
        query += """.group_by(
            AdserverDataModel.mktsource,
            AdserverDataModel.mktmedium,
            AdserverDataModel.mktcampaign,
            AdserverDataModel.adtype,
            AdserverDataModel.pageurlpath).all()"""
        return eval(query)

    @expose('/')
    def index(self):
        query_all = self.query_func()
        data_all = defaultdict(list)
        for item in query_all:
            row = {}
            row['mktsource'] = source = item[0]
            row['mktmedium'] = medium = item[1]
            row['mktcampaign'] = campaign = item[2]
            row['pageurlpath'] = item[3]
            row['adtype'] = item[4]
            row['impressions'] = int(item[5])
            row['ctr'] = round(item[6], 2)
            row['clicks'] = int(item[7])
            row['cpc'] = round(item[8], 2)
            row['cpm'] = round(item[9], 2)
            row['conversion_rate'] = round(item[10], 2)
            row['actions'] = int(item[11])
            row['cpa'] = round(item[12], 2)
            row['spent'] = round(item[13], 2)
            customkey = source + medium + campaign
            data_all[customkey].append(row)

        response = {'result': data_all}
        return json.dumps(response)
        # return json.dumps(response, cls=DecimalEncoder)

# Create admin

# db.drop_all()
# db.create_all()
# admin = Admin(
#     app, name='Test', template_mode='bootstrap3')
admin = admin.Admin(
    app, 'Test', index_view=MyAdminIndexView(), base_template='my_master.html',
    template_mode='bootstrap3')
admin.add_view(Marketing(MarketingTest, db.session))
admin.add_view(AdserverDataView(AdserverDataModel, db.session))
admin.add_view(Dashboard(name='Dashboard'))
admin.add_view(Adserverdata(name='Adserver Data'))
# admin.add_view(MyModelView(User, db.session))
# Flask views


# Initialize flask-login
init_login()

if __name__ == '__main__':
    # Start app
    app.run(host='0.0.0.0')
