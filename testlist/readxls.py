import MySQLdb
import csv

# Open database connection
db = MySQLdb.connect("localhost", "root", "root", "flaskapi_testlist")
# db = MySQLdb.connect("127.0.0.1", "root", "password", "testlist")


# prepare a cursor object using cursor() method
cursor = db.cursor()

fulllist = []
with open('taboola.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for n, row in enumerate(spamreader):
        tpl = (row[6], row[7], row[8], row[4], row[3],
               row[9], row[10], row[11], row[12], row[13],
               row[14], row[15], row[16], row[17], row[0])
        # print n, len(tpl), row[0],  row[6], row[17]
        fulllist.append(tpl)

try:
    cursor.executemany("INSERT into adserver_data_model (mktsource, \
			mktmedium, mktcampaign, pageurlpath, adtype, \
			impressions, ctr, clicks, cpc, cpm, conversion_rate, \
			actions, cpa, spent, date) values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", fulllist)
except Exception as e:
    print e
db.commit()

# disconnec.encode('utf_8')t from server
db.close()
