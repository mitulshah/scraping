#!/usr/bin/python
# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
import datetime
import time
from pytz import timezone
from math import exp
from decimal import Decimal

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=1)


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        # conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


class Main(object):

    def calcScoreQuery(self, previous_day, today):

        query = \
            """SELECT a.*,
        CASE
        WHEN first_collector_hr < 4 THEN 1
        ELSE 0
        END AS flag_hr_0thru3,
        CASE
        WHEN first_collector_hr BETWEEN 4 AND 7 THEN 1
        ELSE 0
        END AS flag_hr_4thru7,
        CASE
        WHEN first_collector_hr BETWEEN 8 AND 11 THEN 1
        ELSE 0
        END AS flag_hr_8thru11,
        CASE
        WHEN first_collector_hr BETWEEN 12 AND 15 THEN 1
        ELSE 0
        END AS flag_hr_12thru15,
        CASE
        WHEN first_collector_hr BETWEEN 16 AND 19 THEN 1
        ELSE 0
        END AS flag_hr_16thru19,
        CASE
        WHEN first_collector_hr BETWEEN 20 AND 23 THEN 1
        ELSE 0
        END AS flag_hr_20thru23,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 0 THEN 1
        ELSE 0
        END AS flag_dow_sun,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 1 THEN 1
        ELSE 0
        END AS flag_dow_mon,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 2 THEN 1
        ELSE 0
        END AS flag_dow_tue,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 3 THEN 1
        ELSE 0
        END AS flag_dow_wed,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 4 THEN 1
        ELSE 0
        END AS flag_dow_thu,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 5 THEN 1
        ELSE 0
        END AS flag_dow_fri,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 6 THEN 1
        ELSE 0
        END AS flag_dow_sat,
        CASE
        WHEN mkt_campaign like '%mobile%' OR CAST(first_collector_date AS DATE) = '04/11/2016' THEN 1
        ELSE 0
        END AS flag_mobile,
        CASE
        WHEN (mkt_campaign not like '%mobile%' AND CAST(first_collector_date AS DATE) <> '04/11/2016') OR mkt_campaign = 'persistent' THEN 1
        ELSE 0
        END AS flag_desktop,
        CASE
        WHEN page_urlpath like '/article%' THEN 1
        ELSE 0
        END AS flag_arti,
        CASE
        WHEN page_urlpath like '/balance-transfer%' THEN 1
        ELSE 0
        END AS flag_comp,
        CASE
        WHEN page_urlpath like '/content%' THEN 1
        ELSE 0
        END AS flag_prod,
        CASE
        WHEN page_urlpath IN ('/article/4-balance-transfer-cards-with-0-interest-until-2017','/article/mobile-4-balance-transfer-cards-with-0-interest-until-2017') THEN 1
        ELSE 0
        END AS flag_arti_4_balance_transfer,
        CASE
        WHEN page_urlpath IN ('/article/balance-transfer-credit-cards') THEN 1
        ELSE 0
        END AS flag_arti_balance_transfer_cc,
        CASE
        WHEN page_urlpath IN ('/article/pay-off-credit-card-debt') THEN 1
        ELSE 0
        END AS flag_arti_pay_off,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/10-best-balance-transfer-credit-cards-2016','/balance-transfer/mobile-10-best-balance-transfer-credit-cards-2016') THEN 1
        ELSE 0
        END AS flag_comp_10_best_2016,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/mobile-best-balance-transfer-credit-card-nofee','/balance-transfer/mobile-best-balance-transfer-credit-card-nofee') THEN 1
        ELSE 0
        END AS flag_comp_10_best_nofee,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/best-balance-transfer-credit-cards-highest-rated','/balance-transfer/mobile-best-balance-transfer-credit-cards-highest-rated') THEN 1
        ELSE 0
        END AS flag_comp_10_best_highest,
        CASE
        WHEN page_urlpath IN ('/content/chase-slate-product-review') THEN 1
        ELSE 0
        END AS flag_prod_chase_slate,
        CASE
        WHEN page_urlpath IN ('/content/citi-simplicity-product-review','/content/mobile-citi-simplicity-product-review') THEN 1
        ELSE 0
        END AS flag_prod_citi_simplicity,
        CASE
        WHEN page_urlpath IN ('/content/discover-it-product-review') THEN 1
        ELSE 0
        END AS flag_prod_discover_it,
        CASE
        WHEN refr_urlpath LIKE '%msn%' THEN 1
        ELSE 0
        END AS msn,
        CASE
        WHEN refr_urlpath LIKE '%mobileposse%' THEN 1
        ELSE 0
        END AS mobileposse,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/crunchmind%' THEN 1
        ELSE 0
        END AS crunchmind,
        CASE
        WHEN refr_urlpath LIKE '%marketingandresearchsolutionslp%' THEN 1
        ELSE 0
        END AS marketingandresearchsolutionslp,
        CASE
        WHEN refr_urlpath LIKE '%wazimo%' THEN 1
        ELSE 0
        END AS wazimo,
        CASE
        WHEN refr_urlpath LIKE '%qpolitical%' THEN 1
        ELSE 0
        END AS qpolitical,
        CASE
        WHEN refr_urlpath LIKE '%socialtheater%' THEN 1
        ELSE 0
        END AS socialtheater,
        CASE
        WHEN refr_urlpath LIKE '%nydailynews%' THEN 1
        ELSE 0
        END AS nydailynews,
        CASE
        WHEN refr_urlpath LIKE '%nbcnews%' THEN 1
        ELSE 0
        END AS nbcnews,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/seccosquared%' THEN 1
        ELSE 0
        END AS seccosquared,
        CASE
        WHEN refr_urlpath LIKE '%viralnova%' THEN 1
        ELSE 0
        END AS viralnova,
        CASE
        WHEN refr_urlpath LIKE '%localtv%' THEN 1
        ELSE 0
        END AS localtv,
        CASE
        WHEN refr_urlpath LIKE '%247sports%' THEN 1
        ELSE 0
        END AS sports247,
        CASE
        WHEN refr_urlpath LIKE '%cox%' THEN 1
        ELSE 0
        END AS cox,
        CASE
        WHEN refr_urlpath LIKE '%cbsinteractive%' THEN 1
        ELSE 0
        END AS cbsinteractive,
        CASE
        WHEN refr_urlpath LIKE '%verticalscope%' THEN 1
        ELSE 0
        END AS verticalscope,
        CASE
        WHEN refr_urlpath LIKE '%socialsweetheartsgmbh%' THEN 1
        ELSE 0
        END AS socialsweetheartsgmbh,
        CASE
        WHEN refr_urlpath LIKE '%alltopics%' THEN 1
        ELSE 0
        END AS alltopics,
        CASE
        WHEN refr_urlpath LIKE '%worldlifestyle%' THEN 1
        ELSE 0
        END AS worldlifestyle,
        CASE
        WHEN refr_urlpath LIKE '%aol%' THEN 1
        ELSE 0
        END AS aol,
        CASE
        WHEN refr_urlpath LIKE '%americanmedia%' THEN 1
        ELSE 0
        END AS americanmedia,
        CASE
        WHEN refr_urlpath LIKE '%disqus%' THEN 1
        ELSE 0
        END AS disqus,
        CASE
        WHEN refr_urlpath LIKE '%tribunebroadcasting%' THEN 1
        ELSE 0
        END AS tribunebroadcasting,
        CASE
        WHEN refr_urlpath LIKE '%advance%' THEN 1
        ELSE 0
        END AS advance,
        CASE
        WHEN refr_urlpath LIKE '%cpxi%' THEN 1
        ELSE 0
        END AS cpxi,
        CASE
        WHEN refr_urlpath LIKE '%labx%' THEN 1
        ELSE 0
        END AS labx,
        CASE
        WHEN refr_urlpath LIKE '%wikia%' THEN 1
        ELSE 0
        END AS wikia,
        CASE
        WHEN refr_urlpath LIKE '%ziffdavis%' THEN 1
        ELSE 0
        END AS ziffdavis,
        CASE
        WHEN refr_urlpath LIKE '%yesimright%' THEN 1
        ELSE 0
        END AS yesimright,
        CASE
        WHEN refr_urlpath LIKE '%ijreview%' THEN 1
        ELSE 0
        END AS ijreview,
        CASE
        WHEN refr_urlpath LIKE '%salonmedia%' THEN 1
        ELSE 0
        END AS salonmedia,
        CASE
        WHEN refr_urlpath LIKE '%inquisitr%' THEN 1
        ELSE 0
        END AS inquisitr,
        CASE
        WHEN refr_urlpath LIKE '%kaizenmediagroup%' THEN 1
        ELSE 0
        END AS kaizenmediagroup,
        CASE
        WHEN refr_urlpath LIKE '%tegna%' THEN 1
        ELSE 0
        END AS tegna,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/merriamwebster%' THEN 1
        ELSE 0
        END AS merriamwebster,
        CASE
        WHEN refr_urlpath LIKE '%nbcots%' THEN 1
        ELSE 0
        END AS nbcots,
        CASE
        WHEN refr_urlpath LIKE '%gannettcompany%' THEN 1
        ELSE 0
        END AS gannettcompany,
        CASE
        WHEN refr_urlpath LIKE '%timewarnercable%' THEN 1
        ELSE 0
        END AS timewarnercable,
        CASE
        WHEN refr_urlpath LIKE '%usatoday%' THEN 1
        ELSE 0
        END AS usatoday,
        CASE
        WHEN refr_urlpath LIKE '%breitbartcom%' THEN 1
        ELSE 0
        END AS breitbartcom,
        CASE
        WHEN refr_urlpath LIKE '%dailymail%' THEN 1
        ELSE 0
        END AS dailymail,
        CASE
        WHEN refr_urlpath LIKE '%nbcsports%' THEN 1
        ELSE 0
        END AS nbcsports,
        CASE
        WHEN refr_urlpath LIKE '%perezhilton%' THEN 1
        ELSE 0
        END AS perezhilton,
        CASE
        WHEN refr_urlpath LIKE '%vervemobile%' THEN 1
        ELSE 0
        END AS vervemobile,
        CASE
        WHEN refr_urlpath LIKE '%foodnetwork%' THEN 1
        ELSE 0
        END AS foodnetwork,
        CASE
        WHEN refr_urlpath LIKE '%readersdigest%' THEN 1
        ELSE 0
        END AS readersdigest,
        CASE
        WHEN refr_urlpath LIKE '%spartzmedia%' THEN 1
        ELSE 0
        END AS spartzmedia,
        CASE
        WHEN refr_urlpath LIKE '%tickld%' THEN 1
        ELSE 0
        END AS tickld,
        CASE
        WHEN refr_urlpath LIKE '%digi%' THEN 1
        ELSE 0
        END AS digi,
        CASE
        WHEN refr_urlpath LIKE '%myfox%' THEN 1
        ELSE 0
        END AS myfox,
        CASE
        WHEN b.domain_sessionid IS NULL THEN 0
        ELSE 1
        END AS conversion_flag
        FROM (SELECT a.domain_sessionid,
        page_urlpath,
        mkt_campaign,
        TRUNC(first_collector_tstamp) first_collector_date,
        DATE_PART(h,first_collector_tstamp) first_collector_hr,
        MIN(first_collector_tstamp) event_time,
        refr_urlpath
        FROM atomic.events a
        INNER JOIN (SELECT domain_sessionid,
        MIN(collector_tstamp) AS first_collector_tstamp,
        MIN(etl_tstamp) AS first_etl_tstamp
        FROM atomic.events
        WHERE app_id = 'magnify'
        AND   mkt_source = 'taboola'
        AND   mkt_medium = 'referral'
        AND   collector_tstamp BETWEEN '{0}' AND dateadd(hr,+4,'{1}')
        GROUP BY domain_sessionid) b
        ON a.domain_sessionid = b.domain_sessionid
        AND a.collector_tstamp = b.first_collector_tstamp
        AND etl_tstamp = b.first_etl_tstamp
        GROUP BY a.domain_sessionid,
        page_urlpath,
        mkt_campaign,
        TRUNC(first_collector_tstamp),
        DATE_PART(h,first_collector_tstamp),
        first_collector_tstamp,
        refr_urlpath) a
        LEFT OUTER JOIN (SELECT DISTINCT domain_sessionid
        FROM atomic.events
        WHERE app_id = 'magnify'
        AND   page_url LIKE '%SEMBT%'
        AND   collector_tstamp BETWEEN '{0}' AND dateadd(hr,+4,'{1}')
        GROUP BY domain_sessionid) b
        ON a.domain_sessionid = b.domain_sessionid 
        ORDER BY a.event_time 
        -- OFFSET 29501
        """.format(previous_day, today)

        try:
            # print query
            cur.execute(query)
            return cur.fetchall()
        except Exception, e:
            print e.message

    @redshift_dbconn
    def executeQuery(self):
        print "Query start at {}".format(time.ctime())
        results = self.calcScoreQuery(previous_day, today)
        # print results
        print "Process start at {}".format(time.ctime())
        for result in results:
            r = (Decimal(result['flag_arti'] * -1.247) +
                 Decimal(result['flag_dow_sun'] * 0.9916) +
                 Decimal(result['flag_dow_wed'] * 0.8277) +
                 Decimal(result['flag_hr_0thru3'] * -0.7175) +
                 Decimal(result['marketingandresearchsolutionslp'] * -1.391) +
                 Decimal(result['localtv'] * 0.9315) +
                 Decimal(result['verticalscope'] * 1.203) +
                 Decimal(result['aol'] * 0.913) +
                 Decimal(result['crunchmind'] * -1.715) +
                 Decimal(result['msn'] * 0.4922) +
                 Decimal(result['wazimo'] * -2.02) +
                 Decimal(result['sports247'] * 1.618) +
                 Decimal(result['socialsweetheartsgmbh'] * 2.054) +
                 Decimal(result['americanmedia'] * 1.283) +
                 Decimal(result['mobileposse'] * 1.476))
            print "r = {}".format(r)
            e = exp(r)
            print "e = {}".format(e)
            score_t = e/float((1+e))
            result.extend(["AMD", score_t])
        print "Insert Process is Started at {}".format(time.ctime())
        print "Total Number of rows {}".format(len(results))
        # cur.executemany(
        #     """INSERT INTO atomic.magnify_model values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", results)
        # print "Last process is also now completed at {}.. Now relax and chill".format(time.ctime())

# Obj = Main()
# Obj.executeQuery()


urllist = ['https://www.offgridworld.com/', 'http://www.greatergood.com/', 'https://www.ezoic.com/', 'http://rare.us/', 'http://www.allfreecrochet.com/', 'https://ssl1.gmti.com/', 'http://www.greenbot.com/', 'http://www.sliptalk.com/', 'http://idealist4ever.com/', 'http://www.angmalaya.net/', 'https://www.google.co.in/', 'http://www.stophillarypac.org/', 'http://mastphotos.com.cutestat.com/', 'http://theriflebird.com/', 'http://www.ksdk.com/', 'http://walterfootball.com/', 'http://www.guidestar.org/', 'http://www.necn.com/', 'http://www.inquisitr.com/', 'http://www.gmfullsize.com/', 'http://pngfever.com/', 'http://www.wnep.com/', 'http://www.wlky.com/', 'http://www.99traveltips.com/', 'http://blockcommunications.com/', 'http://q13fox.com/', 'http://en.wikipedia.org/', 'http://www.livinglymedia.com/', 'http://bust.com/', 'http://en.paperblog.com/', 'http://www.wizzed.com/', 'http://www.advocate.com/', 'http://www.siliconindia.com/', 'https://bryantravissmith.com/', 'http://soymujerinfo.com/', 'http://www.richmond.com/', 'http://www.maplestage.com/', 'http://dramayou.com/', 'http://faves.rare.us/', 'http://www.saturdaydownsouth.com/', 'http://tinybuddha.com/', 'http://www.ram1500diesel.com/', 'http://abc7chicago.com/', 'http://www.gocomics.com/', 'http://www.wcvb.com/', 'http://www.insidenova.com/', 'http://www.articles.chicagotribune.com/', 'http://log3condo.com/', 'http://www.lasvegassun.com/', 'http://utahgunexchange.com/', 'http://www.macworld.com/', 'http://www.celebuzz.com/', 'http://foreignpolicy.com/', 'http://whats-theword.com/', 'http://virallyinc.com.ipaddress.com/', 'http://www.shreveporttimes.com/', 'http://www.usports.sg/', 'http://www.espncricinfo.com/', 'http://www.healthylifetricks.com/', 'http://www.guitarplayer.com/', 'http://www.hollywoodlife.com/', 'http://cyber-breeze.com/', 'http://www.ipsglider.net/', 'http://www.wfsb.com/', 'http://www.diychatroom.com/', 'https://www.theinternetworks.co.uk/', 'http://www.hockey-reference.com/', 'http://www.theadvocate.com/', 'http://www.walesonline.co.uk/', 'http://www.healthylivingstyle.net/', 'http://thatgrapejuice.net/', 'http://www.veteranstoday.com/', 'http://www.imobileandroid.com/', 'http://phl17.com/', 'http://www.excite.com/', 'http://www.thevisitor.co.uk/', 'http://wzakcleveland.hellobeautiful.com/', 'http://www.armytimes.com/', 'http://www.boston.com/', 'http://www.gannett.com/', 'http://www.bittorrent.com/', 'http://thefirearmslounge.freeforums.net/', 'http://www.kptv.com/', 'http://www.wellbuzz.com/', 'https://www.taboola.com/', 'http://www.daytondailynews.com/', 'http://www.pontiflex.com/', 'http://www.zestvip.com/', 'http://www.flexonline.com/', 'http://wncn.com/', 'https://github.com/', 'http://usuncut.com/', 'http://www.allthetests.com/', 'https://www.bikerornot.com/', 'http://www.historicmysteries.com/', 'http://www.news965.com/', 'http://n4bb.com/', 'http://www.tamedia.ch/', 'http://dailywowzer.com/', 'http://entertainment.time.com/', 'http://www.jacksonsun.com/', 'http://nameguess.com/', 'http://www.dose.com/', 'http://www.sheldonsfans.com/', 'http://weknowmemes.com/', 'http://www.ignisnatura.cl/', 'http://www.forumfoundry.com/', 'http://lifehacklane.com/', 'http://profootballtalk.nbcsports.com/', 'http://oldschool1003.hellobeautiful.com/', 'http://www.ndtv.com/', 'http://www.modernman.com/', 'http://www.gaycities.com/', 'http://www.press-citizen.com/', 'http://conservativeamerica-online.com/', 'http://www.suffolknewsherald.com/', 'http://www.newsobserver.com/', 'http://polar.ncep.noaa.gov/', 'http://www.star-telegram.com/', 'http://theviraldance.com/', 'http://themeowpost.com/', 'http://www.quickmeme.com/', 'http://runt-of-the-web.com/', 'http://www.theolympian.com/', 'http://zap2it.com/', 'http://www.saymedia.com/', 'http://www.thenewcivilrightsmovement.com/', 'http://money.usnews.com/', 'http://www.songshd.pk/', 'http://www.savagesports.co.uk/', 'http://www.leaderlive.co.uk/', 'http://economictimes.indiatimes.com/', 'https://www.techsupportall.com/', 'http://www.boxingnews24.com/', 'http://www.wholehogsports.com/', 'http://www.deccanherald.com/', 'http://www.koat.com/', 'https://www.newzealandnow.govt.nz/', 'http://www.deccanchronicle.com/', 'http://celebchatter.com/', 'http://www.crackerdaily.com/', 'http://www.sanfoundry.com/', 'http://www.wownowiknow.com/', 'http://www.arch2o.com/', 'http://fantastico.guru.ipaddress.com/', 'http://www.intellectualtakeout.org/', 'http://perldoc.perl.org/', 'http://www.indianmotorcycles.net/', 'http://www.idahostatesman.com/', 'http://www.iamatexan.com/', 'http://www.crainsdetroit.com/', 'http://wnct.com/', 'http://m.tmz.com/', 'http://hosted.ap.org/', 'https://www.washingtonpost.com/', 'http://www.inkfreenews.com/', 'http://www.blackjack.pch.com/', 'http://www.inquirer.net/', 'http://www.sikkimexotica.com/', 'http://www.pappaspost.com/', 'http://www.in.techradar.com/', 'http://www.vicksburgpost.com/', 'http://www.meredith.com/', 'http://www.faithfamilyamerica.com/', 'http://www.blastingnews.com/', 'http://www.bitlanders.com/', 'http://www.freeappgg.com/', 'http://www.chroniclelive.co.uk/', 'http://www.femalefirst.co.uk/', 'http://rachfeed.com/', 'http://www.militarytimes.com/', 'http://www.mysportweb.com/', 'http://www.secrant.com/', 'http://www.golfwrx.com/', 'http://sfist.com/', 'http://wkrg.com/', 'http://www.computerworld.com/', 'http://www.mashpedia.com/', 'http://www.fuzzfix.com/', 'http://www.whnt.com/', 'http://www.annistonstar.com/', 'http://www.game-debate.com/', 'http://myhoneysplace.com/', 'http://www.salon.com/', 'http://linkis.com/', 'http://www.khaosodenglish.com/', 'http://www.whatifmediagroup.com/', 'http://www.desmoinesregister.com/', 'http://www.dailywire.com/', 'http://naturalon.com/', 'http://www.blogfa.com/', 'http://www.alertid.com/', 'http://www.mid-day.com/', 'http://www.lifestylestores.com/', 'http://www.wusa9.com/', 'http://www.yourdailydish.com/', 'http://www.sfexaminer.com/', 'http://www.wideopenspaces.com/', 'http://www.thelocal.de/', 'http://www.jagranjosh.com/', 'http://www.fortworth.dealsaver.com/', 'http://wivb.com/', 'http://newmediarockstars.com/', 'http://fox2now.com/', 'http://www.acheiusa.com/', 'http://texasguntrader.com/', 'http://www.infolinks.com/', 'http://crockmoms.com/', 'http://www.bnd.com/', 'http://www.uspresidentialelectionnews.com/', 'https://www.excelcampus.com/', 'http://www.cookingchanneltv.com/', 'http://www.digi.com.my/', 'http://acidcow.com/', 'http://www.damnyouautocorrect.com/', 'http://www.foxnews.com/', 'http://www.express.pk/', 'http://mysteriousuniverse.org/', 'http://www.bollymoviereviewz.com/', 'http://elitesportsny.com/', 'http://draftwizard.fantasypros.com/', 'http://www.albertleatribune.com/', 'http://www.yahoo.com/', 'http://www.cnsnews.com/', 'http://www.biography.com/', 'http://www.bestplaces.net/', 'http://thedailypositive.com/', 'http://igibud.com/', 'https://www.nigerianbulletin.com/', 'http://www.popsugar.com/', 'http://viraloften.com/', 'http://viralslot.com/', 'http://www.nbcconnecticut.com/', 'http://thatoregonlife.com/', 'http://www.doncasterfreepress.co.uk/', 'http://www.ayushvedainformatics.com/', 'http://teamrock.com/', 'http://www.wtkr.com/', 'http://www.dumpaday.com/', 'http://wane.com/', 'http://www.boredomkicker.com/', 'http://www.harley-davidson.com/', 'http://www.gatewaymedia.com/', 'https://www.roadsnacks.net/', 'http://rajasthanpatrika.patrika.com/', 'http://www.ddfl.org/', 'http://www.proudcons.com/', 'http://espn.go.com/', 'http://www.tigerdroppings.com/', 'http://www.commercialappeal.com/', 'https://www.nerdwallet.com/', 'http://www.manchestereveningnews.co.uk/', 'https://www.bungie.net/', 'http://www.nbclosangeles.com/', 'http://lajmi.net/', 'http://www.efecto24.com/', 'http://starsunfolded.com/', 'http://www.easterseals.com/', 'http://www.citizen-times.com/', 'http://www.powershow.com/', 'http://www.coshoctontribune.com/', 'http://www.lectlaw.com/', 'http://nbc4i.com/', 'http://www.phonearena.com/', 'http://www.thehollywoodgossip.com/', 'http://www.ibtimes.co.in/', 'http://www.russellgrant.com/', 'http://wsvn.com/', 'http://www.countercurrentnews.com/', 'https://themetix.com/', 'http://whnt.com/', 'http://wate.com/', 'https://translate.google.com/', 'http://www.onegreenplanet.org/', 'http://www.therebel.media/', 'http://www.dailyamerican.com/', 'http://www.celebchatter.com/', 'http://www.adaptnetwork.com/', 'http://epaper.tribuneindia.com/', 'http://www.fox35orlando.com/', 'http://www.englishforum.ch/', 'http://www.greatfallstribune.com/', 'http://celebritynetworths.org/', 'http://www.seriouseats.com/', 'http://limelightdaily.com/', 'http://ksn.com/', 'http://photo.net/', 'http://www.chicagotribune.com/', 'http://gl1800riders.com/', 'http://www.laacib.net/', 'http://www.baltimorebaseball.com/', 'http://www.gainesville.com/', 'http://www.dailymotion.com/', 'http://www.bellybelly.com./', 'http://theuspatriot.com/', 'http://updentity.rlecoalition.com/', 'http://www.afternoonspecial.com/', 'http://en.heroquizz.com/', 'http://www.nbcsandiego.com/', 'http://www.adweek.com/', 'http://www.wtae.com/', 'http://rdrnews.com/', 'http://abcnews.go.com/', 'http://www.clark.com/', 'http://www.fillthewell.com/', 'http://wsls.com/', 'http://farandulaecuatoriana.com/', 'http://knox.villagesoup.com/', 'http://www.moviefone.com/', 'http://www.rd.com/', 'http://udaipurkiran.com/', 'http://www.socialtheater.com/', 'https://multimedia.journalism.berkeley.edu/', 'http://fun.icoolbuzz.com/', 'http://wjtv.com/', 'http://www.mediavine.com/', 'http://www.money.usnews.com/', 'http://usfantasynews.com/', 'http://www.networkworld.com/', 'http://www.onlinevideoconverter.com/', 'https://www.fmcsa.dot.gov/', 'http://www.sportskeeda.com/', 'http://www.13wmaz.com/', 'http://s5.photobucket.com/', 'http://kdvr.com/', 'http://www.veritenews.com/', 'http://www.dailysikhupdates.com/', 'http://articles.orlandosentinel.com/', 'http://global.setopati.com/', 'http://hubfa.com/', 'http://www.theatlantic.com/', 'http://www.bildungsfoerderung-nepal.de/', 'http://inthesetimes.com/', 'http://iranianuk.com/', 'http://www.3clickadvance.com/', 'http://www.rawstory.com/', 'http://advertising.gawker.com/', 'http://automationnation.labtechsoftware.com/', 'http://new.digi.com.my/', 'http://www.tigernet.nic.in/', 'http://www.heroviral.com/', 'http://abc7ny.com/', 'http://www.themeparktourist.com/', 'http://www.arkansasonline.com/', 'http://www.londonbroadcastingcompany.com/', 'http://www.stereogum.com/', 'http://www.chicagobears.com/', 'http://www.97xonline.com/', 'http://www.frivolfluff.com/', 'http://www.metrotimes.com/', 'http://www.en.mentalfeed.com/', 'http://makeupandbeauty.com/', 'http://rickeysmileymorningshow.com/', 'http://www.csnne.com/', 'http://singaporenewsblog.com/', 'http://login.flipora.com/', 'http://www.candycrush-cheats.com/', 'http://www.onlyinyourstate.com/', 'http://sportsandgoal.com/', 'http://runthetrap.com/', 'http://deadline.com/', 'http://themeshreport.com/', 'http://www.khou.com/', 'http://www.frankiesfacts.com/', 'http://designtaxi.com/', 'http://www.viralwalrus.com/', 'http://www.theherald-news.com/', 'http://www.lohud.com/', 'http://nationalinterest.org/', 'http://gurmeet.net/', 'http://www.fergusfallsjournal.com/', 'http://www.divyabhaskar.co.in/', 'http://www.autoguide.com/', 'https://mail.cloudzimail.com/', 'http://www.wikipicky.com/', 'http://www.bjpenn.com/', 'http://www.trivalleycentral.com/', 'http://www.pro-football-reference.com/', 'http://www.metacritic.com/', 'http://pch.com/', 'http://www.brighthub.com/', 'http://www.austinbassfishing.com/', 'http://www.fangraphs.com/', 'http://www.estaily.com/', 'http://www.amny.com/', 'http://snewsi.com/', 'http://hellobeautiful.com/', 'http://www.pinkvilla.com/', 'http://5.189.169.69:8085/', 'http://www.defensivecarry.com/', 'http://www.guitarworld.com/', 'http://www.cnbc.com/', 'http://www.wokv.com/', 'http://screenvisionmedia.com/', 'http://www.hometownstations.com/', 'https://en.wikipedia.org/', 'http://www.pressconnects.com/', 'http://www.chicagoreader.com/', 'http://brainsmuggler.com/', 'http://hindi.oneindia.com/', 'http://vast-media.com/', 'http://www.sports-reference.com/', 'http://www.irontontribune.com/', 'http://mentalfloss.com/', 'http://www.kvue.com/', 'http://www.fox13news.com/', 'http://www.democraticunderground.com/', 'http://conservativepost.com/', 'http://koha.net/', 'http://www.nitansolutions.com/', 'http://www.kaizenmedia.com.au/', 'http://health.raftaar.in/', 'http://www.smithsonianmag.com/', 'http://www.phillyvoice.com/', 'http://www.homeclick.com/', 'http://www.quizzzat.com/', 'http://www.getsurrey.co.uk/', 'http://digitalsynopsis.com/', 'http://www.demandmedia.com/', 'http://www.11alive.com/', 'http://www.mirror.co.uk/', 'http://prodiynews.com/', 'http://adogslove.com/', 'http://www.naplesnews.com/', 'http://stackoverflow.com/', 'http://www.tegnafoundation.org/', 'http://www.delmarvanow.com/', 'http://naiduniaepaper.jagran.com/', 'http://www.wideopencountry.com/', 'http://www.help.disqus.com/', 'http://www.suggest.com/', 'http://www.cbssports.com/', 'http://thelibertarianrepublic.com/', 'http://www.superstarmagazine.com/', 'http://www.wftv.com/', 'http://www.nj.com/', 'http://www.xfinity.com/', 'https://www.woorank.com/', 'http://www.vhnd.com/', 'http://www.reshareworthy.com/', 'http://tattooartistmagazineblog.com/', 'http://wthitv.com/', 'http://www.pix11.com/', 'http://www.celebritynetworth123.com/', 'http://myautofeed.com/', 'http://sigtalk.com/', 'http://www.everydayfamily.com/', 'http://www.avsforum.com/', 'http://techbits.co.in/', 'http://www2.tmg.com.tw/', 'http://aptech.vn/', 'http://www.austindailyherald.com/', 'http://yamstar.com/', 'http://getinmybelly.com/', 'http://buzzingtrend.com/', 'http://kotaku.com/', 'http://moneyweek.com/', 'http://www.merriam-webster.com/', 'http://www.westernmassnews.com/', 'http://www.marionschools.net/', 'https://www.henley.ac.uk/', 'http://www.keepinspiring.me/', 'http://walletgroove.com/', 'http://floridaguntrader.com/', 'http://www.dailyrecord.com/', 'http://www.huffingtonpost.co.uk/', 'http://neave.com/', 'http://www.theguardian.com/', 'http://www.csnbayarea.com/', 'http://viralmozo.com/', 'http://www.nbcnewyork.com/', 'http://www.t-shirtforums.com/', 'http://www.fonepaw.com/', 'http://www.nbcbayarea.com/', 'http://www.sportsgrid.com/', 'http://lynrd.com/', 'http://penelope.uchicago.edu/', 'https://urlquery.net/', 'http://www.golfchannel.com/', 'http://www.shtyle.fm/', 'http://www.hondaatvforums.net/', 'http://www.onenewsnow.com/', 'http://www.timewarnercable.com/', 'http://turtleboysports.com/', 'http://sharestills.com/', 'http://www.edgetrends.com/', 'http://jamaica-star.com/', 'http://www.wpbeginner.com/', 'http://guesstheemoji-answers.com/', 'http://www.cbsnews.com/', 'https://www.eaa.unsw.edu.au/', 'http://beforeitsnews.com/', 'http://www.socialholicnetwork.com/', 'http://www.edutopia.org/', 'http://www.muyinteresante.es/', 'http://www.kwwl.com/', 'http://answermug.com/', 'http://madworldnews.com/', 'http://financialjuneteenth.com/', 'http://www.northeasttimes.com/', 'http://perezhilton.com/', 'http://www.instyle.com.au/', 'http://hindi.insistpost.com/', 'http://www.onecountry.com/', 'http://xobenzo.com/', 'http://articles.dailypilot.com/', 'http://www.challengertalk.com/', 'https://constitutionalrightspac.com/', 'http://www.azcentral.com/', 'http://praiseindy.hellobeautiful.com/', 'http://www.utahsright.com/', 'http://www.bombist.com/', 'http://www.gulte.com/', 'http://nativewarriors.co/', 'http://www.klkntv.com/', 'http://indiatoday.intoday.in/', 'http://thekrazycouponlady.com/', 'http://www.kob.com/', 'http://www.sacurrent.com/', 'http://www.fox26houston.com/', 'http://crimefeed.com/', 'http://www.fox40.com/', 'http://www.pawmygosh.com/', 'http://indiancountrytodaymedianetwork.com/', 'http://www.forbes.com/', 'http://www.k99online.com/', 'http://cw39.com/', 'http://www.frick.org/', 'http://www.goal.com/', 'http://laughmine.com/', 'http://www.statesmanjournal.com/', 'http://www.power953.com/', 'http://www.mrctv.org/', 'http://www.valnetinc.com/', 'http://www.nationnews.com/', 'http://www.airforcetimes.com/', 'http://www.lovewale.com/', 'http://www.tactical-life.com/', 'http://officialhuskylovers.com/', 'http://www.peekworthy.com/', 'http://www.fox46charlotte.com/', 'http://www.defymedia.com/', 'http://www.thepoke.co.uk/', 'http://jewishbusinessnews.com/', 'http://www.digitaltrends.com/', 'http://esimedia.co.uk/', 'http://www.carbonmedia.com/', 'http://www.expressandstar.com/', 'http://politicalhumor.about.com/', 'http://meaww.com/', 'http://www.siteprice.org/', 'http://www.thelocal.fr/', 'http://www.loanstradermba.com/', 'http://theselfsufficientliving.com/', 'http://healthprep.com/', 'http://americanupbeat.com/', 'http://suadvance.syr.edu/', 'http://www.liverpoolecho.co.uk/', 'http://www.fresnobee.com/', 'http://thomsonreuters.com/', 'http://wkrn.com/', 'http://articles.sun-sentinel.com/', 'http://www.freemake.com/', 'http://www.taringa.net/', 'http://www.theplace2.ru/', 'http://animalmozo.com/', 'http://lightersideofrealestate.com/', 'http://spainclick.com/', 'http://www.rediff.com/', 'http://topdocumentaryfilms.com/', 'http://www.techsupportforum.com/', 'http://worldinsidepictures.com/', 'http://americanactionnews.com/', 'http://www.jagran.com/', 'http://www.performgroup.com/', 'http://abc27.com/', 'http://hexagram.com/', 'http://www.haber7.com/', 'http://www.stylebistro.com/', 'http://www.ledger-enquirer.com/', 'http://www.csnchicago.com/', 'http://loquenosabia.com/', 'http://www.wqad.com/', 'https://www.pinterest.com/', 'https://in.pinterest.com/', 'http://www.highya.com/', 'http://www.elpasotimes.com/', 'http://photos.filmibeat.com/', 'http://tamil.oneindia.com/', 'http://www.hivequal.org/', 'http://www.allthingsceleb.com/', 'http://www.jezebel.com/', 'http://5newsonline.com/', 'http://www.latestviralspot.com/', 'http://www.usmessageboard.com/', 'http://www.vox.com/', 'http://www.newjerseyhunter.com/', 'http://www.nydailynews.com/', 'http://www.businesstoday.in/', 'http://www.boxingscene.com/', 'http://www.interbasket.net/', 'http://www.bollywoodbubble.com/', 'http://www.courier-journal.com/', 'http://www.thv11.com/', 'http://www.joemygod.com/', 'http://www.littlethings.com/', 'https://plus.google.com/', 'http://www.maxpreps.com/', 'http://www.thenewstribune.com/', 'http://www.imdb.com/', 'http://6abc.com/', 'http://paparazzija.com/', 'http://www.wisn.com/', 'http://travellenses.com/', 'http://www.jconline.com/', 'http://www.samuel-warde.com/', 'http://www.steelersdepot.com/', 'http://www.buzzfaze.com/', 'http://i24mujer.com/', 'http://freshome.com/', 'http://thestir.cafemom.com/', 'http://www.investors.com/', 'http://www.jpost.com/', 'http://www.baseball-reference.com/', 'http://www.guampdn.com/', 'http://weeklyhoroscope.com/', 'http://www.nationalmemo.com/', 'http://www.blackmorevale.co.uk/', 'http://www.pasionaguila.com/', 'http://www.bored.com/', 'http://www.cronica.com/', 'http://www.tapology.com/', 'http://www.pcworld.com/', 'https://www.linkedin.com/', 'http://heavy.com/', 'http://www.mediastinct.com/', 'http://www.twitlonger.com/', 'http://www.conventuslaw.com/', 'http://www.elephantjournal.com/', 'http://viralsection.com/', 'http://www.acu.edu/', 'http://www.dodgetalk.com/', 'http://www.archipo.net/', 'http://movie.geourdu.com/', 'http://www.arcticchat.com/', 'http://www.bollywoodlife.com/', 'http://www.manatelugumovies.net/', 'http://www.chicago.suntimes.com/', 'http://www.businessinsider.com/', 'http://www.dailyhaha.com/', 'http://www.powerlineblog.com/', 'http://americancolumn.com/', 'http://jspace.com/', 'http://www.wlwt.com/', 'http://www.tickld.com/', 'http://www.viralpie.net/', 'http://www.thegloss.com/', 'http://www.moddb.com/', 'http://tankler.com/', 'http://www.dailycomet.com/', 'http://www.ktbs.com/', 'http://www.hitfix.com/', 'http://www.badabun.net/', 'https://theconservativetreehouse.com/', 'http://nymag.com/', 'http://www.columbiatribune.com/', 'http://www.jagranjunction.com/', 'https://www.theguardian.com/', 'http://www.wrestlingforum.com/', 'http://www.vancitybuzz.com/', 'http://anime-joy.tv/', 'http://www.3pdevelopment.com/', 'http://forexmasternet.com/', 'http://www.satspapers.org/', 'http://www.v4vartha.com/', 'http://www.nbcmiami.com/', 'http://time.com/', 'http://www.ufc.com/', 'http://www.leftwinglock.com/', 'http://www.knowyourmobile.com/', 'http://www.12news.com/', 'http://my.screenname.aol.com/', 'http://www.queen-of-theme-party-games.com/', 'http://www.ratemyteachers.com/', 'http://www.leaderpub.com/', 'http://www.cheatsheet.com/', 'http://www.adn.com/', 'http://www.likesharetweet.com/', 'http://www.kctv5.com/', 'http://www.ign.com/', 'http://www.playbuzz.com/', 'http://www.schurz.com/', 'http://www.wthr.com/', 'http://www.starnewsonline.com/', 'http://amicohoops.net/', 'https://www3.epa.gov/', 'http://www.jdpower.com/', 'http://www.leadertelegram.com/', 'http://www.lifedaily.com/', 'http://www.mycentraljersey.com/', 'https://www.timewarnercable.com/', 'http://allchristiannews.com/', 'https://misedades.wordpress.com/', 'http://www.popnhop.com/', 'http://www.webdunia.com/', 'http://www.yourdailydip.com/', 'http://moviepilot.com/', 'http://en.nametests.com/', 'http://www.mydailymoment.com/', 'http://www.knoxnews.com/', 'http://www.foxcarolina.com/', 'http://www.zimbabwesituation.com/', 'http://colleges.usnews.rankingsandreviews.com/', 'http://www.lgbtqnation.com/', 'http://www.boredomtherapy.com/', 'http://www.hitc.com/', 'http://forharriet.spreadshirt.com/', 'http://www.wmtw.com/', 'https://gist.github.com/', 'http://www.komando.com/', 'http://www.eonline.com/', 'http://theultralinx.com/', 'http://www.games2rule.com/', 'http://www.themalaymailonline.com/', 'http://www.easyaupair.com/', 'http://www.foresthistory.org/', 'http://www.sbnation.com/', 'http://www.businessinsider.in/', 'http://www.connectsavannah.com/', 'https://support.office.com/', 'http://www.nbcwashington.com/', 'http://www.i24news.tv/', 'http://www.mcskinsearch.com/', 'http://www.notator.org/', 'http://www.guidingtech.com/',
           'http://health.usnews.com/', 'http://bdnews24.com/', 'http://www.msn.com/', 'http://www.news-leader.com/', 'http://www.etonline.com/', 'http://fanbuzz.rare.us/', 'http://www.yesimright.com/', 'http://www.wmata.com/', 'http://reservaviral.com/', 'http://www.thestranger.com/', 'http://www.gossipcop.com/', 'http://www.mcclatchy.com/', 'http://wspa.com/', 'http://www.ap.org/', 'http://www.lansingstatejournal.com/', 'http://wbtw.com/', 'http://www.womens-forum.com/', 'http://www.thelocal.it/', 'http://www.freemalaysiatoday.com/', 'http://www.meetup.com/', 'http://www.yardbarker.com/', 'http://thebark.com/', 'http://www.delawareonline.com/', 'http://wfla.com/', 'http://irokotv.com/', 'http://www.jamaicaobserver.com/', 'https://www.rt.com/', 'http://www.telegraph.co.uk/', 'http://www.grupopalomooficial.com/', 'http://www.wbir.com/', 'http://www.allnewspipeline.com/', 'http://www.thisblewmymind.com/', 'http://www.accountkiller.com/', 'http://www.malaysiakini.com/', 'http://www.historyfanatic.com/', 'http://www.vcstar.com/', 'http://www.hollywoodtake.com/', 'http://russia-insider.com/', 'http://onwardstate.com/', 'http://247wallst.com/', 'http://mytimes.indiatimes.com/', 'http://khon2.com/', 'http://getcomics.info/', 'http://www.grubstreet.com/', 'http://www.nola.com/', 'http://www.business-standard.com/', 'http://www.pennlive.com/', 'http://www.eveningsun.com/', 'https://www.youtube.com/', 'http://www.rantsports.com/', 'http://www.urbanjoker.com/', 'http://www.andhravilas.net/', 'http://www.publicopiniononline.com/', 'http://epaper.jagran.com/', 'http://wwlp.com/', 'http://www.dhakatribune.com/', 'http://www.lyricsmint.com/', 'http://www.primeromining.com/', 'http://neurogadget.com/', 'http://chicagoist.com/', 'http://www.cio.in/', 'http://www.pushsquare.com/', 'http://support.dailyhunt.in/', 'http://therealdeal.com/', 'http://www.dailyherald.com/', 'http://dcist.com/', 'http://www.greedyrates.ca/', 'http://www.jsonline.com/', 'http://www.hometipsworld.com/', 'http://wavy.com/', 'http://whois.webslookup.com/', 'http://www.picayuneitem.com/', 'http://www.dallascowboys.com/', 'http://qpolitical.com/', 'http://www.news-press.com/', 'http://www.knowable.com/', 'http://www.kcchronicle.com/', 'http://www.newsoxy.com/', 'http://www.andhraheadlines.com/', 'http://www.manithan.com/', 'http://cdn.eblogfa.com/', 'http://www.ketv.com/', 'http://www.unmotivating.com/', 'http://www.luxxory.com/', 'http://www.hotmomsclub.com/', 'http://wrbl.com/', 'http://www.omgfacts.com/', 'http://fox13now.com/', 'http://theweek.com/', 'http://www.weqyoua.net/', 'http://www.stackoverflow.com/', 'http://www.news24nepal.tv/', 'http://www.petmd.com/', 'http://yourdailyjournal.com/', 'http://www.wpworking.com/', 'http://gadgets.ndtv.com/', 'http://www.extremefreestyle.wordpress.com/', 'http://www.nymag.com/', 'https://www.demandmedia.com/', 'http://www.treasurenet.com/', 'http://americanlivewire.com/', 'http://www.bitethebuzz.com/', 'http://www.relativelyinteresting.com/', 'http://www.ctnow.com/', 'http://www.montgomeryadvertiser.com/', 'http://weeklydailynews.com/', 'http://www.poughkeepsiejournal.com/', 'http://www.mandatory.com/', 'http://www.fredericksburg.com/', 'http://recomhub.com/', 'http://www.easycounter.com/', 'http://www.tmnews.com/', 'http://access.mvps.org/', 'http://www.mdsaude.com/', 'http://www.semesterz.com/', 'http://www.gannettnj.com/', 'http://www.creativebloq.com/', 'http://www.polltracker.talkingpointsmemo.com/', 'http://www.tamilstar.com/', 'http://www.browndailyherald.com/', 'http://www.timesofindia.indiatimes.com/', 'http://www.orlandoweekly.com/', 'http://vodafone.intelliresponse.com/', 'http://www.out.com/', 'http://www.marunadanmalayali.com/', 'http://www.youpak.com.pk/', 'http://www.sfgate.com/', 'http://www.magnifymoney.com/', 'http://www.one-europe.info/', 'http://www.packersnews.com/', 'http://www.mobilerated.com/', 'https://www.englishandmedia.co.uk/', 'https://www.como.gov/', 'http://wtkr.com/', 'http://www.golf.com/', 'http://www.startribune.com/', 'http://thehealthyapple.com/', 'http://noozley.com/', 'http://www.celiac.com/', 'http://www.boredpanda.com/', 'http://myfirstclasslife.com/', 'http://www.wbal.com/', 'http://help.forumotion.com/', 'http://clashofclansbuilder.com/', 'http://www.freep.com/', 'http://stripe.com/', 'http://www.polarisatvforums.com/', 'http://www.wnd.com/', 'http://www.advanc-ed.org/', 'http://www.horowitzfreedomcenter.org/', 'http://www.wfaa.com/', 'http://forums.cgsociety.org/', 'http://www.blueridgenow.com/', 'http://www.funnyjunk.com/', 'http://www.can-amforum.com/', 'http://cash4humor.com.w3snoop.com/', 'http://edition.cnn.com/', 'http://wnep.com/', 'http://domainsigma.com/', 'http://thebellevuegazette.com/', 'http://www.25cineframes.com/', 'http://www.downloadmp3free.site/', 'http://www.lifed.com/', 'http://thegedsection.com/', 'http://www.fox17online.com/', 'http://www.carlicenseplatess.com/', 'http://virginiatech.sportswar.com/', 'http://www.gamespot.com/', 'http://games.msn.com/', 'http://www.cricbuzz.com/', 'http://www.odditycentral.com/', 'http://www.maduradas.com/', 'http://youseeingthis.com/', 'http://en.pandacat.me/', 'http://pilotonline.com/', 'http://www.usatoday.com/', 'http://www.eugeneoloughlin.com/', 'http://www.stiripesurse.ro/', 'http://www.mydailyviral.com/', 'http://www.daijiworld.com/', 'http://taptitude.wikia.com/', 'http://www.rap-up.com/', 'http://bigstory.ap.org/', 'http://www.woven.com/', 'http://noticiasraras.es/', 'http://www.thelocal.com/', 'http://www.stltoday.com/', 'http://www.ydr.com/', 'http://www.tv.indonewyork.com/', 'http://www.capegazette.com/', 'http://www.abovetopsecret.com/', 'http://pamelageller.com/', 'http://www.muscleandfitness.com/', 'http://www.independentmail.com/', 'http://www.deadspin.com/', 'http://m14forum.com/', 'https://discussions.apple.com/', 'http://www.nbc.com/', 'http://theybf.com/', 'http://fox6now.com/', 'http://www.freedomnewspaper.com/', 'http://www.pressroomvip.com/', 'http://lifehacker.com/', 'http://cbs4indy.com/', 'http://www.centralmaine.com/', 'http://www.tupaki.com/', 'http://www.instructables.com/', 'http://www.kcra.com/', 'http://expressclassifiedstt.com/', 'http://moneyfeedsme.com/', 'http://www.chicagonow.com/', 'http://fdprn.com/', 'http://www.tallmadgeexpress.com/', 'http://www.kmbc.com/', 'http://www.dictionary.com/', 'http://www.robinspost.com/', 'http://www.americanitsolutions.com/', 'http://www.fox4kc.com/', 'http://inextlive.jagran.com/', 'https://www.hypestat.com/', 'http://freebeacon.com/', 'https://www.eduplace.com/', 'http://digitizingworld.com/', 'http://tierracuriosa.net/', 'http://tribune.com.pk/', 'http://teslcanadajournal.ca/', 'https://orders.grinandbearit.com.au/', 'http://www.mindbuzz.com.au/', 'http://www.lonny.com/', 'http://www.parismatch.com/', 'http://www.metalinjection.net/', 'http://www.90min.in/', 'http://www.charlotteobserver.com/', 'http://www.chieftain.com/', 'http://www.clickhole.com/', 'https://www.mentor.com/', 'http://www.gaystarnews.com/', 'http://www.emmis.com/', 'http://mansion-homes.com/', 'http://www.liveleak.com/', 'http://www.statisticbrain.com/', 'http://www.aenetworks.com/', 'http://www.floridatoday.com/', 'http://www.foxla.com/', 'http://www.10best.com/', 'http://www.oranews.tv/', 'http://www.authoritynutrition.com/', 'http://www.al.com/', 'http://download.cnet.com/', 'http://buzzsharer.com/', 'http://www.purplemath.com/', 'http://www.amazon.com/', 'http://golf.swingbyswing.com/', 'http://www.shtfplan.com/', 'http://www.ruidosonews.com/', 'http://www.actionnewsjax.com/', 'http://www.scoopwhoop.com/', 'http://www.alternativehealthreports.org/', 'http://www.bollywoodgaram.com/', 'http://www.whotv.com/', 'http://www.rense.com/', 'http://fixsitespeed.com/', 'http://www.nbcnews.com/', 'http://www.mcclatchydc.com/', 'http://sportschatplace.com/', 'http://www.argusleader.com/', 'http://www.football-italia.net/', 'http://www.mmaglobal.com/', 'http://news10.com/', 'http://www.greenvillewater.com/', 'http://www.israelhayom.com/', 'http://www.wnem.com/', 'http://viralguppy.com/', 'http://www.nwherald.com/', 'http://elitedaily.com/', 'http://consequenceofsound.net/', 'http://www.mobileposse.com/', 'http://www.butthatsnoneofmybusiness.com/', 'http://www.sportsblog.com/', 'http://sharethe.buzz/', 'http://www.sooziq.com/', 'http://www.sniperforums.com/', 'http://wtvr.com/', 'http://www.knbr.com/', 'http://thenextweb.com/', 'http://www.ktvu.com/', 'http://www.wfmj.com/', 'http://www.goodnewsnetwork.org/', 'http://www.sofeminine.co.uk/', 'http://www.moneysupermarket.com/', 'http://gothamist.com/', 'https://www.bostonglobe.com/', 'http://homestead-and-survival.com/', 'http://www.wonderwall.com/', 'http://www.realclearpolitics.com/', 'http://www.socialdunks.com/', 'http://www.majorten.com/', 'http://www.ba-bamail.com/', 'http://www.mirchi9.com/', 'http://www.kdrv.com/', 'http://www.bristolpost.co.uk/', 'http://www.foodnetwork.com/', 'http://www.realitywives.net/', 'http://toprightnews.com/', 'http://www.whiskeyriff.com/', 'http://www.biggeekdad.com/', 'http://www.digi.com/', 'http://www.cbs.com/', 'http://ummat.net/', 'http://www.expatforum.com/', 'http://www.autoblog.com/', 'http://www.nbcunicareers.com/', 'http://www.kraftrecipes.com/', 'http://www.virtualjerusalem.com/', 'http://articles.baltimoresun.com/', 'http://www.streetinsider.com/', 'http://www.captainquizz.com/', 'http://www.infoworld.com/', 'http://www.threedollarclick.com/', 'https://www.ziffdavis.com/', 'http://www.hsi.com.hk/', 'http://www.answers.com/', 'http://www.timesrecordnews.com/', 'http://www.blackpoolgazette.co.uk/', 'http://www.fox5dc.com/', 'http://www.fastfoodmenuprices.com/', 'http://www.whio.com/', 'http://www.todayifoundout.com/', 'http://thirdrosemedia.com/', 'http://discoverinformation.com/', 'http://www.whattoexpect.com/', 'http://www.m.newson6.com/', 'http://mypix.cbs19.tv/', 'http://gossiponthis.com/', 'http://www.cycleworld.com/', 'https://www.vg247.com/', 'http://realtimepolitics.com/', 'http://www.thetimesherald.com/', 'http://www.jerusalemonline.com/', 'http://www.sandiegouniontribune.com/', 'http://fox59.com/', 'https://developer.apple.com/', 'http://theblackbusinessschool.com/', 'http://www.cracked.com/', 'http://noticiasvzla.com/', 'http://www.johnstonpress.co.uk/', 'https://itunes.apple.com/', 'http://www.gawker.com/', 'http://thehill.com/', 'http://www.boneyaad.net/', 'http://www.vervemobile.com/', 'http://www.avclub.com/', 'http://tamilgun.com/', 'http://fox5sandiego.com/', 'http://www.vanguardngr.com/', 'http://usbizznews.com/', 'http://xceleb-iconsx.livejournal.com/', 'http://www.mylol.com/', 'http://free.mauj.com/', 'https://uplay.ubi.com/', 'http://www.forexlive.com/', 'http://www.log3condo.com/', 'http://w3bin.com/', 'http://www.motorcyclistonline.com/', 'http://www.iloveoldschoolmusic.com/', 'http://www.necttfkc.org/', 'http://thetruthdivision.com/', 'http://forums.androidcentral.com/', 'http://www.ford.com/', 'http://www.itsthevibe.com/', 'http://www.examiner.com/', 'http://92q.com/', 'http://www.foxdeportes.com/', 'http://www.betches.com/', 'http://www.techrepublic.com/', 'http://www.bonniercorp.com/', 'http://www.socialsweethearts.de/', 'http://www.theblaze.com/', 'http://www.dobberprospects.com/', 'http://eastcoastcountdown.com/', 'https://m.facebook.com/', 'http://www.whois.com/', 'http://www.indystar.com/', 'http://lifetailored.com/', 'http://m.atchuup.com/', 'http://www.newshijack.net/', 'http://www.funsted.com/', 'http://www.modernghana.com/', 'http://www.mangareader.net/', 'http://www.mensfitness.com/', 'http://www.vbulletin.com/', 'http://www.donbalon.com/', 'http://injo.com/', 'http://www.ibtimes.co.uk/', 'http://www.faithit.com/', 'http://www.pressdemocrat.com/', 'http://www.collegefootball.ap.org/', 'http://www.oddee.com/', 'http://www.hellawella.com/', 'http://www.latimes.com/', 'http://www.sherdog.com/', 'http://www.t3.com/', 'http://www.wcsh6.com/', 'https://www.capitalone.com/', 'http://www.thegazette.com/', 'http://www.goodhousekeeping.com/', 'http://www.ted.com/', 'http://www.maverickforums.net/', 'http://www.elitereaders.com/', 'http://www.inthefashionloop.com/', 'http://mundohispanico.com/', 'https://disqus.com/', 'http://soapcentral.com/', 'http://www.theresourcegirls.com/', 'https://sitedelver.com/', 'http://blackdoctor.org/', 'http://www.chowhound.com/', 'http://weheartit.com/', 'http://www.bostonmagazine.com/', 'http://www.wsoctv.com/', 'http://dmediamom.com/', 'http://www.home-designing.com/', 'http://www.1728.org/', 'http://www.lovebscott.com/', 'http://www.y8.com/', 'http://www.bradenton.com/', 'http://www.lucianne.com/', 'http://www.popscreen.com/', 'http://www.thelocal.dk/', 'http://www.newsandpromotions.com/', 'http://boredomtherapy.com/', 'http://crooksandliars.com/', 'http://www.thebaseballcube.com/', 'https://nobullying.com/', 'http://illuminatipuppet.com/', 'http://whois.domaintools.com/', 'http://www.mrnoggin.com/', 'http://www.theamericanconservative.com/', 'http://home.bt.com/', 'http://www.stlmugshots.com/', 'http://brainberries.co/', 'http://www.trx250r.net/', 'http://www.7apps.me/', 'http://www.savagesports.tv/', 'http://cwww.odyaray.com/', 'http://www.sqlservercentral.com/', 'http://generalquizz.com/', 'http://www.ivelovefruit.com/', 'http://www.indiancinemagallery.com/', 'http://www.thedieselstop.com/', 'http://www.escapehere.com/', 'http://www.ajc.com/', 'http://www.damnlol.com/', 'http://www.rotoworld.com/', 'https://www.3clickadvance.com/', 'http://www.quiznatic.com/', 'http://www.wltx.com/', 'http://www.fizzdot.com/', 'http://www.kesq.com/', 'http://www.mcrecordonline.com/', 'http://www.fightnews.com/', 'http://lee.net/', 'http://www.birminghammail.co.uk/', 'http://www.farrahgray.com/', 'http://www.factslides.com/', 'http://fox21news.com/', 'http://ktla.com/', 'http://www.nationalreview.com/', 'http://www.newson6.com/', 'http://www.blabbermouth.net/', 'http://thecelebritylane.com/', 'http://www.snapchat.codes/', 'http://aramisinteractive.com/', 'http://www.canterbury.ac.nz/', 'http://www.wcnc.com/', 'http://www.airliners.net/', 'http://manplate.com/', 'http://www.nbcsports.com/', 'http://www.thesmokinggun.com/', 'http://www.chatsports.com/', 'http://www.oliveboard.in/', 'http://www.qpolitical.com/', 'http://bcmediagroup.com/', 'http://vidmax.com/', 'http://www.nintendolife.com/', 'https://za.pinterest.com/', 'http://sitegur.com/', 'http://www.policybazaar.com/', 'http://www.entimports.com/', 'http://www.derbytelegraph.co.uk/', 'http://gaana.com/', 'http://www.wmur.com/', 'https://pages.questexweb.com/', 'http://www.scarymommy.com/', 'http://sostrenews.com/', 'http://www.observer-reporter.com/', 'http://www.kdvr.com/', 'http://www.4alltravelers.com/', 'http://www.disqus.com/', 'http://galleryroulette.com/', 'http://www.huffingtonpost.ca/', 'https://subscene.com/', 'https://fitnessrepublic.com/', 'http://www.hollywoodreporter.com/', 'http://www.reviewjournal.com/', 'http://epaper.telegraphindia.com/', 'http://www.tvone.tv/', 'http://canadafreepress.com/', 'https://www.sharcnet.ca/', 'https://www.en.advertisercommunity.com/', 'http://www.lifehacker.co.in/', 'http://www.thesportreview.com/', 'http://allforfashiondesign.com/', 'http://www.westchestermagazine.com/', 'http://www.dennis.co.uk/', 'http://wpri.com/', 'http://blackamericaweb.com/', 'http://www.todaysbuzz.com/', 'http://www.stylemotivation.com/', 'http://philadelphia.cbslocal.com/', 'http://www.attitudemedia.com/', 'http://www.fox5atlanta.com/', 'http://extreme.com/', 'http://www.afv.com/', 'http://www.therockwfk.com/', 'http://hiphollywood.com/', 'http://designyoutrust.com/', 'http://www.euronews.com/', 'http://www.tuscaloosanews.com/', 'http://www.komentbox.com/', 'http://www.triviahive.com/', 'http://www.sciencealert.com/', 'http://www.ancient-code.com/', 'http://www.menshealth.com/', 'http://www.brainjet.com/', 'http://www.phillymag.com/', 'http://www.wgal.com/', 'http://www.brobible.com/', 'http://www.lakako.com/', 'http://www.andalusiastarnews.com/', 'http://www.vulture.com/', 'http://hotspotatl.com/', 'http://thefederalist.com/', 'http://about.dose.com/', 'http://www.verticalscope.com/', 'http://wtnh.com/', 'http://mediastinct.com.cutestat.com/', 'http://www.taosnews.com/', 'http://www.greenbaypressgazette.com/', 'http://www.saukvalley.com/', 'http://unofficialnetworks.com/', 'http://www.idownloadblog.com/', 'https://kinja.com/', 'http://www2.ljworld.com/', 'http://www.realworldsurvivor.com/', 'http://www.tmz.com/', 'http://www.proceso.com.mx/', 'http://panlasangpinoy.com/', 'http://www.gatewaymedia.in/', 'http://www.hoochfootball.net/', 'http://centurycycles.com/', 'http://www.reuters.com/', 'http://www.fox25boston.com/', 'http://www.tribunemedia.com/', 'http://conditions.healthgrove.com/', 'http://www.nottinghampost.com/', 'http://www.fox9.com/', 'http://www.reddit.com/', 'http://www.merging-markets.com/', 'http://how-mylot-works.blogspot.in/', 'http://www.wikihow.com/', 'http://www.234pulse.com/', 'http://www.doyouremember.co.uk/', 'http://www.bostonteapartyship.com/', 'http://www.stphilipsprimaryschool-litherland.co.uk/', 'http://www.clevescene.com/', 'http://shop.tacticalshit.com/', 'http://www.thelocal.se/', 'http://blog.travelogyindia.com/', 'http://www.elledecor.com/', 'http://www.alabasterreporter.com/', 'http://www.cpxi.com/', 'http://thechive.com/', 'http://www.timesofisrael.com/', 'http://www.kiro7.com/', 'http://fox8.com/', 'https://service.usatoday.com/', 'http://www.lcsun-news.com/', 'http://www.bloomberg.com/', 'http://www.ezoic.com/', 'http://djournal.com/', 'http://www.bomboradyo.com/', 'http://www.pophitz.com/', 'http://yourspace.reporternews.com/', 'http://www.tvfanatic.com/', 'http://addisvideo.net/', 'http://www.destinationseeker.com/', 'http://www.thefactsite.com/', 'http://onlinehealth.wiki/', 'http://www.alternativenation.net/', 'http://www.statesman.com/', 'http://beatofhawaii.com/', 'http://americangg.net/', 'http://jalopnik.com/', 'http://epaper.greaterkashmir.com/', 'http://www.grizzlycentral.com/', 'http://www.hollywoodbuzz.tv/', 'http://www.greatandhra.com/', 'https://live.paloaltonetworks.com/', 'http://www.politicschatter.com/', 'http://intvseries.com/', 'http://www.chronicletimes.com/', 'http://www.tyronetimes.co.uk/', 'http://politicops.com/', 'http://epaper.dailypostindia.com/', 'http://thewhiskeywash.com/', 'https://sitereview.bizzybud.com/', 'http://www.wehuntedthemammoth.com/', 'http://www.theblacksheartimes.com/', 'http://www.bewebsmart.com/', 'http://www.letstalkaboutsport.blogspot.in/', 'http://www.worldlifestyle.com/', 'http://www.myrtlebeachonline.com/', 'http://www.foxsports.com/', 'http://aristeguinoticias.com/', 'http://www.wsmv.com/', 'http://www.huffingtonpost.com/', 'http://buzzlie.com/', 'http://tvbythenumbers.zap2it.com/', 'http://www.hollywood.com/', 'http://www.pressherald.com/', 'http://betrending.com/', 'http://www.theleafchronicle.com/', 'http://www.democratandchronicle.com/', 'http://www.courant.com/', 'http://www.dailykos.com/', 'https://www.accesshollywood.com/', 'http://www.quizbone.com/', 'http://atlantablackstar.com/', 'http://hwof.mobi/', 'http://www.attractionsofamerica.com/', 'http://www.coloradoan.com/', 'https://bitchmedia.org/', 'http://www.webtreeonline.com/', 'http://www.mediaite.com/', 'http://racingdudes.com/', 'http://clintonnc.com/', 'http://artdaily.com/', 'http://memeburn.com/', 'http://www.plus3network.com/', 'http://www.buddytv.com/', 'http://wric.com/', 'http://www.mediageneral.com/', 'http://www.alltop.com/', 'http://epaper.siasat.com/', 'http://aajtak.intoday.in/', 'http://www.digitimes.com/', 'http://www.kmplayer.com/', 'http://www.hotsited.com/', 'http://www.fixsitespeed.com/', 'http://www.thewashingtondailynews.com/', 'https://www.gomlab.com/', 'http://www.americanmediainc.com/', 'https://www.tripadvisor.com/', 'http://www.caribbean360.com/', 'http://alltopics.com/', 'http://www.gosanangelo.com/', 'http://www.theladbible.com/', 'http://www.thegrumpyfish.com/', 'http://www.wktv.com/', 'http://www.wsj.com/', 'http://www.politico.com/', 'http://www.agweb.com/', 'http://www.hearstmagazines.co.uk/', 'https://www.pubgalaxy.com/', 'https://www.threedollarclick.com/', 'https://skepchick.org/', 'http://mcj3media.com/', 'http://www.fame10.com/', 'https://www.facebook.com/', 'http://ecowatch.com/', 'http://behindwoods.com/', 'http://www.tasteofhome.com/', 'http://247sports.com/', 'http://www.scoop.it/', 'http://www.theonion.com/', 'http://www.fox2detroit.com/', 'http://bigteams.com/', 'http://walkingdead.wikia.com/', 'http://www.theneworleansadvocate.com/', 'http://www.watchtheyard.com/', 'http://www.tsiterank.com/', 'http://www.mrc.org/', 'http://www.wxii12.com/', 'http://www.zengardner.com/', 'http://sharedable.com/', 'http://www.courierpostonline.com/', 'http://www.cincinnati.com/', 'http://en.vonvon.me/', 'http://www.chartattack.com/', 'http://advance.oregonstate.edu/', 'http://www.tamiltvshows.net/', 'https://www.scribd.com/', 'http://www.dailymail.co.uk/', 'http://www.jewsnews.co.il/', 'http://www.100picsquizanswers.net/', 'http://mypraiseatl.hellobeautiful.com/', 'http://www.animeyt.tv/', 'https://play.google.com/', 'http://www.troymessenger.com/', 'http://www.quirkyfeeds.com/', 'http://www.wherevent.com/', 'http://www.dramaonline.com/', 'http://www.georgetangchineseastrology.com/', 'http://www.lifeisbig.tv/', 'http://www.garagejournal.com/', 'http://classimatic.com/', 'http://www.heraldargus.com/', 'http://www.adpushup.com/', 'http://www.viralnova.com/', 'http://www.nerdwallet.com/', 'http://www.thewrap.com/', 'http://www.justjared.com/', 'http://www.livehindustan.com/', 'http://in.reuters.com/', 'http://talent.studiod.com/', 'http://socawlege.com/', 'http://blog.therealrickeysmiley.com/', 'http://nickoskitchen.com/', 'http://www.fox7austin.com/', 'http://www.ktvn.com/', 'http://rugerforum.net/', 'http://www.thechive.com/', 'http://www.courierpress.com/', 'https://soundcloud.com/', 'http://www.makeuseof.com/', 'http://petapixel.com/', 'http://www.mrfood.com/', 'http://www.activebeat.com/', 'http://www.lyvedigital.com/', 'http://www.eddale.co/', 'http://www.wkyc.com/', 'http://www.megaphonetv.com/', 'http://www.stabroeknews.com/', 'http://www.embarrassingproblems.com/', 'http://www.nytimes.com/', 'http://wtop.com/', 'http://www.sportingnews.com/', 'http://www.mindblowingvideos.com/', 'http://www.eurosport.com/', 'http://sfcusa.org/', 'http://digiday.com/', 'http://wgnradio.com/', 'http://www.israelnationalnews.com/', 'http://kickerdaily.com/', 'http://buddymd.com/', 'http://www.localpulse.net/', 'http://mlbstartingnine.com/', 'http://www.trendelier.com/', 'http://dailypakistan.com.pk/', 'http://viralfury.com/', 'http://www.ziffdavis.com/', 'http://www.thaivisa.com/', 'https://login.thestarpress.com/', 'http://healthypanda.net/', 'http://www.sunherald.com/', 'http://www.wesh.com/', 'http://www.clicksafety.com/', 'http://www.iflscience.com/', 'http://www.breitbart.com/', 'http://www.fool.com/', 'http://www.spaste.com/', 'http://www.commanderforums.org/', 'http://www.droid-life.com/', 'http://www.thecrux.com/', 'http://www.huffingtonpost.in/', 'http://www.wpxi.com/', 'http://help.disqus.com/', 'https://www.maplesoft.com/', 'https://help.disqus.com/', 'http://gmtnation.com/', 'http://www.adelaidenow.com.au/', 'http://www.afrimedia.org/', 'http://www.tellychakkar.com/', 'https://twitter.com/', 'http://www.indiatrendingnow.com/', 'http://quizly.io/', 'http://www.wikia.com/', 'http://www.nh1.com/', 'http://www.csnphilly.com/', 'http://www.careercast.com/', 'http://www.heraldtribune.com/', 'http://www.newsmax.com/', 'http://www.kansascity.com/', 'http://chachanova.com/', 'http://www.en.heroquizz.com/', 'http://www.ct-mls.com/', 'http://www.4029tv.com/', 'http://whois.easycounter.com/', 'http://naturallymoi.com/', 'http://www.travelandleisure.com/', 'http://durantdemocrat.com/', 'http://www.burlingtonfreepress.com/', 'http://wgntv.com/', 'http://www.spiritdaily.com/', 'http://www.dispatch.com/', 'http://astralandopal.com/', 'http://www.semissourian.com/', 'http://www.swinburne.edu.au/', 'http://www.soafanatic.com/', 'http://quefacilidades.com/', 'http://www.townandcountrymag.com/', 'http://www.aol.com/', 'http://www.cbsinteractive.com/', 'http://www.lawnewz.com/', 'http://www.dailypress.com/', 'https://secure.trump2016.com/', 'http://woodtv.com/', 'http://www.dodgedurango.net/', 'http://www.basketball-reference.com/', 'http://www.wetpaint.com/', 'http://www.kansas.com/', 'http://www.santabarbarapost.com/', 'http://www.washingtontimes.com/', 'http://www.journal-news.com/', 'http://www.wboc.com/', 'http://www.legendaryvideos.com/', 'http://thinkprogress.org/', 'http://www.virginiamn.com/', 'http://theviralera.com/', 'http://www.udayavani.com/', 'https://www.buzzfeed.com/', 'http://timesofindia.indiatimes.com/', 'http://www.usnews.com/', 'http://www.mytractorforum.com/', 'http://www.themarysue.com/', 'http://www.missmalini.com/', 'http://www.natchezdemocrat.com/']
print len(urllist)