import csv
from sklearn import datasets
from sklearn.linear_model import LogisticRegression
import numpy as np
import pandas as pd

# data = []
# with open('mm11.csv', 'rb') as csvfile:
#     spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for d in spamreader:
#         data.append(d)

data = pd.read_csv('mm11.csv', index_col=False, header=0)
x = data.values[:, :-1]
y = data.flag.values

# Fit Logistic Regression
lr = LogisticRegression()
lr.fit(X=x, y=y)

print lr.intercept_, lr.coef_

# columns = data.pop(0)
# df = pd.DataFrame(data=data, columns=columns)

# # Fit Logistic Regression
# lr = LogisticRegression()
# lr.fit(X=df[['Age']], y=df['ZepplinFan'])

# # View the coefficients
# lr.intercept_
# lr.coef_
