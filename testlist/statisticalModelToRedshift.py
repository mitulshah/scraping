import xlrd
import psycopg2
import datetime
from pytz import timezone

utc_time = datetime.datetime.now(timezone('UTC'))


def read_xlsfile(filename):
    workbook = xlrd.open_workbook(filename)
    worksheet = workbook.sheet_by_index(1)

    # Set to 0 if you want to include the header data.
    offset = 3  # To ingor first table (3 rows and header of second table)

    rows = []
    for i, row in enumerate(range(worksheet.nrows)):
        if i <= offset:  # (Optionally) skip headers
            continue
        r = []
        for j, col in enumerate(range(worksheet.ncols)):
            if j < 5:
                d = worksheet.cell_value(i, j)
                if (d == u'NA'):
                    r.append(0)
                else:
                    r.append(d)
            else:
                break
        if r[1] or r[1] == 0:
            r.append(utc_time)
            # r.append('2016-05-12 00:00:00')
            rows.append(tuple(r))
    return rows


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """
    def func_wrapper(*args, **kwargs):
        global cur
        # Define our connection string
        conn_string = "host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com'  \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        conn = psycopg2.connect(conn_string)
        conn.autocommit = True
        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        cur = conn.cursor()

        func(*args, **kwargs)

        # conn.commit()
        # close db connection
        conn.close()

    return func_wrapper


@redshift_dbconn
def insertDataToRedShift():

    filedata = read_xlsfile('/var/www/testlist/CPC2_estimates.xlsx')
    # print filedata
    for data in filedata:
        try:
            cur.execute(
                "INSERT INTO atomic.magnify_model (coefficient, \
			    estimate, std_error, zvalue, pr_mod_z, created_at \
			    ) values ( %s, %s, %s, %s, %s, %s)", data)
        except Exception as e:
            print e

insertDataToRedShift()
