import cookielib
import os
import urllib
import urllib2
import re
import string
from bs4 import BeautifulSoup
import time
import requests
from credential import username, password
import json
import operator
import csv
from socket import timeout
from furl import furl

cookie_filename = "taboola.cookies.txt"


class TaboolaParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        proxy = urllib2.ProxyHandler({'http': '178.62.52.60:8118'})
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            proxy
        )
        self.opener.addheaders = [
            ('User-agent',
             ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'))
        ]

        # Login
        x = self.loginPage()
        if x:
            self.loadData()

        self.cj.save()

    def generateSession(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            f_url = response.url
            f = furl(f_url)
            return True
        except Exception as e:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            # return self.generateSession(url, data)
            raise

    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            # return ''.join(response.readlines())
            d = response.read()
            return d
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            return self.loadPage(url, data)

    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        html = self.loadPage("https://backstage.taboola.com/backstage/login")
        soup = BeautifulSoup(html, 'html.parser')
        serverTime = soup.find('input', {'name': 'serverTime'})['value']
        csrf = soup.find('input', {'name': '_csrf'})['value']
        redir = soup.find('input', {'name': 'redir'})['value']
        sig = soup.find('input', {'name': 'sig'})['value']

        login_data = urllib.urlencode({
            'j_username': self.login,
            'j_password': self.password,
            'serverTime': serverTime,
            '_csrf': csrf,
            'redir': redir,
            'sig': sig
        })
        try:
            html = self.generateSession(
                "https://backstage.taboola.com/backstage/j_spring_security_check", login_data)
        except Exception as e:
            print(e)

        if html:
            return True
#https://backstage.taboola.com/backstage/ajax/1047353/campaigns/campaign-summary/report?format=json&id=4438w8kvovlu&groupId=campaigns&reportId=campaign-summary&dateStart=2017-02-17&dateEnd=2017-02-23&dateRangeValue=3&term=last%207%20day&currentDimension=day&queryFilter=%5B%7B%22id%22%3A%22campaign_param%22%2C%22operator%22%3A%22equal%22%2C%22value%22%3A%22-1%22%7D%5D&t=1487939929651&_=1487939928355
    def loadData(self):
        url = "https://backstage.taboola.com/"
        html = self.loadPage(url)
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table', {'class': 'zebra'})
        print table.find('tbody')

TaboolaParser(username, password)
