from bs4 import BeautifulSoup
import requests
import urllib2
import re
import MySQLdb
import ast
from itertools import cycle
import logging
from math import ceil
import time

mainlst = []
# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '50.207.44.18:3128'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

resp_data = []


def fetch_data_using_scraping(page=''):

    base_url = "https://www.credibly.com/business-loans-index/business-loans-index/industries/"
    request = urllib2.Request(base_url, headers=hdr)

    result = urllib2.urlopen(request)
    data = result.read()
    soup = BeautifulSoup(data, 'html.parser')

    for element in soup.find('div', {'id': 'geo-grid'}).find_all('a', href=True):
        itemurl = element['href']
        # itemurl = 'https://www.credibly.com/business-loans-index/business-loans-index/industries/subway-franchises/'
        irequest = urllib2.Request(itemurl, headers=hdr)

        iresult = urllib2.urlopen(irequest)
        idata = iresult.read()
        isoup = BeautifulSoup(idata, 'html.parser')
        page_title = isoup.find('div', {'class': 'page-title'}).text
        
        intro = isoup.find('div', {'class': 'city_intro'})

        facts = []
        for fact in intro.find_all('div', {'class': 'fact'}):
            facts.append(fact.find('li').text)
        score = intro.find('div', {'class': 'credit_score'}).find('span').text
        # print score, facts
        description = isoup.find('div', {'class': 'moreInfoFin'}).text
        # print description

        arr = [page_title, itemurl, score, facts, description]
        resp_data.append(arr)
        # to write in file as backup
        # with(open('lst.py', 'a')) as f:
        #     f.write(str(arr) + ', ')
        print page_title

fetch_data_using_scraping() 

# print "storing into database ...!!!!!"
# # Open database connection


db = MySQLdb.connect("localhost", "root", "root", "scrape", charset='utf8',
                     use_unicode=True)

# prepare a cursor object using cursor() method
cursor = db.cursor()

for item in resp_data:
    sql = "INSERT into credibly (title, url, score, facts, description, category) values(%s, %s, %s, %s, %s, %s)"

    cursor.execute(sql, (item[0], item[1], item[2], str(item[
                   3]), item[4], 'industry'))
db.commit()

db.close()
