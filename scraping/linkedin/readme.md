<h4>Linkedin Scraping</h4>

<ul>
	<li style="text-align: left;">Fetch all connections of top five skill of user</li>
	<ul> 
	<li>
		<b>Input</b> : 
		<ol>
		<li> Add linkedin url of any user </li>
      	<li> "clickedEntityId" (get the value from user's url)</li>
      	</ol>
    </li>
    <li>
    	<b>Output</b> : Output will be written in csv file
	</li>
	<b>Note : </b> Cookie file is created when user logged In in his account. Then we don't need to authenticate user on every request so <i> comment out the code </i>
	<b><code>self.loginPage()</code></b>
	
	</ul>
	<b>Important Note: Don't use your personal account to scrap data from Linkedin.</b>
</ul>
