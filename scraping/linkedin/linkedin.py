import cookielib
import os
import urllib
import urllib2
import re
import string
from bs4 import BeautifulSoup
import time
import requests
from credential import username, password
import json
import operator
import csv

cookie_filename = "parser.cookies.txt"


class LinkedInParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        proxy = urllib2.ProxyHandler({'http': '216.189.148.204:8080'})
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            proxy
        )
        self.opener.addheaders = [
            ('User-agent',
             ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'))
        ]

        # Login
        self.loginPage()

        self.loadData()

        self.cj.save()

    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            return ''.join(response.readlines())
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            return self.loadPage(url, data)

    def loginPage(self):
        """
        Handle login. This should populate our cookie jar.
        """
        html = self.loadPage("https://www.linkedin.com/")
        soup = BeautifulSoup(html, 'html.parser')
        csrf = soup.find(id="loginCsrfParam-login")['value']
        sourcealias = soup.find(id="sourceAlias-login")['value']

        login_data = urllib.urlencode({
            'session_key': self.login,
            'session_password': self.password,
            'loginCsrfParam': csrf,
            'sourceAlias': sourcealias
        })

        html = self.loadPage(
            "https://www.linkedin.com/uas/login-submit", login_data)

        if html:
            print "You're logged In now, Please comment out the function loginpage for next request"
        return

    def loadData(self):

        # change the below lines and add the link of specific user
        # reciept id is "clickedEntityId" (get the value from below url)
        users = [
            # ["https://www.linkedin.com/profile/view?id=AAkAAAAgYnEByPmSctHr4KdsAf34ez1TU28uE_k&authType=NAME_SEARCH&authToken=Au8Q&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A2122353%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1446551497221%2Ctas%3ACliff%20Cook",
            #     2122353],
            # ["https://www.linkedin.com/profile/view?id=AAkAAAnPoCQBYg18W8BcUKuZIuJZM8hHfRQYwQg&authType=NAME_SEARCH&authToken=gKdS&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A164601892%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1446544110799%2Ctas%3Ashazia%20manus",
            #     164601892],
            # ["https://www.linkedin.com/profile/view?id=AAkAAAB9_kQBDkwWfieCL3-ysMNm5HFpgUVUS_g&authType=NAME_SEARCH&authToken=AQYc&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A8257092%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1446544132774%2Ctas%3Asajeel",
            #     8257092],
            # ["https://www.linkedin.com/profile/view?id=AAkAAADSCLwBCvkvu234oHwAzqnlaVFOEb-OCXg&authType=NAME_SEARCH&authToken=mGDY&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A13764796%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1446544226427%2Ctas%3Aabbas%20merch", 13764796]
            ["https://www.linkedin.com/profile/view?id=AAkAAAAmYJUBMEeiht3hyiCM98pHnEyKZgvFlC8&authType=NAME_SEARCH&authToken=npRY&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A2515093%2CauthType%3ANAME_SEARCH%2Cidx%3A1-4-4%2CtarId%3A1446552759284%2Ctas%3Arahul%20", 2515093],
            ["https://www.linkedin.com/profile/view?id=AAkAAAA30a8BpiURhI3SiPinbBM-feNJojxbH3E&authType=NAME_SEARCH&authToken=OrBa&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A3658159%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1446552822077%2Ctas%3Anagendra", 3658159],
            ["https://www.linkedin.com/profile/view?id=AAkAAAAtRNsBk29xcHZM1yt9DbA9aKtzsM4nfsw&authType=NAME_SEARCH&authToken=aEEN&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A2966747%2CauthType%3ANAME_SEARCH%2Cidx%3A1-2-2%2CtarId%3A1446552875574%2Ctas%3Akaran%20b", 2966747]
            ]
        addeduser = []
        for user in users:
            page = user[0]
            receiptID = user[1]

            html = self.loadPage(page)
            soup = BeautifulSoup(html, 'html.parser')
            skill_li = soup.find(
                'ul', {'class': 'skills-section'}).find_all('li')

            for li_ele in skill_li:
                li = li_ele.find('span', {'class': 'skill-pill'})
                if li:
                    skill = li.find(
                        'span', {'class': 'endorse-item-name'}).text
                    print skill
                    userurl = 'https://www.linkedin.com/profile/endorser-info-dialog'
                    payload = {
                        'skillName': skill,
                        'recipientId': receiptID,
                    }
                    data = urllib.urlencode(payload)
                    r = self.opener.open(userurl, data)
                    response = json.loads(r.read())
                    endorserslst = response['content'][
                        'endorser_info_dialog']['endorsers']
                    endorsed_for = response['content'][
                        'endorser_info_dialog']['recipientFirstName'] + ' ' + \
                        response['content']['endorser_info_dialog'][
                            'recipientLastName']
                    skillName = response['content']['endorser_info_dialog'][
                            'skillName']
                    vals = []
                    for ele in endorserslst:
                        uprofile = ele['profileURL'].encode('utf-8')
                        if any(l.lower() in ['bank', 'credit', 'union']
                               for l in ele['headline'].split()) and \
                                uprofile not in addeduser:
                        # if uprofile not in addeduser:
                            addeduser.append(uprofile)
                            val = [ele['fullName'].encode('utf-8'),
                                   ele['headline'].encode('utf-8'),
                                   ele['locationString'].encode('utf-8'),
                                   skillName, endorsed_for,
                                   uprofile]
                            vals.append(val)

                    with open('linkedindata.csv', 'a') as f:
# writer = csv.writer(f, quoting=csv.QUOTE_ALL)

                        w = csv.writer(f)
                        w.writerows(vals)

        return

parser = LinkedInParser(username, password)
