from bs4 import BeautifulSoup
import requests
import re
import MySQLdb

mainlst = []


def fetch_data_using_scraping(page='?searchId=1443099754&action=search&page=1'):

    base_url = "https://aghires.com/browse-by-company/%s" % (page)
    result = requests.get(base_url)

    data = result.text
    soup = BeautifulSoup(data, 'html.parser')

    for element in soup.find('tbody').find_all('tr'):
        all_td = element.find_all('td')
        td_html = all_td[1].find('a', href=True)
        location = all_td[2].text
        print "##### start scraping for {} #####".format(td_html.text)

        r_link = requests.get(td_html['href'])
        if r_link.status_code == 200:
            r = r_link.text

            soup_internal = BeautifulSoup(r, 'html.parser')

            company_info = soup_internal.find(
                'div', {'class': 'compProfileInfo'})

            findphone = company_info.find('strong', text=re.compile('Phone'))
            extract_phone = findphone.nextSibling.split()

            # fetch phone number
            try:
                phone = extract_phone[1]
            except:
                phone = ''
            # fetch website if exists
            try:
                website = company_info.find('a').get('href')
            except:
                website = ''

            # fetch website logo, if exists
            try:
                websiteLogo = company_info.find('img').get('src')
            except:
                websiteLogo = ''

            address = company_info.text.split('Phone')[0]

            company_detail = soup_internal.find(
                'div', {'class': 'listingInfo'}).text

            sublst = [td_html.text, location, websiteLogo,
                      address, phone, website, company_detail]

            # print "Congratulation..!! {} is scrapped :)".format(td_html.text)

            mainlst.append(sublst)
            # print mainlst

    # call the function again if next page is exist.
    nextPage = soup.find('span', {'class': 'nextBtn'}).find('a').get('href')
    if nextPage:
        fetch_data_using_scraping(nextPage)

fetch_data_using_scraping()
print mainlst
with(open('lst.py', 'w')) as f:
    f.write(str(mainlst))

print "storing into database ...!!!!!"
# Open database connection
db = MySQLdb.connect("localhost", "root", "root", "scrape")

# prepare a cursor object using cursor() method
cursor = db.cursor()

for item in mainlst:
    sql = "INSERT into aghires (name, location, logo, address, phone, website, company_detail) values(%s, %s, %s, %s, %s, %s, %s)"

    cursor.execute(sql, (item[0].encode('utf-8'), item[1].encode('utf-8'), item[2].encode('utf-8'), item[
                   3].encode('utf-8'), item[4].encode('utf-8'), item[5].encode('utf-8'), item[6].encode('utf-8')))
    db.commit()

# disconnec.encode('utf-8')t from server
db.close()
