from config import SQLAlchemy
from datetime import datetime
from uuid import uuid4
db = SQLAlchemy()


class Campaigns(db.Model):

    """
    Model to campaigns list in db
    """
    __tablename__ = 'campaigns'

    campaignid = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer,
                       db.ForeignKey('users.id'),
                       nullable=False)
    name = db.Column(db.String(255))
    startdate = db.Column(db.DateTime)
    enddate = db.Column(db.DateTime)
    sumclicks = db.Column(db.Integer)
    isactive = db.Column(db.String(20))
    insertdate = db.Column(db.DateTime)
    isdeleted = db.Column(db.Integer)
    deletedate = db.Column(db.DateTime)

    def __init__(self, userid, name,
                 startdate, enddate, sumclicks, isactive, insertdate,
                 isdeleted, deletedate):
        # self.campaignid = campaignid
        self.userid = userid
        self.name = name
        self.startdate = startdate
        self.enddate = enddate
        self.sumclicks = 0
        self.isactive = isactive
        self.insertdate = datetime.utcnow()
        self.isdeleted = 0
        self.deletedate = datetime.utcnow()

    def __repr__(self):
        return '{} {}'.format(self.campaignid, self.name)


class User(db.Model):

    """
    Model refere to users table
    """
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    token = db.Column(db.String(255))
    token_expiry = db.Column(db.DateTime)
    isdeleted = db.Column(db.Integer)

    def __init__(self, username, token, token_expiry,
                 isdeleted):
        self.username = username
        self.token = token
        self.token_expiry = token_expiry
        self.isdeleted = 0

    def __repr__(self):
        pass
