from datetime import datetime, timedelta
from uuid import uuid4
from copy import deepcopy

from flask.ext.restplus import Api, Resource, fields
from stormpath.error import Error as StormpathError
# from flask_restful import reqparse
from werkzeug.contrib.fixers import ProxyFix

# import local module
from models import *
from config import db, app, Flask, client

app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='Backend API',
          description='Flask framework with CRUD Operation',
          )

application = client.applications.get(
    'https://api.stormpath.com/v1/applications/4XoOXB03UNt2SAgLN1bLUh')
directory = client.directories.get(
    'https://api.stormpath.com/v1/directories/4XooTUl9bqiIJJD9aoQLoB')


ns_auth = api.namespace('auth', description='Authenticate \
    yourself to access API')

ns_compaigns = api.namespace(
    'compaign', description='Compaign Operations')

ns_compaign = api.namespace(
    'compaign', description='Compaign Operations')


def abort_if_doesnt_exist(ele):
    api.abort(404, "{} doesn't exist".format(ele))


parser = api.parser()
parser.add_argument(
    'username', type=str, required=True, help='Username or email of stormpath',
    location='form')
parser.add_argument(
    'password', type=str, required=True, help='Password for stormpath account',
    location='form')

parser_post = api.parser()
parser_post.add_argument(
    'name', type=str, required=False, help='Name of Compaign',
    location='form')
parser_post.add_argument(
    'startdate', type=str, required=False, help='startdate of compaign, \
    format: yyyy-mm-dd hh:mm',
    location='form')
parser_post.add_argument(
    'enddate', type=str, required=False, help='enddate of compaign,\
    format: yyyy-mm-dd hh:mm',
    location='form')
parser_post.add_argument(
    'isactive', choices=['y', 'n', 'term'], required=False,
    help='yes=>y, no=>n, terminate=>term',
    location='form', default='y')

parser_full = parser_post.copy()
parser_full.add_argument(
    'username', type=str, required=True, help='Username or Email of stormpath',
    location='form')
parser_full.add_argument(
    'token', type=str, required=True, help='Token generated using authentication',
    location='form')


@ns_auth.route('/')
class Authenticate(Resource):

    """ authenticate the user by email or username and password"""
    @api.doc(parser=parser)
    def post(self):
        """ Authenticate User by Stormpath Account """
        args = parser.parse_args()
        username = args['username']
        password = args['password']

        try:
            auth_attempt = application.authenticate_account(username, password)
        except StormpathError, e:
            return {'message': e.message, 'status': False}
        except Exception, e:
            return {'message': e.message, 'status': False}

        token = str(uuid4())

        query = db.session.query(User).filter_by(
            username=username, isdeleted=0).first()
        expirytime = (datetime.utcnow() + timedelta(minutes=15)
                      ).strftime("%Y-%m-%d %H:%M:%S")
        if not query:
            record = User(username, token, expirytime, 0)
            db.session.add(record)
        else:
            query.token = token
            query.token_expiry = expirytime

        try:
            db.session.commit()
        except Exception as e:
            return {'message': e.message, 'status': False}

        return {'status': True, 'token': token,
                'expiry': expirytime}, 200


@ns_compaigns.route('/')
class CompaignsList(Resource):

    """fetch or insert compaigns"""
    @api.doc(parser=parser_full)
    def post(self):
        """
        create new compaign
        """
        args = parser_full.parse_args()
        print args
        if not args or not 'username' in args or \
                not 'token' in args or not args['username'] or \
                not args['token']:
            abort_if_doesnt_exist('token')

        startdate = enddate = isactive = name = ''
        username = args['username']
        token = args['token']
        currenttime = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        query = db.session.query(User).filter_by(
            username=username, isdeleted=0).first()
        if not query:
            return {'message': "invalid username", 'status': False}

        if query.token == token and query.token_expiry > datetime.utcnow():
            userid = query.id
        else:
            return {'message': "token is expired", 'status': False}

        if 'name' in args:
            name = args['name'] or ''

        if 'startdate' in args and args['startdate'] and \
                'enddate' in args and args['enddate']:
            # startdate and enddate is optional
            startdate = validate(args['startdate'])
            enddate = validate(args['enddate'])
            if startdate and enddate:
                if startdate > enddate:
                    return {'error': 'enddate should be greater than startdate',
                            'status': False}
            else:
                return {'error': "Incorrect datetime format,\
                    should be YYYY-MM-DD HH:MM", 'status': False}

        if 'isactive' in args:
            isactive = args['isactive']

        record = Campaigns(
            userid, name, startdate, enddate, 0, isactive,
            currenttime, 0, currenttime)
        try:
            db.session.add(record)
            db.session.commit()
            insertedid = record.campaignid
        except Exception as e:
            return {'error': e.message, 'status': False}, 400

        expirytime = (
            datetime.utcnow() + timedelta(minutes=15)
        ).strftime("%Y-%m-%d %H:%M:%S")
        query.token_expiry = expirytime
        try:
            db.session.commit()
        except Exception as e:
            return {'message': e.message, 'status': False}

        return {'status': True, 'message': 'campaign is created',
                'id': insertedid}, 201

    def get(self):
        """
        fetch all active records
        """
        query = db.session.query(Campaigns).filter(
            Campaigns.isactive == 'y',
            Campaigns.isdeleted == 0).all()

        return create_json(query)


@ns_compaign.route('/<string:id>/<string:token>')
@api.doc(responses={404: 'compaign not found'},
         params={'id': 'Compaign ID', 'token': 'Token generated \
         by Authentication'})
class Compaign(Resource):

    @api.doc(responses={204: 'Compaign deleted'})
    def delete(self, id, token):
        '''Delete a given compaign'''
        if not id or not token:
            abort_if_doesnt_exist('ID or token')

        query = db.session.query(Campaigns).filter_by(
            campaignid=id, isdeleted=0).first()
        if not query:
            abort_if_doesnt_exist('compaign id {}'.format(id))

        usrquery = db.session.query(User).filter_by(
            id=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            abort_if_doesnt_exist('token')

        if usrquery.token_expiry > datetime.utcnow():
            try:
                query.isdeleted = 1
                query.deletedate = datetime.utcnow().strftime(
                    "%Y-%m-%d %H:%M:%S")
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': False}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': False}
        else:
            return {'message': "token is expired", 'status': False}

        return {'status': True}, 204

    @api.doc(parser=parser_post)
    def put(self, id, token):
        '''Update a given compaign'''

        if not id or not token:
            abort_if_doesnt_exist('ID or token')

        args = parser_post.parse_args()

        query = db.session.query(Campaigns).filter_by(
            campaignid=id, isdeleted=0).first()
        if not query:
            abort_if_doesnt_exist('compaign id {}'.format(id))

        usrquery = db.session.query(User).filter_by(
            id=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            abort_if_doesnt_exist('token')

        if usrquery.token_expiry > datetime.utcnow():

            if 'name' in args:
                query.name = args['name']

            if 'startdate' in args and args['startdate'] and \
                    'enddate' in args and args['enddate']:
                        # startdate and enddate is optional
                startdate = validate(args['startdate'])
                enddate = validate(args['enddate'])
                if startdate and enddate:
                    if startdate > enddate:
                        return {
                            'error': 'enddate should be greater than startdate',
                            'status': False}
                    else:
                        query.startdate = args['startdate']
                        query.enddate = args['enddate']
                else:
                    return {'error': "Incorrect datetime format,\
                        should be YYYY-MM-DD HH:MM", "status": False}

            query.isactive = args['isactive']
            query.insertdate = datetime.utcnow().strftime(
                "%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': True}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': True}
        else:
            return {'message': "token is expired", 'status': False}

        return {'status': True}, 200


def validate(date_text):
    try:
        return datetime.strptime(date_text, '%Y-%m-%d %H:%M')
    except Exception as e:
        return False


def create_json(query):
    """
    function to used to generate json of
    fetched data from database
    """
    if not len(query):
        return {'message': "No records are found.", 'status': False}
    result = []
    for item in query:
        result.append({'id': item.campaignid,
                       'name': item.name,
                       'userid': item.userid,
                       'startdate': str(item.startdate),
                       'enddate': str(item.enddate),
                       'sumclick': item.sumclicks,
                       'isactive': item.isactive})
    return {'compaigns': result, "status": True}

if __name__ == '__main__':
    app.run(debug=True)
