<h2>SaaS platform that combines many of the things</h2>
send request using curl
example :

<h3> 1. To fetch all compaigns using GET method </h3>

  <code> curl -i  localhost:5000/compaign </code>

<h3>2. To add new compaign using POST Method</h3>
  
   <code>curl -i -H "Content-Type: application/json" -X POST -d '{"username":"mitulshah", "startdate":"2014-10-12 01:50", "enddate":"2015-10-05 12:10", "name":"Agri Bussiness23", "isactive":"y", "token":"4b7825fe-6e51-4465-ac3b-a98830942777"}' http://localhost:5000/compaign</code>


<h3>3. To update compaign using PUT</h3>

  <code> curl -i -H "Content-Type: application/json" -X PUT -d '{"startdate":"2014-10-12 01:50", "enddate":"2015-10-05 12:10", "name":"Agri Bussiness123", "isactive":"y"}' http://localhost:5000/compaign/10003/4b7825fe-6e51-4465-ac3b-a98830942777</code>

<h3>4. To delete comapign using DELETE</h3>

   <code>curl -i -H "Content-Type: application/json" -X DELETE  http://localhost:5000/compaign/10003/4b7825fe-6e51-4465-ac3b-a98830942777</code>

<h3>5. For authentication and update token</h3>
   
   <code>curl -i -H "Content-Type: application/json" -X POST -d '{"username":"mitulshah","password":"Ab123456"}' http://localhost:5000/auth</code>


