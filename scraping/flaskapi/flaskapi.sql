-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 27, 2015 at 09:24 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flaskapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `campaignid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `sumclicks` int(11) NOT NULL,
  `isactive` varchar(20) NOT NULL COMMENT 'active, not active, terminate',
  `insertdate` datetime NOT NULL,
  `isdeleted` int(2) NOT NULL,
  `deletedate` datetime NOT NULL,
  PRIMARY KEY (`campaignid`),
  KEY `userid` (`userid`),
  KEY `userid_2` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10007 ;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`campaignid`, `userid`, `name`, `startdate`, `enddate`, `sumclicks`, `isactive`, `insertdate`, `isdeleted`, `deletedate`) VALUES
(10001, 1, 'Agribusiness outbound marketing', '2015-10-05 12:33:19', '2015-10-21 09:40:16', 0, 'y', '2015-09-28 00:00:00', 0, '0000-00-00 00:00:00'),
(10002, 1, 'Affiliate agribusiness', '2015-09-05 12:33:19', '2015-09-21 09:40:16', 0, 'term', '2015-08-28 00:00:00', 0, '0000-00-00 00:00:00'),
(10003, 1, 'Agri Bussineswws123', '2014-10-12 01:50:00', '2015-10-05 12:10:00', 0, 'y', '2015-10-27 15:43:13', 0, '2015-10-27 13:59:15'),
(10004, 1, 'Agriculter farm', '2014-10-12 01:50:00', '2015-10-05 12:10:00', 0, 'n', '2015-10-22 07:42:54', 0, '0000-00-00 00:00:00'),
(10005, 1, 'Agri Bussiness23', '2014-10-12 01:50:00', '2015-10-05 12:10:00', 0, 'y', '2015-10-27 10:49:56', 0, '2015-10-27 10:49:56'),
(10006, 1, 'Agri Bussiness23', '2014-10-12 01:50:00', '2015-10-05 12:10:00', 0, 'y', '2015-10-27 11:17:59', 0, '2015-10-27 11:17:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expiry` datetime NOT NULL,
  `isdeleted` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `token`, `token_expiry`, `isdeleted`) VALUES
(1, 'mitulshah', '4b7825fe-6e51-4465-ac3b-a98830942777', '2015-10-27 17:58:13', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD CONSTRAINT `campaigns_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
