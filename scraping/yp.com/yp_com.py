from bs4 import BeautifulSoup
import requests
import urllib2
import re
import MySQLdb
import ast
from itertools import cycle
import logging
from math import ceil
import time

states = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']

# for logging
LOG_FILENAME = 'yallowpages.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)

# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '50.207.44.18:3128'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)


def yellowpages_scraping(state, currentpage, total_pages=1,
                         totalresult=0, retry=0, page_not_found=0):
    if currentpage > 101:
        return False

    mainlst = []
    base_url = "http://www.yellowpages.com/search?search_terms=agriculture&geo_location_terms=%s&s=name&page=%s" % (
        state, str(currentpage))

    # Aquire data from URL
    request = urllib2.Request(base_url, headers=hdr)
    status = False
    if page_not_found < 10:
        page_not_found += 1
        try:
            result = urllib2.urlopen(request)
            status = True
        except urllib2.HTTPError, err:
            print err.code
            print "Trying again"
            yellowpages_scraping(
                state, currentpage, total_pages, totalresult, 0, page_not_found)

        except Exception as e:
            print "error :: is {}".format(str(e))
            yellowpages_scraping(
                state, currentpage, total_pages, totalresult, 0, page_not_found)
    else:
        print "Sorry!! {} is not responding.. Request for page {}..!!!".format(state, currentpage + 1)
        yellowpages_scraping(
            state, currentpage+1, total_pages, totalresult, 0, 0)
    if status and result.getcode() == 200:
        print state + " :: started on " + time.ctime() + " page :: " + str(currentpage)
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')

        if total_pages == 1:
            # fetch the total result when any new state is comming
            pagetext = soup.find('div', {'class': 'pagination'}).find('p').text
            ofIndex = pagetext.index('of ') + len('of ')
            resultIndex = pagetext.index('results')
            totalresult = pagetext[ofIndex:resultIndex]
            total_pages = int(ceil(int(totalresult)/30))

        blocks = soup.find_all('div', {'class': 'info'})
        if not len(blocks):
            if retry < 3:
                print "Alert -- Trying {} time to get result from {} of {}".format(
                    retry, currentpage, state)
                retry += 1
                yellowpages_scraping(
                    state, currentpage, total_pages, totalresult, retry)
            else:
                print "Warning !!! Page {} is skipped for {}".format(currentpage, state)
                logging.debug(
                    {'error': 'Page is skipped due to lack of result',
                     'state': state, 'page': currentpage})
                yellowpages_scraping(
                    state, currentpage+1, total_pages, totalresult, 0)

        for element in blocks:
            # fetch the number of record
            numberString = element.find('h3', {'class': 'n'}).text
            number = numberString.split('.')[0]

            # fetch the link for inner page
            anchortag = element.find('a', href=True)
            innerpagelink = 'http://www.yellowpages.com' + anchortag['href']
            try:
                mainlst.append(fetch_innerpage_content(innerpagelink, state))
            except Exception as e:
                print e.message
                mainlst.append(fetch_innerpage_content(innerpagelink, state))

            # print "{} -- {} out of {} records are fetched.".format(
            #     state, number, totalresult)

        # start storing the data for every single pages
        db = MySQLdb.connect(
            "localhost", "root", "root", "scrape", charset='utf8')
        # prepare a cursor object using cursor() method
        cursor = db.cursor()

        for item in mainlst:
            try:
                cursor.execute("""INSERT into yp_com (business_name, state, latitude, longitude, street_address, city_state, phone, website, email, years_in_business, datetime, description, servicesOrProducts, associations, aka, brands, payment, categories, url) values (%s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
                    str(item[0]), str(item[1]), item[2], item[3], str(item[4]), str(item[5]), str(item[6]), str(item[7]), str(item[8]), str(item[9]),  str(item[10]), str(item[11]), str(item[12]), str(item[13]), str(item[14]), str(item[15]), str(item[16]), str(item[17]), str(item[18])))
                db.commit()
            except Exception as e:
                logging.debug(str({'error': e.message, 'url': item[18]}))
        # disconnec.encode('utf-8')t from server
        db.close()
        print "{}, {} pages are stored in db..!!!".format(state, currentpage)

        # call the function again if next page is exist.
        if total_pages > currentpage:
            yellowpages_scraping(state, int(currentpage)+1, total_pages,
                                 totalresult, 0)


def fetch_innerpage_content(innerpagelink, state):

    innerRequest = urllib2.Request(innerpagelink, headers=hdr)
    try:
        innerResult = urllib2.urlopen(innerRequest)

        if innerResult.getcode() == 200:
            innerData = innerResult.read()
            innerSoup = BeautifulSoup(innerData, 'html.parser')
            # print "{} -- {}".format(number, state)
            # number += 1
            first_paragraph = innerSoup.find(
                'div', {'class', 'business-card'})

            try:
                business_name = first_paragraph.find('h1').text
            except:
                business_name = ''

            try:
                latlong = first_paragraph['data-pushpin']
                latitude = ast.literal_eval(latlong)['lat']
                longitude = ast.literal_eval(latlong)['lon']
            except:
                latitude = ''
                longitude = ''

            try:
                street_address = first_paragraph.find(
                    'p', {'class', 'street-address'}).text
            except:
                street_address = ''

            try:
                city_state = first_paragraph.find(
                    'p', {'class', 'city-state'}).text
            except:
                city_state = ''

            try:
                phone = first_paragraph.find(
                    'p', {'class', 'phone'}).text
            except:
                phone = ''

            try:
                websiteTag = first_paragraph.find(
                    'a', {'class': 'custom-link'}, href=True)
                website = websiteTag['href']
            except:
                website = ''

            try:
                emailTag = first_paragraph.find(
                    'a', {'class': 'email-business'}, href=True)
                email = emailTag['href'].split('mailto:')[1]
            except:
                email = ''

            try:
                years_in_business = ''
                for yrs_in_business in innerSoup.find_all('section', {'class': 'years-in-business'}):
                    years_in_business = yrs_in_business.find(
                        'p', {'class': 'count'}).text
            except:
                years_in_business = ''

            second_paragraph = innerSoup.find('dl')
            # print second_paragraph.parent.name
            if second_paragraph.parent.name == 'section':
                datetime = []
                try:
                    for time in second_paragraph.find_all('time'):
                        datetime.append(time['datetime'])
                except:
                    datetime = []

                try:
                    description = second_paragraph.find(
                        'dd', {'class': 'description'}).text
                except:
                    description = ''

                try:
                    service = second_paragraph.find(
                        'dt', text=re.compile('Services/Products:'))
                    servicesOrProducts = service.nextSibling.string
                except:
                    servicesOrProducts = ''

                try:
                    associations = second_paragraph.find(
                        'dd', {'class': 'associations'}).text
                except:
                    associations = ''

                try:
                    aka = second_paragraph.find(
                        'dd', {'class': 'aka'}).text
                except:
                    aka = ''

                try:
                    brands = second_paragraph.find(
                        'dd', {'class': 'brands'}).text
                except:
                    brands = ''

                try:
                    payment = second_paragraph.find(
                        'dd', {'class': 'payment'}).text
                except:
                    payment = ''

                try:
                    categoriesTag = second_paragraph.find(
                        'dd', {'class': 'categories'}).find_all('a')
                except:
                    categoriesTag = []

                categories = []
                for category in categoriesTag:
                    categories.append(category.text)

                return [business_name, state, latitude, longitude,
                        street_address, city_state, phone, website, email,
                        years_in_business, datetime, description,
                        servicesOrProducts, associations, aka, brands,
                        payment, categories, innerpagelink]

    except urllib2.HTTPError, err:
        print err.code
        print "Trying again"

    except Exception as e:
        print e.message

for state in states:
    yellowpages_scraping(state, 1)
    print "==#== {} has been completed. ==*==".format(state)
    # break
