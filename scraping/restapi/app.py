from datetime import datetime, timedelta
from uuid import uuid4
from copy import deepcopy
import ast

from flask.ext.restplus import Api, Resource, fields
from stormpath.error import Error as StormpathError
from flask_restful import reqparse
from werkzeug.contrib.fixers import ProxyFix
from sqlalchemy.sql import func
# import local module
from models import *
from config import db, app, Flask, client

app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='Bank/Institute Advertise API',
          description='campaigns/Ads CRUD Operation',
          )

application = client.applications.get(
    'https://api.stormpath.com/v1/applications/4XoOXB03UNt2SAgLN1bLUh')
directory = client.directories.get(
    'https://api.stormpath.com/v1/directories/4XooTUl9bqiIJJD9aoQLoB')


ns_auth = api.namespace('auth', description='Authenticate \
    yourself to access API')

ns_ads = api.namespace(
    'ads', description='Advertisement Operations')

ns_ad = api.namespace(
    'ads', description='Advertisement Operations')

ns_ad_by_campaign = api.namespace(
    'campaign', description='Campaign Operations')
ns_ad_by_campaigns = api.namespace(
    'campaign', description='Campaign Operations')


def abort_if_doesnt_exist(ele):
    api.abort(404, "{} doesn't exist".format(ele))


parser = reqparse.RequestParser()
parser.add_argument(
    'username', type=str, required=True, help='Username or email of stormpath',
    location='form')
parser.add_argument(
    'password', type=str, required=True, help='Password for stormpath account',
    location='form')

parser_post = api.parser()
parser_post.add_argument(
    'name', type=str, required=False, help='Name of Advertise',
    location='form')
parser_post.add_argument(
    'campaignid', type=str, required=True,
    help='List of campaign id (e.g. [10001,10005])',
    location='form')

parser_full = parser_post.copy()
parser_full.add_argument(
    'userid', type=int, required=True, help='UserID',
    location='form')
parser_full.add_argument(
    'token', type=str, required=True, help='Token generated using \
    authentication',
    location='form')

campaignpost = api.parser()
campaignpost.add_argument(
    'name', type=str, required=False, help='Name of campaign',
    location='form')
campaignpost.add_argument(
    'startdate', type=str, required=False, help='startdate of campaign, \
    format: yyyy-mm-dd hh:mm',
    location='form')
campaignpost.add_argument(
    'enddate', type=str, required=False, help='enddate of campaign,\
    format: yyyy-mm-dd hh:mm',
    location='form')
campaignpost.add_argument(
    'isactive', choices=['y', 'n', 'term'], required=False,
    help='yes=>y, no=>n, terminate=>term',
    location='form', default='y')
campaignpost.add_argument(
    'userid', type=int, required=True, help='UserID',
    location='form')
campaignpost.add_argument(
    'token', type=str, required=True, help='Token generated using \
    authentication',
    location='form')


@ns_auth.route('/')
class Authenticate(Resource):

    """ authenticate the user by email or username and password"""
    @api.doc(parser=parser)
    def post(self):
        """ Authenticate User by Stormpath Account """
        args = parser.parse_args()
        username = args['username']
        password = args['password']

        try:
            auth_attempt = application.authenticate_account(username, password)
        except StormpathError, e:
            return {'message': e.message, 'status': False}
        except Exception, e:
            return {'message': e.message, 'status': False}

        token = str(uuid4())

        query = db.session.query(User).filter_by(
            username=username, isdeleted=0).first()
        expirytime = (datetime.utcnow() + timedelta(minutes=15)
                      ).strftime("%Y-%m-%d %H:%M:%S")
        if not query:
            record = User(username, token, expirytime, 0)
            db.session.add(record)
        else:
            query.token = token
            query.token_expiry = expirytime

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return {'message': e.message, 'status': False}

        return {'status': True, 'token': token,
                'expiry': expirytime}, 200


@ns_ads.route('/')
class AdsList(Resource):

    """fetch or insert campaigns"""
    @api.doc(parser=parser_full)
    def post(self):
        """
        create new advertise
        """
        args = parser_full.parse_args()
        if not args or not 'userid' in args or \
                not 'token' in args or not args['userid'] or \
                not args['token']:
            abort_if_doesnt_exist('token')

        if 'campaignid' not in args or not args['campaignid']:
            abort_if_doesnt_exist('Campaign ID')
        else:
            campaignid = ast.literal_eval(args['campaignid'])
            # campaign = db.session.query(Campaign).filter_by(
            #     campaignid=campaignid,
            #     isdeleted=0).all()

            # if len(campaign) != 1:
            #     abort_if_doesnt_exist('Campaign')

        startdate = enddate = isactive = name = ''        
        userid = args['userid']
        token = args['token']
        currenttime = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        query = db.session.query(User).filter_by(
            userid=userid, isdeleted=0).first()
        if not query:
            return {'message': "invalid user", 'status': False}

        if query.token == token and query.token_expiry > datetime.utcnow():
            userid = query.userid
        else:
            return {'message': "token is expired", 'status': False}

        if 'name' in args:
            name = args['name'] or ''

        # if 'startdate' in args and args['startdate'] and \
        #         'enddate' in args and args['enddate']:
        #     # startdate and enddate is optional
        #     startdate = validate(args['startdate'])
        #     enddate = validate(args['enddate'])
        #     if startdate and enddate:
        #         if startdate > enddate:
        #             return {'error': 'enddate should be greater than startdate',
        #                     'status': False}
        #     else:
        #         return {'error': "Incorrect datetime format,\
        #             should be YYYY-MM-DD HH:MM", 'status': False}

        # if 'isactive' in args:
        #     isactive = args['isactive']

        record = Advertise(name, userid, 0)
        try:
            db.session.add(record)
            db.session.commit()
            insertedid = record.advertiseid
        except Exception as e:
            db.session.rollback()
            return {'error': e.message, 'status': False}, 400

        for camp in campaignid:
            try:
                addwithcampaign = AdsCampaign(insertedid, camp, 0)
                db.session.add(addwithcampaign)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                print e.message

        expirytime = (
            datetime.utcnow() + timedelta(minutes=15)
        ).strftime("%Y-%m-%d %H:%M:%S")
        query.token_expiry = expirytime

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return {'message': e.message, 'status': False}

        return {'status': True, 'message': 'Advertise is created',
                'id': insertedid}, 201

    def get(self):
        """
        fetch all active records
        """
        query = db.session.query(Advertise).join(
            AdsCampaign).join(
            Campaign).add_columns(
            Advertise.advertiseid, Advertise.userid,
            Advertise.advertisename, 
            func.group_concat(Campaign.campaignid)
            ).filter(
            Campaign.isactive == 'y',
            Advertise.isdeleted == 0,
            AdsCampaign.isdeleted == 0,
            Campaign.isdeleted == 0).group_by(
            Advertise.advertiseid).order_by(
            Advertise.advertiseid.desc())
        res = query.all()
        # print query
        if not len(res):
            return {'message': "No records are found.", 'status': False}
        result = []
        for item in res:
            result.append({'advertiseid': item[1],
                           'advertisename': str(item[0]),
                           'userid': item[2],
                           'campaignid': map(int, item[4].split(','))
                           })

        return {'advertise': result, "status": True}


@ns_ad.route('/<string:id>/<string:token>')
@api.doc(responses={404: 'Advertise not found'},
         params={'id': 'Advertise ID', 'token': 'Token generated \
         by authentication'})
class AdsOperation(Resource):

    @api.doc(responses={204: 'advertise deleted'})
    def delete(self, id, token):
        '''Delete a given advertise'''
        if not id or not token:
            abort_if_doesnt_exist('ID or token')

        query = db.session.query(Advertise).filter_by(
            advertiseid=id, isdeleted=0).first()
        if not query:
            abort_if_doesnt_exist('advertise id:{}'.format(id))

        usrquery = db.session.query(User).filter_by(
            userid=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            abort_if_doesnt_exist('token')

        if usrquery.token_expiry > datetime.utcnow():
            try:
                query.isdeleted = 1
                query.deletedate = datetime.utcnow().strftime(
                    "%Y-%m-%d %H:%M:%S")
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return {'message': e.message, 'status': False}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return {'message': e.message, 'status': False}
        else:
            return {'message': "token is expired", 'status': False}

        return {'status': True}, 204

    @api.doc(parser=parser_post)
    def put(self, id, token):
        '''Update a given advertise'''

        if not id or not token:
            abort_if_doesnt_exist('ID or token')

        args = parser_post.parse_args()

        if 'campaignid' not in args or not args['campaignid']:
            abort_if_doesnt_exist('Campaign ID')
        else:
            campaignid = ast.literal_eval(args['campaignid'])

        query = db.session.query(Advertise).filter_by(
            advertiseid=id, isdeleted=0).first()
        if not query:
            abort_if_doesnt_exist('Advertise id:{}'.format(id))

        usrquery = db.session.query(User).filter_by(
            userid=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            abort_if_doesnt_exist('token')

        if usrquery.token_expiry > datetime.utcnow():

            if 'name' in args:
                query.advertisename = args['name']

            # delete old relation of ads and campaign
            db.session.query(AdsCampaign).filter_by(advertiseid=id).delete()
            
            for camp in campaignid:
                try:
                    addwithcampaign = AdsCampaign(id, camp, 0)
                    db.session.add(addwithcampaign)
                    db.session.commit()
                except Exception as e:
                    db.session.rollback()
                    print e.message

            query.insertdate = datetime.utcnow().strftime(
                "%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return {'message': e.message, 'status': True}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return {'message': e.message, 'status': True}
        else:
            return {'message': "token is expired", 'status': False}

        return {'status': True}, 200


@ns_ad_by_campaigns.route('/<string:id>')
@api.doc(responses={404: 'Campaign not found'},
         params={'id': 'Campaign ID'})
class AllAdsByCampaign(Resource):
    def get(self, id):
        """
        fetch advertise by campaign
        """
        query = db.session.query(Advertise).join(
            AdsCampaign).join(
            Campaign).add_columns(
            Advertise.advertiseid, Advertise.userid,
            Advertise.advertisename, Campaign.campaignid,
            Campaign.startdate, Campaign.enddate, 
            Campaign.sumclicks, Campaign.isactive, 
            Campaign.campaignname).filter(
            Campaign.campaignid == id,
            Campaign.isactive == 'y',
            Advertise.isdeleted == 0, Campaign.isdeleted==0,
            AdsCampaign.isdeleted==0).order_by(Advertise.advertiseid.desc())
        
        res = query.all()
        if not len(res):
            return {'message': "No records are found.", 'status': False}
        result = []
        for item in res:
            result.append({'advertiseid': item[1],
                           'userid': item[2],
                           'name': item[3],
                           'campaignid': item[4],
                           'startdate': str(item[5]),
                           'enddate': str(item[6]),
                           'sumclick': item[7],
                           'isactive': item[8],
                           'campaignName': item[9]
                           })
        return {'campaigns': result, "status": True}


@ns_ad_by_campaign.route('/')
class CampaignList(Resource):
    @api.doc(parser=campaignpost)
    def post(self):
        """
        create new advertise
        """
        args = campaignpost.parse_args()
        if not args or not 'userid' in args or \
                not 'token' in args or not args['userid'] or \
                not args['token']:
            abort_if_doesnt_exist('token')

        startdate = enddate = isactive = name = ''        
        userid = args['userid']
        token = args['token']
        currenttime = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        query = db.session.query(User).filter_by(
            userid=userid, isdeleted=0).first()
        if not query:
            return {'message': "invalid user", 'status': False}

        if query.token == token and query.token_expiry > datetime.utcnow():
            userid = query.userid
        else:
            return {'message': "token is expired", 'status': False}

        if 'name' in args:
            name = args['name'] or ''

        if 'startdate' in args and args['startdate'] and \
                'enddate' in args and args['enddate']:
            # startdate and enddate is optional
            startdate = validate(args['startdate'])
            enddate = validate(args['enddate'])
            if startdate and enddate:
                if startdate > enddate:
                    return {'error': 'enddate should be greater than startdate',
                            'status': False}
            else:
                return {'error': "Incorrect datetime format,\
                    should be YYYY-MM-DD HH:MM", 'status': False}

        if 'isactive' in args:
            isactive = args['isactive']

        record = Campaign(name, startdate, enddate, 0,
                          isactive, currenttime, 0,
                          currenttime)
        try:
            db.session.add(record)
            db.session.commit()
            insertedid = record.campaignid
        except Exception as e:
            db.session.rollback()
            return {'error': e.message, 'status': False}, 400

        expirytime = (
            datetime.utcnow() + timedelta(minutes=15)
        ).strftime("%Y-%m-%d %H:%M:%S")
        query.token_expiry = expirytime

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return {'message': e.message, 'status': False}

        return {'status': True, 'message': 'campaign is created',
                'id': insertedid}, 201


def validate(date_text):
    try:
        return datetime.strptime(date_text, '%Y-%m-%d %H:%M')
    except Exception as e:
        return False


if __name__ == '__main__':
    app.run(debug=True)
