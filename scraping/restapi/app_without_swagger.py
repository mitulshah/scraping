import json
from datetime import datetime, timedelta
from uuid import uuid4

from flask import jsonify, abort, request
from flask.ext.httpauth import HTTPBasicAuth
from flask.ext.restful import Api, Resource, reqparse
from stormpath.error import Error as StormpathError

# import local module
from models import *
from config import db, app, Flask, client

api = Api(app)
auth = HTTPBasicAuth()

application = client.applications.get(
    'https://api.stormpath.com/v1/applications/4XoOXB03UNt2SAgLN1bLUh')
directory = client.directories.get(
    'https://api.stormpath.com/v1/directories/4XooTUl9bqiIJJD9aoQLoB')

# 200 OK
# 201 Created
# 202 Accepted
# 301 Moved Permanently
# 401 Unauthorized
# 404 Not Found
# 500 Internal Server Error
# 502 Bad Gateway


def validate(date_text):
    try:
        return datetime.strptime(date_text, '%Y-%m-%d %H:%M')
    except Exception as e:
        return False

parser = reqparse.RequestParser()
parser.add_argument('username', type=str)
parser.add_argument('password', type=str)


class Authentication(Resource):

    """
    StormPath authentication of user
    Input :: username, password
    Output :: status, token, expiry
    """

    def post(self):
        args = parser.parse_args()
        username = args['username']
        password = args['password']

        try:
            auth_attempt = application.authenticate_account(username, password)
        except StormpathError, e:
            return {'message': e.message, 'status': False}
        except Exception, e:
            return {'message': e.message, 'status': False}

        token = str(uuid4())

        query = db.session.query(User).filter_by(
            username=username, isdeleted=0).first()
        expirytime = (datetime.utcnow() + timedelta(minutes=15)
                      ).strftime("%Y-%m-%d %H:%M:%S")
        if not query:
            record = User(username, token, expirytime, 0)
            db.session.add(record)
        else:
            query.token = token
            query.token_expiry = expirytime

        try:
            db.session.commit()
        except Exception as e:
            return {'message': e.message, 'status': False}

        return {'status': True, 'token': token,
                'expiry': expirytime}, 200


class Compaign(Resource):

    def get(self, id, token=None, isactive='y'):
        """
        fetch all active records of specific user
        input param : userid
        """
        query = db.session.query(Campaigns).filter(
            Campaigns.userid == id,
            Campaigns.isactive == isactive,
            Campaigns.isdeleted == 0).all()
        return create_json(query)

    def delete(self, id, token):
        """
        delete the record by changing flag isdeleted to 1
        input param : token, compaignid
        """
        query = db.session.query(Campaigns).filter_by(
            campaignid=id, isdeleted=0).first()
        if not query:
            return {'message': "No records are found.", 'status': False}

        usrquery = db.session.query(User).filter_by(
            id=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            return {'message': "invalid token", 'status': False}

        if usrquery.token_expiry > datetime.utcnow():
            try:
                query.isdeleted = 1
                query.deletedate = datetime.utcnow().strftime(
                    "%Y-%m-%d %H:%M:%S")
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': False}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': False}
        else:
            return {'message': "token is expired", 'status': False}

        return {'status': True}, 200

    def put(self, id, token):
        """
        update record
        input param : token, compaignid
        """
        query = db.session.query(Campaigns).filter_by(
            campaignid=id, isdeleted=0).first()
        if not query:
            return {'message': "No records are found.", 'status': False}

        usrquery = db.session.query(User).filter_by(
            id=query.userid, token=token, isdeleted=0).first()
        if not usrquery:
            return {'message': "invalid token", 'status': False}

        if usrquery.token_expiry > datetime.utcnow():

            if 'name' in request.json:
                query.name = request.json['name']

            if 'startdate' in request.json and request.json['startdate'] and \
                    'enddate' in request.json and request.json['enddate']:
                        # startdate and enddate is optional
                startdate = validate(request.json['startdate'])
                enddate = validate(request.json['enddate'])
                if startdate and enddate:
                    if startdate > enddate:
                        return jsonify(
                            {'error': 'enddate should be greater than startdate'})
                    else:
                        query.startdate = request.json['startdate']
                        query.enddate = request.json['enddate']
                else:
                    return jsonify({'error': "Incorrect datetime format,\
                        should be YYYY-MM-DD HH:MM"})

            query.isactive = request.json['isactive']
            query.insertdate = datetime.utcnow().strftime(
                "%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': True}

            usrquery.token_expiry = (
                datetime.utcnow() + timedelta(minutes=15)
            ).strftime("%Y-%m-%d %H:%M:%S")
            try:
                db.session.commit()
            except Exception as e:
                return {'message': e.message, 'status': True}
        else:
            return {'message': "token is expired", 'status': False}
        return {'status': True}, 200


class CompaignList(Resource):

    def post(self):
        """
        create new records in db
        input = username, token
        (optional parameteres) :: startdate, enddate, isactive
        """
        if not request.json or not 'username' in request.json or \
                not 'token' in request.json:
            abort(400)

        startdate = enddate = isactive = name = ''
        username = request.json['username']
        token = request.json['token']
        currenttime = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

        query = db.session.query(User).filter_by(
            username=username, isdeleted=0).first()
        if not query:
            return {'message': "invalid username", 'status': False}

        if query.token == token and query.token_expiry > datetime.utcnow():
            userid = query.id
        else:
            return {'message': "token is expired", 'status': False}

        if 'name' in request.json:
            name = request.json['name']

        if 'startdate' in request.json and request.json['startdate'] and \
                'enddate' in request.json and request.json['enddate']:
            # startdate and enddate is optional
            startdate = validate(request.json['startdate'])
            enddate = validate(request.json['enddate'])
            if startdate and enddate:
                if startdate > enddate:
                    return jsonify(
                        {'error': 'enddate should be greater than startdate'})
            else:
                return jsonify({'error': "Incorrect datetime format,\
                    should be YYYY-MM-DD HH:MM"})

        if 'isactive' in request.json and request.json['isactive']:
            isactive = request.json['isactive']

        record = Campaigns(
            userid, name, startdate, enddate, 0, isactive,
            currenttime, 0, currenttime)
        try:
            db.session.add(record)
            db.session.commit()
        except Exception as e:
            return {'error': e.message, 'status': False}, 400

        expirytime = (
            datetime.utcnow() + timedelta(minutes=15)
        ).strftime("%Y-%m-%d %H:%M:%S")
        query.token_expiry = expirytime
        try:
            db.session.commit()
        except Exception as e:
            return {'message': e.message, 'status': False}

        return {'status': True, 'message': 'campaign is created'}, 201

    def get(self):
        """
        fetch all active records
        """
        query = db.session.query(Campaigns).filter(
            Campaigns.isactive == 'y',
            Campaigns.isdeleted == 0).all()

        return create_json(query)


def create_json(query):
    """
    function to used to generate json of
    fetched data from database
    """
    if not len(query):
        return {'message': "No records are found.", 'status': False}
    result = []
    for item in query:
        result.append({'id': item.campaignid,
                       'name': item.name,
                       'userid': item.userid,
                       'startdate': str(item.startdate),
                       'enddate': str(item.enddate),
                       'sumclick': item.sumclicks,
                       'isactive': item.isactive})
    return jsonify({'compaigns': result})


##
# Actually setup the Api resource routing here
##
api.add_resource(CompaignList, '/compaign', endpoint='compaigns')
api.add_resource(Compaign, '/compaign/<int:id>/<string:token>',
                 endpoint='compaign')
api.add_resource(Authentication, '/auth')

if __name__ == '__main__':
    app.run(debug=True)
