from config import SQLAlchemy
from datetime import datetime
from uuid import uuid4
db = SQLAlchemy()


class Campaign(db.Model):

    """
    Model to campaign list in db
    """
    __tablename__ = 'campaigns'

    campaignid = db.Column(db.Integer, primary_key=True)
    campaignname = db.Column(db.String(255))
    startdate = db.Column(db.DateTime)
    enddate = db.Column(db.DateTime)
    sumclicks = db.Column(db.Integer)
    isactive = db.Column(db.String(20))
    insertdate = db.Column(db.DateTime)
    isdeleted = db.Column(db.Integer)
    deletedate = db.Column(db.DateTime)

    def __init__(self, campaignname, startdate, enddate, sumclicks,
                 isactive, insertdate, isdeleted, deletedate):
        # self.campaignid = campaignid
        self.campaignname = campaignname
        self.startdate = startdate
        self.enddate = enddate
        self.sumclicks = 0
        self.isactive = isactive
        self.insertdate = datetime.utcnow()
        self.isdeleted = 0
        self.deletedate = datetime.utcnow()

    def __repr__(self):
        return '{} {}'.format(self.campaignid, self.campaignname)


class Advertise(db.Model):

    """
    Model referes to advertise table
    """
    __tablename__ = 'advertises'

    advertiseid = db.Column(db.Integer, primary_key=True)
    advertisename = db.Column(db.String(100))
    userid = db.Column(db.Integer,
                       db.ForeignKey('users.userid'),
                       nullable=False)
    isdeleted = db.Column(db.Integer)

    def __init__(self, advertisename, userid, isdeleted):
        self.advertisename = advertisename
        self.userid = userid
        self.isdeleted = 0

    def __repr__(self):
        return '{} {}'.format(self.advertisename, self.userid)


class User(db.Model):

    """
    Model referes to users table
    """
    __tablename__ = 'users'

    userid = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    token = db.Column(db.String(255))
    token_expiry = db.Column(db.DateTime)
    isdeleted = db.Column(db.Integer)

    def __init__(self, username, token, token_expiry,
                 isdeleted):
        self.username = username
        self.token = token
        self.token_expiry = token_expiry
        self.isdeleted = 0

    def __repr__(self):
        pass


class AdsCampaign(db.Model):

    """
    model referes to relation b/w advertise and campaign
    """
    __tablename__ = 'ads_campaign'

    adscampaignid = db.Column(db.Integer, primary_key=True)
    advertiseid = db.Column(db.Integer,
                            db.ForeignKey('advertises.advertiseid'),
                            nullable=False)
    campaignid = db.Column(db.Integer,
                           db.ForeignKey('campaigns.campaignid'),
                           nullable=False)
    isdeleted = db.Column(db.Integer)

    def __init__(self, advertiseid, campaignid, isdeleted):
        self.advertiseid = advertiseid
        self.campaignid = campaignid
        self.isdeleted = 0

    def __repr__(self):
        pass
