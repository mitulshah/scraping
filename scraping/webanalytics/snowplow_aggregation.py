import psycopg2
import datetime
import time
from decimal import Decimal
from pytz import timezone

fmt = "%Y-%m-%d %H:%M:%S"

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=1)
cur = ''
curr = ''


def postgresql_dbconn(func):
    """
    decorator function to used
    to connect with postgresql database
    """
    def func_wrapper(*args, **kwargs):
        global curr
        # Define our connection string
        conn_string = "host='localhost'  \
                       dbname='snowplow' \
                       user='power_user' \
                       password='n31181b'\
                       port=5432"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        conn = psycopg2.connect(conn_string)

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        curr = conn.cursor()

        func(*args, **kwargs)

        conn.commit()
        # close db connection
        conn.close()

    return func_wrapper


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """
    def func_wrapper(*args, **kwargs):
        global cur
        # Define our connection string
        conn_string = "host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com'  \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        conn = psycopg2.connect(conn_string)

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        cur = conn.cursor()

        func(*args, **kwargs)

        conn.commit()
        # close db connection
        conn.close()

    return func_wrapper


class Sqlaggregations(object):

    """
    Fetch raw data and summarize
    data using group by on multiple
    dimension
    """

    @redshift_dbconn
    def data_aggregation(self, start_date, end_date):
        """
        fetch summarize data
        by group of
        mkt_source, mkt_medium, date, app_id etc
        """
        query = """
                SELECT
                app_id, date(convert_timezone('CST', collector_tstamp)) as event_date, 
                mkt_source, mkt_medium, mkt_campaign,
                CASE WHEN domain_sessionidx=1 THEN 'Y' ELSE 'N' END AS is_user_new,
                COUNT(DISTINCT(domain_userid || '-' || domain_sessionidx)) as Visits,
                sum(tr_total) amount
                from atomic.events a
                where event in ('transaction', 'page_view')
                AND collector_tstamp between convert_timezone('CST', '{0}')
                    AND convert_timezone('CST', '{1}')
                GROUP BY date(convert_timezone('CST', collector_tstamp)), event,
                mkt_source, mkt_medium, mkt_campaign, app_id,
                CASE WHEN domain_sessionidx=1 THEN 'Y' ELSE 'N' END
                ORDER BY event_date
                """ .format(start_date, end_date)

        # print query
        try:
            cur.execute(query)
            self.result = cur.fetchall()
        except Exception as e:
            self.result = []
            print e.message

    @postgresql_dbconn
    def add_summarize_data_to_db(self, previous_day, today):
        """
            fetch aggregated data from redshift
            server and store in summarize table
        """
        self.data_aggregation(previous_day, today)
        now_utc = datetime.datetime.now(timezone('UTC'))
        us_time = now_utc.astimezone(timezone('US/Central')).strftime(fmt)

        for row in self.result:
            val = (
                row[0], row[1], row[2], row[3], row[4], row[5], int(row[6]),
                row[7], us_time, us_time)
            curr.execute("INSERT INTO atomic.summarize_events(\
                app_id, collector_date, mkt_source, mkt_medium,\
                mkt_campaign, is_user_new, visits, amount, created_at,\
                updated_at)\
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", val)


Obj = Sqlaggregations()
Obj.add_summarize_data_to_db(previous_day, today)
