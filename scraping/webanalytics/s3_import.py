import datetime
import os
import unicodecsv as csv

from facebook.fb_insights import Fb_insights
from google.adwords_report import GoogleAdwords
import boto3

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=30)

fb = Fb_insights(
    '161210880605065', '7c02f432c6fd31e0a60696070c9e75a1',
    'CAACSntVFu4kBAM5Ej8RZBGOZBg12oVMQf3h7hyDouPA8o4F5xXLRkLCPo1oILUjyCpcZCgFF1YAOCWQ12xzlVZCD0USKKXZAtOZCAnLNygSjkt5cZARCnYdloatV2ZCIZC9kel2XFgRSiFeG1EBbOi77fOLZAdZBxT2KaZCMWZBeX5VVNIO0TLVkIxTI3')

ctime = ''.join(e for e in str(datetime.datetime.now()) if e.isalnum())
bucket = 'allwriteio-spend'


class UploadReport(object):

    """
            upload report to s3 aws bucket
            report location : allwriteio-spend
    """

    def __init__(self):
        pass

    def adwords(self):
        """
                get google adwords report
                and store into s3 bucket
        """
        adObj = GoogleAdwords()
        for k, v in adObj.adwordsReports().iteritems():
            csv_file = str(ctime) + '.csv'
            # dir_name = '/tmp/{}'.format(k)
            # if not os.path.exists(dir_name):
            #     os.makedirs(dir_name)

            file_path = '/tmp/' + csv_file
            self.create_csv(file_path, v)

            s3_client = boto3.client('s3')
            s3_client.upload_file(file_path, bucket, 'adwords/{}/{}'.format(
                k, csv_file))

    def fb_campaigns(self):
        """
                get campaign report and store
                into s3 bucket
        """
        keys = ['account_id', 'account_name', 'actions', 'adset_id',
                'adset_name', 'campaign_id', 'campaign_name',
                'cost_per_inline_link_click',
                'cost_per_inline_post_engagement', 'cost_per_total_action',
                'cost_per_unique_click', 'cpm', 'cpp', 'ctr',
                'date_start', 'date_stop', 'frequency',
                'impressions', 'reach', 'spend', 'total_action_value',
                'total_actions', 'total_unique_actions',
                'unique_clicks', 'unique_ctr']

        campaign_data = []
        for camp in fb.campaign_reports(str(previous_day), str(today)):
            campaigndct = dict(camp)
            campaign_data.append(
                [campaigndct.get(campkey, '') for campkey in keys])
            customername = campaigndct['account_name']

        csv_file = customername + '-campaign.csv'

        camp_unique = str(ctime) + '-campaign.csv'

        dir_name = '/tmp/{}'.format(customername)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        file_path = dir_name + csv_file
        self.create_csv(file_path, campaign_data)

        s3_client = boto3.client('s3')
        s3_client.upload_file(file_path, bucket, 'fb/{}/{}'.format(
            customername, camp_unique))

    def fb_adsets(self):
        """
                get adsets report &
                stored into s3 bucket
        """
        keys = ['account_id', 'account_name', 'actions', 'ad_id',
                'ad_name', 'adset_id', 'adset_name', 'campaign_id',
                'campaign_name', 'cost_per_inline_link_click',
                'cost_per_inline_post_engagement',
                'cost_per_total_action', 'cost_per_unique_click',
                'cpm', 'cpp', 'ctr', 'date_start',
                'date_stop', 'frequency', 'impressions',
                'reach', 'spend', 'total_action_value',
                'total_actions', 'total_unique_actions',
                'unique_clicks', 'unique_ctr']
        adset_data = []
        for adset in fb.adset_reports(str(previous_day), str(today)):
            adsetdct = dict(adset)
            adset_data.append(
                [adsetdct.get(adsetkey, '') for adsetkey in keys])
            customername = adsetdct['account_name']

        csv_file = customername + '-adset.csv'
        adset_unique = str(ctime) + '-adset.csv'

        dir_name = '/tmp/{}'.format(customername)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        file_path = dir_name + csv_file
        self.create_csv(file_path, adset_data)

        s3_client = boto3.client('s3')
        s3_client.upload_file(file_path, bucket, 'fb/{}/{}'.format(
            customername, adset_unique))

    def fb_ads(self):
        """
        get ad report and stored into s3 bucket
        """
        keys = ['account_id', 'account_name', 'actions', 'ad_id',
                'ad_name', 'adset_id', 'adset_name', 'adurl',
                'campaign_id', 'campaign_name', 'cost_per_inline_link_click',
                'cost_per_inline_post_engagement', 'cost_per_total_action',
                'cost_per_unique_click', 'destimation_url', 'cpm', 'cpp',
                'ctr', 'date_start', 'date_stop', 'frequency', 'impressions',
                'reach', 'spend', 'total_action_value',
                'total_actions', 'total_unique_actions', 'unique_clicks',
                'unique_ctr']

        ad_data = []
        for ad in fb.ad_reports(str(previous_day), str(today)):
            addct = dict(ad)
            ad_data.append(
                [addct.get(adkey, '') for adkey in keys])
            customername = addct['account_name']
        csv_file = customername + '-ad.csv'
        ad_unique = str(ctime) + '-ad.csv'

        dir_name = '/tmp/{}'.format(customername)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        file_path = dir_name + csv_file
        self.create_csv(file_path, ad_data)

        s3_client = boto3.client('s3')
        s3_client.upload_file(file_path, bucket, 'fb/{}/{}'.format(
            customername, ad_unique))

    def create_csv(self, filename, data):
        with open(filename, 'wb') as fp:
            writr = csv.writer(fp, delimiter=',')
            writr.writerows(data)

report = UploadReport()
report.fb_campaigns()
report.fb_adsets()
report.fb_ads()
report.adwords()
