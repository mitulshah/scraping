#!/usr/bin/python
# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
import datetime
import time
from pytz import timezone
from math import exp

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=60)


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur

        # Define our connection string

        conn_string = \
            """host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com' \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"""

        # get a connection, if a connect cannot be made an exception will be
        # raised here

        conn = psycopg2.connect(conn_string)
        conn.autocommit = True

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries

        # cur = conn.cursor()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        func(*args, **kwargs)

        # conn.commit()
        # close db connection

        conn.close()

    return func_wrapper


class Main(object):

    def calcScoreQuery(self, previous_day, today):

        query = \
            """SELECT a.*,
        CASE
        WHEN first_collector_hr < 4 THEN 1
        ELSE 0
        END AS flag_hr_0thru3,
        CASE
        WHEN first_collector_hr BETWEEN 4 AND 7 THEN 1
        ELSE 0
        END AS flag_hr_4thru7,
        CASE
        WHEN first_collector_hr BETWEEN 8 AND 11 THEN 1
        ELSE 0
        END AS flag_hr_8thru11,
        CASE
        WHEN first_collector_hr BETWEEN 12 AND 15 THEN 1
        ELSE 0
        END AS flag_hr_12thru15,
        CASE
        WHEN first_collector_hr BETWEEN 16 AND 19 THEN 1
        ELSE 0
        END AS flag_hr_16thru19,
        CASE
        WHEN first_collector_hr BETWEEN 20 AND 23 THEN 1
        ELSE 0
        END AS flag_hr_20thru23,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 0 THEN 1
        ELSE 0
        END AS flag_dow_sun,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 1 THEN 1
        ELSE 0
        END AS flag_dow_mon,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 2 THEN 1
        ELSE 0
        END AS flag_dow_tue,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 3 THEN 1
        ELSE 0
        END AS flag_dow_wed,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 4 THEN 1
        ELSE 0
        END AS flag_dow_thu,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 5 THEN 1
        ELSE 0
        END AS flag_dow_fri,
        CASE
        WHEN DATE_PART(dow,first_collector_date) = 6 THEN 1
        ELSE 0
        END AS flag_dow_sat,
        CASE
        WHEN mkt_campaign = '201604contentmobile' OR CAST(first_collector_date AS DATE) = '04/11/2016' THEN 1
        ELSE 0
        END AS flag_mobile,
        CASE
        WHEN (mkt_campaign != '201604contentmobile' AND CAST(first_collector_date AS DATE) <> '04/11/2016') OR mkt_campaign = 'persistent' THEN 1
        ELSE 0
        END AS flag_desktop,
        -- CASE WHEN flag_mobile = 0 THEN 1
        -- ELSE 0
        -- END AS flag_desktop,
        CASE
        -- WHEN page_urlpath IN ('/article/4-balance-transfer-cards-with-0-interest-until-2017','/article/balance-transfer-credit-cards','/article/mobile-4-balance-transfer-cards-with-0-interest-until-2017','/article/pay-off-credit-card-debt') THEN 1
        WHEN page_urlpath like '/article%' THEN 1
        ELSE 0
        END AS flag_arti,
        CASE
        -- WHEN page_urlpath IN ('/balance-transfer/10-best-balance-transfer-credit-cards-2016','/balance-transfer/best-balance-transfer-credit-card-nofee','/balance-transfer/best-balance-transfer-credit-cards-highest-rated','/balance-transfer/mobile-10-best-balance-transfer-credit-cards-2016','/balance-transfer/mobile-best-balance-transfer-credit-card-nofee','/balance-transfer/mobile-best-balance-transfer-credit-cards-highest-rated') THEN 1
        WHEN page_urlpath like '/balance-transfer%' THEN 1
        ELSE 0
        END AS flag_comp,
        CASE
        WHEN page_urlpath like '/content%' THEN 1
        -- WHEN page_urlpath IN ('/content/chase-slate-product-review','/content/citi-simplicity-product-review','/content/discover-it-product-review','/content/mobile-citi-simplicity-product-review') THEN 1
        ELSE 0
        END AS flag_prod,
        CASE
        WHEN page_urlpath IN ('/article/4-balance-transfer-cards-with-0-interest-until-2017','/article/mobile-4-balance-transfer-cards-with-0-interest-until-2017') THEN 1
        ELSE 0
        END AS flag_arti_4_balance_transfer,
        CASE
        WHEN page_urlpath IN ('/article/balance-transfer-credit-cards') THEN 1
        ELSE 0
        END AS flag_arti_balance_transfer_cc,
        CASE
        WHEN page_urlpath IN ('/article/pay-off-credit-card-debt') THEN 1
        ELSE 0
        END AS flag_arti_pay_off,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/10-best-balance-transfer-credit-cards-2016','/balance-transfer/mobile-10-best-balance-transfer-credit-cards-2016') THEN 1
        ELSE 0
        END AS flag_comp_10_best_2016,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/mobile-best-balance-transfer-credit-card-nofee','/balance-transfer/mobile-best-balance-transfer-credit-card-nofee') THEN 1
        ELSE 0
        END AS flag_comp_10_best_nofee,
        CASE
        WHEN page_urlpath IN ('/balance-transfer/best-balance-transfer-credit-cards-highest-rated','/balance-transfer/mobile-best-balance-transfer-credit-cards-highest-rated') THEN 1
        ELSE 0
        END AS flag_comp_10_best_highest,
        CASE
        WHEN page_urlpath IN ('/content/chase-slate-product-review') THEN 1
        ELSE 0
        END AS flag_prod_chase_slate,
        CASE
        WHEN page_urlpath IN ('/content/citi-simplicity-product-review','/content/mobile-citi-simplicity-product-review') THEN 1
        ELSE 0
        END AS flag_prod_citi_simplicity,
        CASE
        WHEN page_urlpath IN ('/content/discover-it-product-review') THEN 1
        ELSE 0
        END AS flag_prod_discover_it,
        CASE
        WHEN refr_urlpath LIKE '%msn%' THEN 1
        ELSE 0
        END AS msn,
        CASE
        WHEN refr_urlpath LIKE '%mobileposse%' THEN 1
        ELSE 0
        END AS mobileposse,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/crunchmind%' THEN 1
        ELSE 0
        END AS crunchmind,
        CASE
        WHEN refr_urlpath LIKE '%marketingandresearchsolutionslp%' THEN 1
        ELSE 0
        END AS marketingandresearchsolutionslp,
        CASE
        WHEN refr_urlpath LIKE '%wazimo%' THEN 1
        ELSE 0
        END AS wazimo,
        CASE
        WHEN refr_urlpath LIKE '%qpolitical%' THEN 1
        ELSE 0
        END AS qpolitical,
        CASE
        WHEN refr_urlpath LIKE '%socialtheater%' THEN 1
        ELSE 0
        END AS socialtheater,
        CASE
        WHEN refr_urlpath LIKE '%nydailynews%' THEN 1
        ELSE 0
        END AS nydailynews,
        CASE
        WHEN refr_urlpath LIKE '%nbcnews%' THEN 1
        ELSE 0
        END AS nbcnews,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/seccosquared%' THEN 1
        ELSE 0
        END AS seccosquared,
        CASE
        WHEN refr_urlpath LIKE '%viralnova%' THEN 1
        ELSE 0
        END AS viralnova,
        CASE
        WHEN refr_urlpath LIKE '%localtv%' THEN 1
        ELSE 0
        END AS localtv,
        CASE
        WHEN refr_urlpath LIKE '%247sports%' THEN 1
        ELSE 0
        END AS sports247,
        CASE
        WHEN refr_urlpath LIKE '%cox%' THEN 1
        ELSE 0
        END AS cox,
        CASE
        WHEN refr_urlpath LIKE '%cbsinteractive%' THEN 1
        ELSE 0
        END AS cbsinteractive,
        CASE
        WHEN refr_urlpath LIKE '%verticalscope%' THEN 1
        ELSE 0
        END AS verticalscope,
        CASE
        WHEN refr_urlpath LIKE '%socialsweetheartsgmbh%' THEN 1
        ELSE 0
        END AS socialsweetheartsgmbh,
        CASE
        WHEN refr_urlpath LIKE '%alltopics%' THEN 1
        ELSE 0
        END AS alltopics,
        CASE
        WHEN refr_urlpath LIKE '%worldlifestyle%' THEN 1
        ELSE 0
        END AS worldlifestyle,
        CASE
        WHEN refr_urlpath LIKE '%aol%' THEN 1
        ELSE 0
        END AS aol,
        CASE
        WHEN refr_urlpath LIKE '%americanmedia%' THEN 1
        ELSE 0
        END AS americanmedia,
        CASE
        WHEN refr_urlpath LIKE '%disqus%' THEN 1
        ELSE 0
        END AS disqus,
        CASE
        WHEN refr_urlpath LIKE '%tribunebroadcasting%' THEN 1
        ELSE 0
        END AS tribunebroadcasting,
        CASE
        WHEN refr_urlpath LIKE '%advance%' THEN 1
        ELSE 0
        END AS advance,
        CASE
        WHEN refr_urlpath LIKE '%cpxi%' THEN 1
        ELSE 0
        END AS cpxi,
        CASE
        WHEN refr_urlpath LIKE '%labx%' THEN 1
        ELSE 0
        END AS labx,
        CASE
        WHEN refr_urlpath LIKE '%wikia%' THEN 1
        ELSE 0
        END AS wikia,
        CASE
        WHEN refr_urlpath LIKE '%ziffdavis%' THEN 1
        ELSE 0
        END AS ziffdavis,
        CASE
        WHEN refr_urlpath LIKE '%yesimright%' THEN 1
        ELSE 0
        END AS yesimright,
        CASE
        WHEN refr_urlpath LIKE '%ijreview%' THEN 1
        ELSE 0
        END AS ijreview,
        CASE
        WHEN refr_urlpath LIKE '%salonmedia%' THEN 1
        ELSE 0
        END AS salonmedia,
        CASE
        WHEN refr_urlpath LIKE '%inquisitr%' THEN 1
        ELSE 0
        END AS inquisitr,
        CASE
        WHEN refr_urlpath LIKE '%kaizenmediagroup%' THEN 1
        ELSE 0
        END AS kaizenmediagroup,
        CASE
        WHEN refr_urlpath LIKE '%tegna%' THEN 1
        ELSE 0
        END AS tegna,
        CASE
        WHEN refr_urlpath LIKE '%1.1/json/merriamwebster%' THEN 1
        ELSE 0
        END AS merriamwebster,
        CASE
        WHEN refr_urlpath LIKE '%nbcots%' THEN 1
        ELSE 0
        END AS nbcots,
        CASE
        WHEN refr_urlpath LIKE '%gannettcompany%' THEN 1
        ELSE 0
        END AS gannettcompany,
        CASE
        WHEN refr_urlpath LIKE '%timewarnercable%' THEN 1
        ELSE 0
        END AS timewarnercable,
        CASE
        WHEN refr_urlpath LIKE '%usatoday%' THEN 1
        ELSE 0
        END AS usatoday,
        CASE
        WHEN refr_urlpath LIKE '%breitbartcom%' THEN 1
        ELSE 0
        END AS breitbartcom,
        CASE
        WHEN refr_urlpath LIKE '%dailymail%' THEN 1
        ELSE 0
        END AS dailymail,
        CASE
        WHEN refr_urlpath LIKE '%nbcsports%' THEN 1
        ELSE 0
        END AS nbcsports,
        CASE
        WHEN refr_urlpath LIKE '%perezhilton%' THEN 1
        ELSE 0
        END AS perezhilton,
        CASE
        WHEN refr_urlpath LIKE '%vervemobile%' THEN 1
        ELSE 0
        END AS vervemobile,
        CASE
        WHEN refr_urlpath LIKE '%foodnetwork%' THEN 1
        ELSE 0
        END AS foodnetwork,
        CASE
        WHEN refr_urlpath LIKE '%readersdigest%' THEN 1
        ELSE 0
        END AS readersdigest,
        CASE
        WHEN refr_urlpath LIKE '%spartzmedia%' THEN 1
        ELSE 0
        END AS spartzmedia,
        CASE
        WHEN refr_urlpath LIKE '%tickld%' THEN 1
        ELSE 0
        END AS tickld,
        CASE
        WHEN refr_urlpath LIKE '%digi%' THEN 1
        ELSE 0
        END AS digi,
        CASE
        WHEN refr_urlpath LIKE '%myfox%' THEN 1
        ELSE 0
        END AS myfox,
        CASE
        WHEN b.domain_sessionid IS NULL THEN 0
        ELSE 1
        END AS conversion_flag
        FROM (SELECT a.domain_sessionid,
        page_urlpath,
        mkt_campaign,
        TRUNC(first_collector_tstamp) first_collector_date,
        DATE_PART(h,first_collector_tstamp) first_collector_hr,
        MIN(first_collector_tstamp) event_time,
        refr_urlpath
        FROM atomic.events a
        INNER JOIN (SELECT domain_sessionid,
        MIN(collector_tstamp) AS first_collector_tstamp,
        MIN(etl_tstamp) AS first_etl_tstamp
        FROM atomic.events
        WHERE app_id = 'magnify'
        AND   mkt_source = 'taboola'
        AND   mkt_medium = 'referral'
        AND   collector_tstamp BETWEEN '{0}' AND dateadd(hr,+4,'{1}')
        GROUP BY domain_sessionid) b
        ON a.domain_sessionid = b.domain_sessionid
        AND a.collector_tstamp = b.first_collector_tstamp
        AND etl_tstamp = b.first_etl_tstamp
        GROUP BY a.domain_sessionid,
        page_urlpath,
        mkt_campaign,
        TRUNC(first_collector_tstamp),
        DATE_PART(h,first_collector_tstamp),
        first_collector_tstamp,
        refr_urlpath) a
        LEFT OUTER JOIN (SELECT DISTINCT domain_sessionid
        FROM atomic.events
        WHERE app_id = 'magnify'
        AND   page_url LIKE '%SEMBT%'
        AND   collector_tstamp BETWEEN '{0}' AND dateadd(hr,+4,'{1}')
        GROUP BY domain_sessionid) b
        ON a.domain_sessionid = b.domain_sessionid
        """.format(previous_day, today)

        try:
            # print query
            cur.execute(query)
            return cur.fetchall()
        except Exception, e:
            print e.message

    @redshift_dbconn
    def executeQuery(self):
        results = self.calcScoreQuery(previous_day, today)
        # print results
        data = []
        for result in results:
            r = ((result['flag_arti'] * -1.247) +
                 (result['flag_dow_sun'] * 0.9916) +
                 (result['flag_dow_wed'] * 0.8277) +
                 (result['flag_hr_0thru3'] * -0.7175) +
                 (result['marketingandresearchsolutionslp'] * -1.391) +
                 (result['localtv'] * 0.9315) +
                 (result['verticalscope'] * 1.203) +
                 (result['aol'] * 0.913) +
                 (result['crunchmind'] * -1.715) +
                 (result['msn'] * 0.4922) +
                 (result['wazimo'] * -2.02) +
                 (result['sports247'] * 1.618) +
                 (result['socialsweetheartsgmbh'] * 2.054) +
                 (result['americanmedia'] * 1.283) +
                 (result['mobileposse'] * 1.476)
                 )
            e = exp(r)
            # print "expontial value : {}".format(e)
            score_t = e/float((1+e))
            # print "score value : {}".format(score_t)
            result.append("AMD")
            result.append(score_t)
            data.append(tuple(result))
        cur.executemany("""INSERT INTO atomic.magnify_model values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", data)

Obj = Main()
Obj.executeQuery()
