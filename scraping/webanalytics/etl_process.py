import psycopg2
import datetime
import time
from decimal import Decimal
from pytz import timezone
from collections import defaultdict

fmt = "%Y-%m-%d %H:%M:%S"

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=30)
cur = ''


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """
    def func_wrapper(*args, **kwargs):
        global cur
        # Define our connection string
        conn_string = "host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com'  \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        conn = psycopg2.connect(conn_string)
        conn.autocommit = True
        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        cur = conn.cursor()

        func(*args, **kwargs)

        # conn.commit()
        # close db connection
        conn.close()

    return func_wrapper


class Sqlaggregations(object):

    """
    Etl process for D1, D7 and D30
    """

    def aggregation_query(self, method):
        """
        created view using
        CREATE VIEW all_events_of_D7 AS
            SELECT *
            FROM atomic.events a
            WHERE a.collector_tstamp between dateadd(day,-8,current_date) AND GETDATE()
            AND app_id='Acds51'
            AND event in ('page_view', 'transaction')
            AND EXISTS (
                    SELECT 1 FROM atomic.events b
                    WHERE b.collector_tstamp BETWEEN dateadd(day, -2, GETDATE()) AND dateadd(day, -1, GETDATE())
                    AND b.event='transaction'
                    AND a.app_id=b.app_id AND a.domain_userid=b.domain_userid
                )
        """
        # query = """ SELECT
        # bb.*, aa.collector_tstamp transaction_tstamp,
        # concat(bb.domain_userid, aa.collector_tstamp) flag
        # from all_events_of_D7 aa
        # join  all_events_of_D7 bb on  aa.domain_userid = bb.domain_userid and
        # bb.collector_tstamp between dateadd(day, -7, aa.collector_tstamp) and aa.collector_tstamp and
        # bb.event != 'transaction' AND
        # aa.app_id =  bb.app_id
        # WHERE aa.event='transaction' AND
        # aa.collector_tstamp BETWEEN dateadd(day, -2, GETDATE()) AND dateadd(day, -1, GETDATE())
        # ORDER BY aa.app_id, aa.domain_userid, aa.collector_tstamp, bb.collector_tstamp
        # """
        query = """SELECT
        bb.app_id, max(bb.collector_tstamp) event_time,
        bb.event,
        bb.domain_userid,
        bb.domain_sessionid,
        bb.mkt_source,
        bb.mkt_medium,
        bb.mkt_campaign,
        aa.tr_total order_amount,
        aa.collector_tstamp transaction_tstamp,
        concat(bb.domain_userid, max(aa.collector_tstamp)) flag
        from all_events_of_D{0} aa
        join  all_events_of_D{0} bb on  aa.domain_userid = bb.domain_userid and
        bb.collector_tstamp between dateadd(day, -{0}, aa.collector_tstamp) and aa.collector_tstamp and
        bb.event != 'transaction' AND
        aa.app_id =  bb.app_id
        WHERE aa.event='transaction' AND
        aa.collector_tstamp BETWEEN dateadd(day, -2, GETDATE()) AND dateadd(day, -1, GETDATE())
        GROUP BY aa.app_id, bb.app_id, bb.domain_sessionid, bb.mkt_source,
        bb.mkt_medium, bb.mkt_campaign, bb.domain_userid, aa.domain_userid,
        aa.collector_tstamp, bb.event, aa.tr_total
        ORDER BY aa.app_id, aa.domain_userid, aa.collector_tstamp,
        aa.domain_userid, max(bb.collector_tstamp)
        """.format(method)
        # print query
        try:
            cur.execute(query)
            return cur.fetchall()
        except Exception as e:
            self.result = []
            print e.message

    def aggratation_opertions(self, method):
        """
            Fetch raw data from atomic.events table
        """
        # fetch partial aggregated data
        result = self.aggregation_query(method)

        # create set of each transaction with page_view
        # where key is collector_tstamp flag
        transaction_sets = defaultdict(list)
        for row in result:
            transaction_sets[row[-1]].append(row)

        now_utc = datetime.datetime.now(timezone('UTC'))
        us_time = now_utc.astimezone(timezone('US/Central')).strftime(fmt)

        result = []
        for key, value in dict(transaction_sets).iteritems():
            # set all values in variable
            app_id, domain_userid = value[0][0], value[0][3]
            first_click_time, first_click_domain_sessionid = value[
                0][1], value[0][4]
            first_click_source = value[0][5]
            first_click_medium = value[0][6]
            first_click_campaign = value[0][7]
            last_click_time, last_click_domain_sessionid = value[
                -1][1], value[-1][4]
            last_click_source = value[-1][5]
            last_click_medium = value[-1][6]
            last_click_campaign = value[-1][7]
            order_amount = value[0][8]

            # calculate weight for each transaction
            length = len(value)
            even = []
            for ele in value:
                weightage = 1/length
                even.append([ele[5], ele[6], ele[7], weightage])

            result.append((app_id, domain_userid, str(first_click_time),
                           first_click_domain_sessionid, first_click_source,
                           first_click_medium,
                           first_click_campaign, str(last_click_time),
                           last_click_domain_sessionid,
                           last_click_source, last_click_medium,
                           last_click_campaign, order_amount, us_time,
                           us_time, even))
        return result

    @redshift_dbconn
    def aggregation_on_D7(self, method):
        result = self.aggratation_opertions(method)

        for row in result:
            cur.execute("INSERT INTO atomic.summarize_conv_d%s ( \
                app_id, domain_userid, fc_tstamp, fc_domain_sessionid,\
                fc_mkt_source, fc_mkt_medium, fc_mkt_campaign, lc_tstamp, \
                lc_domain_sessionid,\
                lc_mkt_source, lc_mkt_medium, lc_mkt_campaign, order_amount, \
                created_at, updated_at)\
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (method,
                                                                                   row[0], row[1], row[2], row[
                                                                                       3], row[4], row[5], row[6],
                                                                                   row[7], row[8], row[9], row[
                                                                                       10], row[11], row[12],
                                                                                   row[13], row[14]))

            cur.execute("SELECT * FROM atomic.summarize_conv_d{7} WHERE \
            	app_id='{0}' AND domain_userid='{1}' AND fc_tstamp='{2}' AND \
            	fc_domain_sessionid='{3}' AND lc_tstamp='{4}' AND \
            	lc_domain_sessionid='{5}' AND created_at='{6}'".format(
                        row[0], row[1], row[2], row[3], row[7], row[8], row[13], method))

            last_insert_id = cur.fetchone()[0]
            for r in row[15]:
                query = "INSERT INTO atomic.summarize_conv_d{0}_even (conv_id, mkt_source, mkt_medium, mkt_campaign, weightage) VALUES (%s, %s, %s, %s, %s)".format(
                    method)
                cur.execute(query, (last_insert_id, r[0], r[1], r[2], r[3]))

Obj = Sqlaggregations()
for val in [1, 7, 30]:
    Obj.aggregation_on_D7(val)
