#!/usr/bin/python
#
# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""This example downloads a criteria performance report.

To get report fields, run get_report_fields.py.

The LoadFromStorage method is pulling credentials and properties from a
"googleads.yaml" file. By default, it looks for this file in your home
directory. For more information, see the "Caching authentication information"
section of our README.

Api: AdWordsOnly
"""

import logging
# import sys
import psycopg2

from googleads import adwords
from getadwordsclient import getClientID

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.transport').setLevel(logging.DEBUG)


class ReportData(object):

    """
    Add the adwords API data job to the 
    postgresql database in snowplow
    """

    def connection(self):
        # Connection with Database
        # Define our connection string
        conn_string = "host='localhost' dbname='snowplow' user='root' password='root'"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        self.conn = psycopg2.connect(conn_string)

        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        self.cur = self.conn.cursor()

    def adwordsQuery(self, client):
        """
        write a adwords query
        """
        report_downloader = client.GetReportDownloader(version='v201509')

        # Create report definition.
        report = {
            'reportName': 'FINAL_URL_REPORT',
            'dateRangeType': 'LAST_30_DAYS',
            'reportType': 'FINAL_URL_REPORT',
            'downloadFormat': 'CSV',
            'selector': {
                'fields': ['CampaignId', 'CampaignName', 'AdGroupId', 'AdGroupName',
                           'AverageCpc', 'Cost', 'Date', 'Clicks', 'EffectiveFinalUrl'
                           ]
            }
        }

        # You can provide a file object to write the output to. For this demonstration
        # we use sys.stdout to write the report to the screen.
        # report_downloader.DownloadReport(
        #     report, sys.stdout, skip_report_header=False, skip_column_header=False,
        #     skip_report_summary=False, include_zero_impressions=False)

        return report_downloader.DownloadReportAsString(
            report, skip_report_header=True, skip_column_header=True,
            skip_report_summary=False, include_zero_impressions=False)

    def RepresentsInt(self, val):
        """
        convert string to integer if it's actual integer
        """
        try:
            return int(val)
        except ValueError:
            return val

    def __init__(self):
        """
        * Create database connection
        * fetch all adwords client customer ID
        * call adwordsQuery function for query
        * insert into database
        """
        # create database connection
        self.connection()

        adwords_client = adwords.AdWordsClient.LoadFromStorage()

        # fetch client by calling getClientID
        for client in getClientID(adwords_client):
            adwords_client.SetClientCustomerId(client[0])
            for line in self.adwordsQuery(adwords_client).split('\n'):
                # call representInt function to convert into int
                lst = map(self.RepresentsInt, line.split(','))

                if len(lst) > 1:
                    try:

                        self.cur.execute("""INSERT INTO atomic.adwords(
							clientid,campaignid, campaignname, adgroupid, adgroupname, 
							avaragecpc, cost, datetime, clicks, final_url) 
						VALUES ({0}, {1}, '{2}', {3}, '{4}', {5}, {6}, '{7}',
							{8}, '{9}')""".format(client[0], lst[0], str(lst[1]),
                             lst[2], str(lst[3]), lst[
                            4]/1000000.0, lst[5]/1000000.0,
                            lst[6], lst[7], str(lst[8])))
                    except Exception as e:
                        print e.message
            self.conn.commit()
        self.conn.close()


# create object of class and call __init__ method
ReportData()
