import psycopg2
import datetime
import time
from pytz import timezone
# import numpy as np

fmt = "%Y-%m-%d %H:%M:%S"

today = datetime.date.today()
previous_day = today - datetime.timedelta(days=30)
cur = ''


def redshift_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """
    def func_wrapper(*args, **kwargs):
        global cur
        # Define our connection string
        conn_string = "host='snowplow.cfeckwagvyh7.us-east-1.redshift.amazonaws.com'  \
                       dbname='webanalytics' \
                       user='niravb' \
                       password='XmTW9rsc'\
                       port=5439"

        # get a connection, if a connect cannot be made an exception will be
        # raised here
        conn = psycopg2.connect(conn_string)
        conn.autocommit = True
        # conn.cursor will return a cursor object, you can use this cursor to
        # perform queries
        cur = conn.cursor()

        func(*args, **kwargs)

        # conn.commit()
        # close db connection
        conn.close()

    return func_wrapper


class RawDataSummary(object):

    """
    Summarized raw data with group of source / medium / campaign
    Client: Magnify, MileCards, ihk
    Period: Return data for Yesterday vs All Time
    Response: summarized data for first click and last click
    """

    def query_first_click(self, kword, period, ptime, clicktype):
        # Execute query
        query = """select  a.app_id,
        a.page_urlhost AS PAGEURLHOST, a.page_urlpath as PAGEURLPATH,
        a.mkt_source as MKTSOURCE,
        a.mkt_medium  as MKTMEDIUM, a.mkt_campaign as MKTCAMPAIGN,
        '{3}' as attr_method,
        count(distinct a.domain_sessionid) as visits,
        count(distinct a.domain_userid) as visitors,
        count(distinct b.domain_userid) as converters,
        '{2}' as timeperiod
        from atomic.events a left outer join
          (select domain_userid from atomic.events
          where page_url like '%{0}%') b
          on a.domain_userid = b.domain_userid
        where
        collector_tstamp between dateadd(day,{1},current_date) and dateadd(day, -1, current_date)
        and mkt_source != ''
        and mkt_medium != ''
        and mkt_campaign != ''""".format(kword, period, ptime, clicktype)
        if clicktype == 'fc':
            query += "and domain_sessionidx = 1"
        query += """group by  a.app_id, a.page_urlhost, a.page_urlpath,
        a.mkt_source, a.mkt_medium, a.mkt_campaign
        order by a.app_id asc"""

        try:
            cur.execute(query)
            return cur.fetchall()
        except Exception as e:
            self.result = []
            print e.message

    @redshift_dbconn
    def operation_on_data(self):
        # calc. US timezone time
        now_utc = datetime.datetime.now(timezone('UTC'))
        us_time = now_utc.astimezone(timezone('US/Central')).strftime(fmt)

        fc_all = self.query_first_click('SEMBT', -366, 'all', 'fc')
        fc_yesterday = self.query_first_click(
            'SEMBT', -2, 'yesterday', 'fc')
        lc_all = self.query_first_click('SEMBT', -366, 'all', 'lc')
        lc_yesterday = self.query_first_click(
            'SEMBT', -2, 'yesterday', 'lc')
        
        # delete old data only new data is receiving
        cur.execute("truncate table atomic.performance_report")

        result = [fc_all, fc_yesterday, lc_all,
                  lc_yesterday]

        # let's insert new summarized records in db
        for res in result:
            for row in res:
                cur.execute("INSERT INTO atomic.performance_report ( app_id,\
                    page_urlhost, page_urlpath, mkt_source, mkt_medium,\
                    mkt_campaign, clicktype, visits, visitors, \
                    converters, ptime, created_at)\
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)\
                    ", (row[0], row[1], row[2], row[3], row[4], row[5], row[6],
                        row[7], row[8], row[9], row[10], us_time))

Obj = RawDataSummary()
Obj.operation_on_data()


# np_fc_all = np.array(fc_all)
# try:
#     np_fc_all_uniqueid = np_fc_all[:, -2]
#     cur.execute("delete from atomic.performance_report where \
#         ptime='all' and uniqueid in ('{0}')".format("', '".join(
#         np_fc_all_uniqueid)))
# except Exception as e:
#     print e.message

# np_fc_yesterday = np.array(fc_yesterday)
# try:
#     np_fc_yesterday_uniqueid = np_fc_yesterday[:, -2]
#     cur.execute("delete from atomic.performance_report where \
#         ptime='yesterday' and uniqueid in ('{0}')".format("', '".join(
#         np_fc_yesterday_uniqueid)))
# except Exception as e:
#     print e.message

# np_lc_all = np.array(lc_all)
# try:
#     np_lc_all_uniqueid = np_lc_all[:, -2]
#     cur.execute("delete from atomic.performance_report where \
#         ptime='all' and uniqueid in ('{0}')".format("', '".join(
#         np_lc_all_uniqueid)))
# except Exception as e:
#     print e.message

# np_lc_yesterday = np.array(lc_yesterday)
# try:
#     np_lc_yesterday_uniqueid = np_lc_yesterday[:, -2]
#     cur.execute("delete from atomic.performance_report where \
#         ptime='yesterday' and uniqueid in ('{0}')".format("', '".join(
#         np_lc_yesterday_uniqueid)))
# except Exception as e:
#     print e.message
