from datetime import datetime, timedelta
from pytz import timezone
from uuid import uuid4
import ast
from collections import defaultdict

from flask.ext.restplus import Api, Resource, fields
from stormpath.error import Error as StormpathError
# from flask_restful import reqparse
from werkzeug.contrib.fixers import ProxyFix
from sqlalchemy.sql import func
# import local module
from models import *
from config import db, app, Flask, client

cst = timezone('US/Central')
curtime = cst.localize(datetime.now()).strftime('%Y-%m-%d')

app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='Snowplow Aggregation API',
          description='Data Summarization',
          )


def abort_if_doesnt_exist(ele):
    api.abort(404, "{} doesn't exist".format(ele))


visits = api.namespace(
    'visits', description='Compute Marketing attributes')
performance_report = api.namespace(
    'performance', description='Performance Report, \
    Yesterday vs All Time')

conversion_param = api.parser()
conversion_param.add_argument(
    'startdate', type=str, required=False, help='Start DATE, \n \
    Format: yyyy-mm-dd',
    location='form')
conversion_param.add_argument(
    'enddate', type=str, required=False, help='End DATE,\n \
    Format: yyyy-mm-dd',
    location='form')


@visits.route('/<string:app_id>/<string:startdate1>/<string:enddate>')
@api.doc(responses={404: 'No record founds'},
         params={'startdate': 'Start Date Format: yyyy-mm-dd',
                 'enddate': 'End Date Format: yyyy-mm-dd',
                 'app_id': 'Client identification Code'})
class Conversion(Resource):

    """
    Return calculation of visits and conversion
    """

    def get(self, app_id, startdate, enddate):
        """
        visit records of given periods
        """
        # resulta = db.session.query(SummarizeEvent)
        query = """SELECT
                   sum(visits), a.mkt_source, a.mkt_medium, a.mkt_campaign
                   FROM atomic.summarize_events a
                   WHERE 
                   a.collector_date between '{0}' and '{2}'
                   AND a.app_id = '{3}'
                   AND EXISTS (
                        SELECT 1 from atomic.summarize_events b
                          where coalesce(a.mkt_source, 'Empty') = coalesce(b.mkt_source, 'Empty')
                          AND coalesce(a.mkt_medium, 'Empty')  = coalesce(b.mkt_medium, 'Empty')
                          AND coalesce(a.mkt_campaign, 'Empty') = coalesce(b.mkt_campaign, 'Empty')
                          AND b.collector_date between '{0}' and '{1}'
                          AND b.app_id = '{3}'
                   )
                   GROUP BY a.mkt_source, a.mkt_medium, a.mkt_campaign
                   ORDER BY mkt_source""".format(startdate, enddate, curtime, app_id)
        raw = db.session.execute(query)

        result = []
        for row in raw:
            result.append(
                {'visits': row[0], 'source': row[1], 'medium': row[2],
                 'campaign': row[3]})
        return {'result': result, "status": True}


@performance_report.route('/<string:app_id>')
@api.doc(responses={404: 'No record founds'},
         params={'app_id': 'Client identification Code'})
class PerformanceReport(Resource):

    """
    Return performance of each channel with their landing page
    """

    def get(self, app_id):
        """

        """
        query = """SELECT * FROM atomic.performance_report \
        WHERE  app_id='{0}' order by ptime, mkt_source,
        mkt_medium, mkt_campaign """.format(
            app_id)

        rows = db.session.execute(query)
        result = []
        d = defaultdict()
        for row in rows:
            uid = row[3] + row[4] + row[5]
            try:
                if len(d[uid]) > 0:
                    pass
            except KeyError:
                d[uid] = defaultdict(list)
            d[uid]['MKTSOURCE'] = row[3]
            d[uid]['MKTMEDIUM'] = row[4]
            d[uid]['MKTCAMPAIGN'] = row[5]
            otherdata = {'PAGEURLHOST': row[1], 'PAGEURLPATH': row[2],
                         'MKTSOURCE': row[3], 'MKTMEDIUM': row[4],
                         'MKTCAMPAIGN': row[5], 'attr_method': row[6],
                         'visits': row[7],
                         'visitors': row[8], 'converters': row[9]}
            if row[10] == u'all':
                d[uid]['resultsalltime'].append(otherdata)
            else:
                d[uid]['resultsyesterday'].append(otherdata)
        result.append(d)

        return {'result': result}

if __name__ == '__main__':
    app.debug = True
    app.run(port=8080)
