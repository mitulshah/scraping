from facebookads.api import FacebookAdsApi
from facebookads.objects import Campaign, AdUser, Insights,\
    AdSet, AdAccount, Ad, AdCreative
from facebookads import objects
from facebookads.specs import ObjectStorySpec

fields = [Insights.Field.account_id,
          Insights.Field.account_name,
          Insights.Field.actions,
          Insights.Field.ad_id,
          Insights.Field.ad_name,
          Insights.Field.adset_id,
          Insights.Field.adset_name,
          Insights.Field.campaign_id,
          Insights.Field.campaign_name,
          Insights.Field.cost_per_total_action,
          Insights.Field.cost_per_inline_link_click,
          Insights.Field.cost_per_inline_post_engagement,
          Insights.Field.cost_per_unique_click,
          Insights.Field.cpm,
          Insights.Field.cpp,
          Insights.Field.ctr,
          Insights.Field.date_start,
          Insights.Field.date_stop,
          Insights.Field.frequency,
          Insights.Field.impressions,
          Insights.Field.reach,
          Insights.Field.spend,
          Insights.Field.total_action_value,
          Insights.Field.total_actions,
          Insights.Field.total_unique_actions,
          Insights.Field.unique_clicks,
          Insights.Field.unique_ctr
          ]


class Fb_insights(object):

    def __init__(self, app_id, app_secret, access_token):
        my_app_id = app_id
        my_app_secret = app_secret
        my_access_token = access_token
        FacebookAdsApi.init(my_app_id, my_app_secret, my_access_token)

        # Add after FacebookAdsApi.init
        me = AdUser(fbid='me')
        # return all account holder object list
        self.my_account = me.get_ad_accounts()[0]

    def campaign_reports(self, since, until):
        """
        Get all Campaigns report for given period
        """
        params = {
            #'date_preset': Insights.Preset.last_7_days,
            'level': Insights.Level.campaign,
            'sort_by': 'date_start',
            'sort_dir': 'desc',
            'time_range': {
                'since': since,
                'until': until,
            },
        }
        print "== Campagin == " * 3
        campaignlist = []

        # for campaign in self.my_account.get_campaigns():
        #     for insight in campaign.get_insights(fields=fields,
        #                                       params=params):
        #       print insight
        #         campaignlist.append(insight)

        for insight in self.my_account.get_insights(
                fields=fields, params=params):
            campaignlist.append(insight)

        print "Successfully retrieved."
        return campaignlist

    def adset_reports(self, since, until):
        """
        Get all Adsets report for given period
        """
        params = {
            #'date_preset': Insights.Preset.last_7_days,
            'level': Insights.Level.adset,
            'sort_by': 'date_start',
            'sort_dir': 'desc',
            'time_range': {
                'since': since,
                'until': until,
            },
        }

        print "== Adsets == " * 3
        adsetlist = []
        for insight in self.my_account.get_insights(
                fields=fields, params=params):
            adsetlist.append(insight)

        print "Successfully retrieved."
        return adsetlist

    def ad_performance_reports(self, since, until):
        """
        get all ad objects for given period
        """
        params = {
            #'date_preset': Insights.Preset.last_7_days,
            'level': Insights.Level.ad,
            'sort_by': 'date_start',
            'sort_dir': 'desc',
            'time_range': {
                'since': since,
                'until': until,
            },
        }
        return self.my_account.get_insights(fields=fields, params=params)

    def fb_creativies(self, since, until):
        """
        This function is used to get destination url
        for each creative object
        """
        fields = [AdCreative.Field.object_story_spec]
        params = {
            # 'level': Insights.Level.ad,
            'time_range': {
                'since': since,
                'until': until,
            },
        }

        return self.my_account.get_ad_creatives(fields=fields, params=params)

    def ad_relative_ids(self, since, until):
        """
        This function is used to get relation between
        ad and creative object
        """
        fields = [Ad.Field.creative]
        params = {
            'level': Insights.Level.ad,
            'time_range': {
                'since': since,
                'until': until,
            },
        }
        return self.my_account.get_ads(fields=fields, params=params)

    def ad_reports(self, since, until):
        """
        This function is used to assign destination url
        to each ad object
        ad_to_creative = dictionary of (AD ID, Creative ID)
        creative_dict = dictionary of (Creative ID, Desitnation url)
        adlist = Each AD has destination url (if ad has)
        """
        # relation of creative id and destination url for all ads
        ad_to_creative = {}  # relation of ad id and creative id for all ads
        for ad_relative_obj in self.ad_relative_ids(since, until):
            ad_id = ad_relative_obj['id']
            creative_id = ad_relative_obj['creative']['id']
            ad_to_creative[ad_id] = creative_id
        # # print ad_to_creative

        creative_dict = {}
        for creative_obj in self.fb_creativies(since, until):
            try:
                creative_id = creative_obj['id']
                dest_url = creative_obj[
                    'object_story_spec']['link_data']['link']
                creative_dict[creative_id] = dest_url
            except KeyError:
                pass
        # # print creative_dict

        adlist = []
        for ads in self.ad_performance_reports(since, until):
            try:
                adid = ads['ad_id']
                creativeid = ad_to_creative[adid]
                destination_url = creative_dict[creativeid]
                ads['destimation_url'] = destination_url
            except KeyError:
                pass
            adlist.append(ads)
        return adlist

"""
To generate long life access token
oauth/access_token?client_id=161210880605065&client_secret=
7c02f432c6fd31e0a60696070c9e75a1&grant_type=fb_exchange_token&
fb_exchange_token=CAACSntVFu4kBAOJpEfgxz4jAjAQZASnXkgZAfOu3AN1ZCJQmIQ
aL8BEvabZAuv89hG6ZAIXPsTIYY5s6UeyeDwCL52Xj2ALDFbnWReoCtytKZAzJTIUJ8TIEC
ZAuNlZCuHNjxcpoivbt2kAmrjbMD2LKtO72KQhNdyXNce8ZAZA7zU3Fw0HCgj7X6LzOvZBeh
qjKv6ORAkX126EXgZDZD
"""
