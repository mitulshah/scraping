# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import urllib2
import MySQLdb


# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '70.248.28.13:8080'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

url = "https://www.cappex.com/page/suggest/collegeSearchResults.jsp?phrase=university&begin=0&submitButton=SEARCH&suggestResultURL=/page/collegeProfile30/modularProfile30.jsp&suggestResultAnchor=&suggestModuleID="
cur = db = ''


def get_all_colleges(base_url):
    request = urllib2.Request(base_url, headers=hdr)
    try:
        result = urllib2.urlopen(request)
    # except urllib2.HTTPError, err:
    #     print(e)rr.code
    except Exception as e:
        print(e)
    data = result.read()
    soup = BeautifulSoup(data, 'html.parser')
    ullist = soup.find(
        'ul', {'class': 'meritAidSearchResults'}).find_all('a', href=True)
    collegelinks = []
    for anchor in ullist:
        link = "https://www.cappex.com" + \
            anchor['href'] + "/scholarships-and-aid"
        collegelinks.append(link)
    return collegelinks


def merit_link():
    meritlinks = get_all_colleges(url)
    for link in meritlinks:
        print(link)
        request = urllib2.Request(link, headers=hdr)
        try:
            result = urllib2.urlopen(request)
        except Exception as e:
            print(e)
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')
        try:
            tabledata = soup.find(
                'div', {'id': 'primaryContentLoggedIn'}).find('table')
        except Exception as e:
            pass

        scholar_link = []
        try:
            tabletr = tabledata.find('tbody').find_all('tr')
            for eachtr in tabletr:
                newlink = "https://www.cappex.com" + \
                    eachtr.find('a', href=True)['href']
                scholar_link.append(newlink)
        except Exception as e:
            pass

        get_data(scholar_link)


def mysql_dbconn(func):
    """
    decorator function to used
    to connect with redshift database
    """

    def func_wrapper(*args, **kwargs):
        global cur
        global db

        # Define our connection string

        db = MySQLdb.connect("localhost", "root", "root", "scrape")

        cur = db.cursor()
        db.set_character_set('utf8')
        cur.execute('SET NAMES utf8;')
        cur.execute('SET CHARACTER SET utf8;')
        cur.execute('SET character_set_connection=utf8;')
        func(*args, **kwargs)

        db.commit()
        db.close()

    return func_wrapper


@mysql_dbconn
def get_data(scholar_links):
    records = []
    for page_link in scholar_links:
        # print(page_link)
        request = urllib2.Request(page_link, headers=hdr)
        try:
            result = urllib2.urlopen(request)
        except Exception as e:
            print(e)
        data = result.read()
        soup = BeautifulSoup(data, 'html.parser')
        scholarship_name = ''
        try:
            university_name = soup.find(
                'h1', {'class': 'grid_16'}).text.strip()
        except Exception as e:
            pass
        main_div = soup.find('div', {'class': 'grid_10'})
        try:
            scholarship_name = main_div.find('h2').text.strip()
        except Exception as e:
            pass
        try:
            amount_div = main_div.find(
                'dl', {"class": "first"}).find_all('dd')[1].text.strip()
        except Exception as e:
            pass
        award_criteria_div = soup.find(
            'div', {'class': 'grid_10'}).find_all('dl')[1]
        alldd = award_criteria_div.find_all('dd')
        gpa = alldd[1].text.strip()
        sat = alldd[2].text.strip()
        act = alldd[3].text.strip()
        records.append((university_name, scholarship_name, amount_div,
                        gpa, sat, act))
    q = """ INSERT INTO `cappex`(`university_name`, `scholarship_name`,
    `amount`, `gpa`, `sat`, `act`)
    VALUES (%s, %s, %s, %s, %s, %s)
    """
    try:
        cur.executemany(q, records)
    except Exception as e:
        print(e)
        db.rollback()
merit_link()
