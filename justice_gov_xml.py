import xmltodict
import json
import urllib2
import MySQLdb

# set proxy
hdr = {"User-Agent":
       "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"}
proxy = urllib2.ProxyHandler({'http': '70.248.28.13:8080'})

# Create an URL opener utilizing proxy
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

# base_url = "https://www.justice.gov/ust/eo/bapcpa/ccde/docs/credit_card_act/cc_approved_agencies.xml"
base_url = "https://www.justice.gov/ust/eo/bapcpa/ccde/docs/credit_card_act/de_approved_agencies.xml"
request = urllib2.Request(base_url, headers=hdr)
result = urllib2.urlopen(request)
data = result.read()
json_formatted = xmltodict.parse(data)
for key_state, state in json_formatted['de_agencies'].iteritems():

    for stateObj in state:
        for ele, elevalue in stateObj.iteritems():
            if ele == 'state_code':
                state_code = elevalue
            if ele == 'state_name':
                state_name = elevalue
            if ele == 'judicial_district':
                if not isinstance(elevalue, list):
                    elevalue = [elevalue]
                for judicial in elevalue:
                    for jdk, jdv in judicial.iteritems():
                        if jdk == 'judicial_district_short_name':
                            judicial_district_short_name = jdv
                        if jdk == 'judicial_district_formal_name':
                            judicial_district_formal_name = jdv
                        if jdk == 'agency':
                            if not isinstance(jdv, list):
                                jdv = [jdv]
                            for agObj in jdv:
                                for agObjk, agObjv in agObj.iteritems():
                                    if agObjk == 'org_id':
                                        org_id = agObjv
                                    if agObjk == 'organization_name':
                                        organization_name = agObjv
                                    if agObjk == 'agency_name_address':
                                        agency_name_address = agObjv
                                    if agObjk == 'agency_web_site_url':
                                        agency_web_site_url = agObjv
                                    if agObjk == 'service_method':
                                        if not isinstance(agObjv, list):
                                            agObjv = [agObjv]
                                        for agItem in agObjv:
                                            for servicK, serviceV in agItem.iteritems():
                                                if servicK == 'service_method_name':
                                                    service_method_name = serviceV
                                                if servicK == 'phone_number':
                                                    phone_number = serviceV
                                                if servicK == 'phone_extension':
                                                    phone_extension = serviceV
                                                if servicK == 'web_site_url':
                                                    web_site_url = serviceV
                                                if servicK == 'location_address_street_address1':
                                                    location_address_street_address1 = serviceV
                                                if servicK == 'location_address_street_address2':
                                                    location_address_street_address2 = serviceV
                                                if servicK == 'location_address_city':
                                                    location_address_city = serviceV
                                                if servicK == 'location_address_state_code':
                                                    location_address_state_code = serviceV
                                                if servicK == 'location_address_postal_code':
                                                    location_address_postal_code = serviceV
                                                if servicK == 'location_address_phone_number':
                                                    location_address_phone_number = serviceV
                                                if servicK == 'doing_business_as':
                                                    doing_business_as = serviceV
                                                if servicK == 'location_language_name':
                                                    location_language_name = serviceV
                                                if servicK == 'translator_only_flag':
                                                    translator_only_flag = serviceV
                                                if servicK == 'written_material_only_flag':
                                                    written_material_only_flag = serviceV
                                                if servicK == 'full_service_flag':
                                                    full_service_flag = serviceV
                                            response = (state_code, state_name,
                                                        judicial_district_short_name,
                                                        judicial_district_formal_name, org_id,
                                                        organization_name,
                                                        agency_name_address, agency_web_site_url,
                                                        service_method_name,
                                                        phone_number, phone_extension, web_site_url,
                                                        location_address_street_address1,
                                                        location_address_street_address2,
                                                        location_address_city,
                                                        location_address_state_code,
                                                        location_address_postal_code,
                                                        location_address_phone_number,
                                                        doing_business_as, location_language_name,
                                                        translator_only_flag,
                                                        written_material_only_flag, full_service_flag,
                                                        'de')
                                            db = MySQLdb.connect(
                                                "localhost", "root", "XmTW9rsc", "scrape",
                                                use_unicode=True, charset='utf8')
                                            cursor = db.cursor()
                                            try:
                                                cursor.execute("""INSERT into cc_approved_agencies (state_code, state_name,
		                                			judicial_district_short_name,
		                                			judicial_district_formal_name, org_id,
		                                			organization_name,
		                                			agency_name_address, agency_web_site_url,
		                                			service_method_name,
		                                			phone_number, phone_extension, web_site_url,
		                                			location_address_street_address1,
		                                			location_address_street_address2,
		                                			location_address_city,
		                                			location_address_state_code,
		                                			location_address_postal_code,
		                                			location_address_phone_number,
		                                			doing_business_as, location_language_name,
		                                			translator_only_flag,
		                                			written_material_only_flag, full_service_flag, cc_type)
		                                			values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
		                                			%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", response)
                                                db.commit()
                                            except Exception as e:
                                                print e
