import cookielib
import os
import urllib
import urllib2
import re
import string
from bs4 import BeautifulSoup
import time
import requests
from credential_fb import username, password
import json
import operator
import csv

cookie_filename = "parserfb.cookies.txt"


class LinkedInParser(object):

    def __init__(self, login, password):
        """ Start up... """
        self.login = login
        self.password = password

        # Simulate browser with cookies enabled
        self.cj = cookielib.MozillaCookieJar(cookie_filename)
        if os.access(cookie_filename, os.F_OK):
            self.cj.load()
        proxy = urllib2.ProxyHandler({'http': '216.189.148.204:8080'})
        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cj),
            proxy
        )
        self.opener.addheaders = [
            ('User-agent',
             ('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'))
        ]

        # Login
        # self.loginPage()

        title = self.loadData()
        print title
        self.cj.save()

    def loadPage(self, url, data=None):
        """
        Utility function to load HTML from URLs for us with hack to continue despite 404
        """
        # We'll print the url in case of infinite loop
        # print "Loading URL: %s" % url
        try:
            if data is not None:
                response = self.opener.open(url, data)
            else:
                response = self.opener.open(url)
            return ''.join(response.readlines())
        except:
            # If URL doesn't load for ANY reason, try again...
            # Quick and dirty solution for 404 returns because of network problems
            # However, this could infinite loop if there's an actual problem
            return self.loadPage(url, data)

    def loadData(self):

        html = self.loadPage("https://www.facebook.com/people/Rahul-Nawab/100003227254766")
        soup = BeautifulSoup(html, 'html.parser')
        return soup.find("title")


parser = LinkedInParser(username, password)
